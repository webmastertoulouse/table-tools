import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { ContextMenuHeader } from "../models/cm-header";
import { Column } from "../models/column";

@Injectable({
  providedIn: 'root'
})
export class TableService {

  private _updateCell                 = new Subject<any>();
  private _autoCompleteCell           = new Subject<any>();
  private _addRow                     = new Subject<any>();
  private _contextMenuHeader          = new Subject<ContextMenuHeader>();
  private _tooltip                    = new Subject<any>();
  private _closeModal                 = new Subject<any>();

  public updateCell$                  = this._updateCell.asObservable();
  public autoCompleteCell$            = this._autoCompleteCell.asObservable();
  public addRow$                      = this._addRow.asObservable();
  public contextMenuHeader$           = this._contextMenuHeader.asObservable();
  public tooltip$                     = this._tooltip.asObservable();
  public closeModal$                  = this._closeModal.asObservable();

  private _showLog: boolean           = false;

  constructor() { }

  onUpdateCell(data: any): void {
    this._updateCell.next(data);

    if (this._showLog === true) {
      console.log('TableService::onUpdateCell::data', data);
    }
  }

  onAutoCompleteCell(data: any): void {
    this._autoCompleteCell.next(data);

    if (this._showLog === true) {
      console.log('TableService::onAutoCompleteCell::data', data);
    }
  }

  onAddRow(data: any): void {
    this._addRow.next(data);

    if (this._showLog === true) {
      console.log('TableService::onAddRow::data', data);
    }
  }

  onContextMenuHeader(data: ContextMenuHeader): void {
    this._contextMenuHeader.next(data);

    if (this._showLog === true) {
      console.log('TableService::onContextMenuHeader::data', data);
    }
  }

  onTooltip(data: { col: Column, event: MouseEvent, value: any, tableCell: any }): void {
    this._tooltip.next(data);

    if (this._showLog === true) {
      console.log('TableService::onTooltip::data', data);
    }
  }

  onCloseModal(data: boolean): void {
    this._closeModal.next(data);

    if (this._showLog === true) {
      console.log('TableService::onCloseModal::data', data);
    }
  }

  set showLog(showLog: boolean) {
    this._showLog = showLog;
  }

  get showLog(): boolean {
    return this._showLog;
  }
}
