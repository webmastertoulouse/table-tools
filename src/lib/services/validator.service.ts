import { Injectable } from '@angular/core';
import { langage } from '../config/langage';
import { date_ddmmyyyy_pattern, number_pattern } from '../const';

@Injectable({
  providedIn: 'root',
})
export class ValidatorService {

    constructor() { }

    translateFormControlError(header: string, errors: any): string {
        let message: string = '';
        for (let error of Object.keys(errors)) {
            message += this._decodeError(header, error, errors[error]);
        }

        return message;
    }

    private _decodeError(header: string, key: string, error: any): string {
        let message: string = '';
        switch (key) {
            case 'maxlength':
                message = langage.TRANSLATE.maxlength.replace('[requiredLength]', error.requiredLength);
                message = message.replace('[actualLength]', error.actualLength);
                break;
            case 'minlength':
                message = langage.TRANSLATE.minlength.replace('[requiredLength]', error.requiredLength);
                message = message.replace('[actualLength]', error.actualLength);
                break;
            case 'required':
                message = langage.TRANSLATE.required;
                break;
            case 'pattern':
                if (error.requiredPattern == '^' + number_pattern + '$') {
                    message = langage.TRANSLATE.numberPattern;
                } else if (error.requiredPattern == '^' + date_ddmmyyyy_pattern + '$') {
                    message = langage.TRANSLATE.date_ddmmyyyy_Pattern;
                }
                break;
        }

        message = message.replace('[field]', header);
        return message;
    }
}