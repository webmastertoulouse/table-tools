import { Component, OnInit, Input, ElementRef, NgZone } from '@angular/core';
import { Column } from '../../models/column';
import { langage } from '../../config/langage';
import { TableToolsComponent } from '../table/table.component';
import {delay} from '../../shared/utils';
import { FormControl } from '@angular/forms';
import { ValidatorService } from '../../services/validator.service';
import { MessageService } from 'primeng/api';
import { select } from '../../shared/select';
import { fr } from '../../config/locale';

@Component({
  selector: '[NsiTableCell]',
  templateUrl: './table-cell.component.html',
  styleUrls: ['./table-cell.component.scss'],
  providers: [MessageService]
})
export class TableCellComponent implements OnInit {

  @Input() col: Column;
  @Input() value: any;

  private _value: any;

  private _delayBeforeUpdate: number = 1500;

  public LANGAGE: any = langage;
  
  public toasterKey: string = "tableCell";

  public locale = fr;

  constructor(private _table: TableToolsComponent,
    private _validatorService: ValidatorService,
    private _messageService: MessageService,
    private _el: ElementRef,
    private _zone: NgZone) {
  }

  ngOnInit(): void {
    this._zone.runOutsideAngular(() => {
      this._value = Object.assign({}, this.value);
      setTimeout(() => {
        this._castCheckbox();
        this._addNullValue2Select();
        
        /*if (this.col.valueChange instanceof Function) {
          this.value = this.col.valueChange(this.col, this.value);
        }*/
        
        this._value = Object.assign({}, this.value);
  
      }, 0);
  
      if (this._table.showLog === true) {
        // console.log('TableCellComponent::ngOnInit::value', this.value);
      }
    })
  }

  onUpdate(rowData: any, col: Column) {
    this._zone.runOutsideAngular(() => {
      if (col.inputType == 'checkbox') {
        if (this._controlForm(rowData)) {
          this._table.tableService.onUpdateCell({ previousData: this._value, rowData: rowData, col: col });
          this._assignNewValue();
        }
      } else {
        if (col.valueChange instanceof Function) {
          rowData = col.valueChange(col, rowData);
        }
  
        delay(() => {
          if (this._controlForm(rowData)) {
            this._table.tableService.onUpdateCell({ previousData: this._value, rowData: rowData, col: col });
            this._assignNewValue();
            this.onEditSopped(col.field, rowData);
            // this._triggerClick();
          }
        }, col.updateDelay ? col.updateDelay : this._delayBeforeUpdate);
      }
    });
  }

  onAutoComplete(rowData: any, col: Column) {
    this._zone.runOutsideAngular(() => {
      if (rowData.query != '') {
        this._table.tableService.onAutoCompleteCell({ rowData: rowData, col: col });
        this._assignNewValue();
      }
    });
  }

  /**
   * Get a dropdown value
   */
  getDropdownValue(col: Column, value: any) {
    return select.getLabel(value, col) || this.LANGAGE.EMPTY_COL;
  }

  /**
   * getSelectMultipleLabel
   * @param id 
   * @param col 
   */
  getSelectMultipleLabel(id: string, col: Column) {
    let label = col.options.filter(o => {
      if ((col.dataKey && o[col.dataKey] && o[col.dataKey].toLowerCase() == id.toLowerCase()) 
        || (o.value && o.value.toLowerCase() == id.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    });

    return (label.length > 0) ? ((col.keyField && label[0][col.keyField]) ? label[0][col.keyField] : label[0].label) : id;
  }

  /**
   * click event on click cell
   * @param field 
   * @param rowData 
   */
  clickEvent(field, rowData) {
    this._zone.runOutsideAngular(() => {
      this._table.onEditInit({ field: field, rowData: rowData });
    });
  }

  onEditSopped(field, value) {
    this._zone.runOutsideAngular(() => {
      this._table.onEditComplete({ field: field,  rowData: value });
    });
  }

  private _assignNewValue() {
    this._zone.runOutsideAngular(() => {
      setTimeout(() => this._value = Object.assign({}, this.value), 0);
    });
  }

  private _castCheckbox() {
    if (this.col.inputType == 'checkbox') {
      this.value[this.col.field] = (
        !this.value[this.col.field]
          || this.value[this.col.field] === 0
          || this.value[this.col.field] === null
          // || this.value[this.col.field] === false
          || this.value[this.col.field] === '0'
      ) ? false : true; 
    }
  }

  private _addNullValue2Select() {

    const value = this.col.dataKey ? this.col.dataKey : 'value';

    if (this.col.inputType == 'select' 
      && this.col.options 
      && this.col.options.length > 0
      && this.col.options.filter(v => v[value] === null).length == 0) {

      if (this.col.dataKey && this.col.keyField) {
        if (this.col.options[0] && this.col.options[0][this.col.dataKey] != '') {
          let option = [];
          option[this.col.dataKey] = '';
          option[this.col.keyField] = '';
          this.col.options.unshift(option);
        }
      } else {
        this.col.options.unshift({ value: null, label: null });
      }
    }
  }

  /* private _triggerClick() {
    document.body.click();
  } */

  /**
   * Control form before send
   * @param rowData
   */
  private _controlForm(rowData) {
    let messages = [];

    if (this.col.validators && this.col.validators.length > 0) {
      let control = new FormControl(rowData[this.col.field], this.col.validators);
      if (control.status.toLowerCase() == 'invalid') {
        messages.push(this._validatorService.translateFormControlError(this.col.header, control.errors));

        if (this._table.tableService.showLog === true) {
          console.log('error control : ', control.errors);
        }

        this._messageService.add({ severity: 'warn', summary: this.LANGAGE.ERROR_FIELD, detail: messages.join("<br />\n"), key: this.toasterKey });
        this._el.nativeElement.className = this._el.nativeElement.className.replace('error_cell', '');
        this._el.nativeElement.classList += ' error-cell';
        return false;
      }
    }

    this._el.nativeElement.className = this._el.nativeElement.className.replace('error-cell', '');
    return true;
  }
}