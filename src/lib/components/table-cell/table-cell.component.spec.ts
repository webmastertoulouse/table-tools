import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TableToolsComponent } from '../table/table.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableModule, Table as PTable, TableService as PTableService, EditableColumn, Table } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from 'primeng';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PaginatorModule } from 'primeng/paginator';
import { SpinnerModule } from 'primeng/spinner';
import { TableCellComponent } from './table-cell.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DebugElement, ElementRef, ChangeDetectorRef, Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { simpleColumns, tableCellColumn, listStandard_Grouped, tableCellColumn2, tableCellColumn3, tableCellColumn4, tableCellColumn5, tableCellColumn6, tableCellColumn7,tableCellColumn8, tableCellColumn9, RowData, RowData2 } from '../../jasmine-test/table-cell';

export class MockElementRef extends ElementRef {
    constructor() { super(null); }
}

export class MockChangeDetectorRef extends ChangeDetectorRef {
    checkNoChanges() {}
    detach() {}
    detectChanges() {}
    markForCheck() {}
    reattach() {}
}

export class MockMessageService extends MessageService {}

describe('TableCellComponent', () => {
  let component: TableCellComponent;
  let fixture: ComponentFixture<TableCellComponent>;
  let debugElement: DebugElement;
  
  let componentTableTools: TableToolsComponent;
  let fixtureNsiTable: ComponentFixture<TableToolsComponent>;
  
  let editableColumn: EditableColumn;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        TableModule,
        DropdownModule,
        AutoCompleteModule,
        CheckboxModule,
        ContextMenuModule,
        DynamicDialogModule,
        InputTextareaModule,
        InputTextModule,
        MultiSelectModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        PanelModule,
        TooltipModule,
        CommonModule,
        TableModule,
        SharedModule,
        RadioButtonModule,
        CalendarModule,
        ToastModule,
        ConfirmDialogModule,
        PaginatorModule,
        SpinnerModule,
        BrowserAnimationsModule
      ],
      declarations: [ 
        TableCellComponent
      ],
      providers: [ 
          TableToolsComponent, 
          ConfirmationService, 
          PTable,
          PTableService,
          { provide: ElementRef, useClass: MockElementRef },
          { provide: ChangeDetectorRef, useClass: MockChangeDetectorRef },
          MessageService
        ]
    }).compileComponents();

  }));

  
  beforeEach(async(() => {
    
    fixture                                 = TestBed.createComponent(TableCellComponent);
    component                               = fixture.componentInstance;
    debugElement                            = fixture.debugElement;

    componentTableTools                       = fixture.debugElement.injector.get(TableToolsComponent);

    componentTableTools.bodyTable             = { rows: [], columns: [], indexColumn: "id" };
    componentTableTools.value                 = listStandard_Grouped;
    componentTableTools.columns               = simpleColumns;

    const privateGetFilteredValueColumns    = spyOn<any>(componentTableTools, '_getFilteredValueColumns').and.callThrough();
    componentTableTools.selectedColumns       = privateGetFilteredValueColumns.call(componentTableTools, simpleColumns);

    const privateGetDropDownColumns         = spyOn<any>(componentTableTools, '_getDropDownColumns').and.callThrough();
    componentTableTools.cleanedColumns        = privateGetDropDownColumns.call(componentTableTools, simpleColumns);

    component.col                           = tableCellColumn;
    component.value                         = RowData;

    fixture.detectChanges();

    editableColumn                          = new EditableColumn(
        componentTableTools.pTable,
        fixture.debugElement.query(By.css('.ui-editable-column')),
        fixture.ngZone
    );

  }));

  afterEach(async(() => {
    fixture.destroy();
  }));
  
  it('Test updateCell subscribers (doit être appelé)', fakeAsync(() => {

    spyOn(componentTableTools.updateEvent, 'emit');

    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "FF";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    tick(2000);
    fixture.detectChanges();
    tick();

    expect(componentTableTools.updateEvent.emit).toHaveBeenCalled();
  }));
  
  it('Test updateCell subscribers (ne doit pas être appelé)', fakeAsync(() => {
    
    component.col = tableCellColumn2;

    spyOn(componentTableTools.updateEvent, 'emit');

    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "FFGGGDDD";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(componentTableTools.updateEvent.emit).not.toHaveBeenCalled();

  }));

  it('Test maxlength validator', fakeAsync(() => {
    
    component.col = tableCellColumn2;

    let spyMessageService = spyOn(MessageService.prototype, 'add');
    
    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "FFGGGDDD";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(spyMessageService).toHaveBeenCalledWith(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.maxlength.replace('[requiredLength]', 6).replace('[actualLength]', 8).replace('[field]', tableCellColumn2.header), 
        key: component.toasterKey
    }));
  }));

  it('Test minlength validator', fakeAsync(() => {
    
    component.col = tableCellColumn3;

    let spyMessageService = spyOn(MessageService.prototype, 'add');
    
    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "FFGD";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(spyMessageService).toHaveBeenCalledWith(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.minlength.replace('[requiredLength]', 6).replace('[actualLength]', 4).replace('[field]', tableCellColumn2.header), 
        key: component.toasterKey
    }));
  }));

  it('Test required validator', fakeAsync(() => {
    
    component.col = tableCellColumn4;

    let spyMessageService = spyOn(MessageService.prototype, 'add');
    
    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(spyMessageService).toHaveBeenCalledWith(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.required.replace('[field]', tableCellColumn2.header), 
        key: component.toasterKey
    }));
  }));

  it('Test numberPattern validator', fakeAsync(() => {
    
    component.col = tableCellColumn5;

    let spyMessageService = spyOn(MessageService.prototype, 'add');
    
    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "FORIFJRO58";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(spyMessageService).toHaveBeenCalledWith(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.numberPattern.replace('[field]', tableCellColumn2.header), 
        key: component.toasterKey
    }));
  }));

  it('Test date_ddmmyyyy_Pattern validator (faux)', fakeAsync(() => {
    
    component.col = tableCellColumn6;

    let spyMessageService = spyOn(MessageService.prototype, 'add');
    
    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "2019/02/01";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(spyMessageService).toHaveBeenCalledWith(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.date_ddmmyyyy_Pattern.replace('[field]', tableCellColumn2.header), 
        key: component.toasterKey
    }));
    
    inputEl.nativeElement.value = "2019-02-01";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(spyMessageService).toHaveBeenCalledWith(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.date_ddmmyyyy_Pattern.replace('[field]', tableCellColumn2.header), 
        key: component.toasterKey
    }));

  }));

  it('Test date_ddmmyyyy_Pattern validator (vrai)', fakeAsync(() => {
    
    component.col = tableCellColumn6;

    let spyMessageService = spyOn(MessageService.prototype, 'add');
    
    editableColumn.onClick(null);

    tick(200);
    fixture.detectChanges();
    tick();

    const inputEl: DebugElement = fixture.debugElement.query(By.css(".ui-editing-cell input"));
    inputEl.nativeElement.value = "31/12/2019";
    inputEl.nativeElement.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    tick(15000);

    expect(spyMessageService).not.toHaveBeenCalled();

  }));

  it('Test method getSelectMultipleLabel', () => {

    component.col = tableCellColumn7;
    component.value = RowData2;
    fixture.detectChanges();

    expect(component.getSelectMultipleLabel("value 1", tableCellColumn7)).toEqual("Label 1");
  });

  it('Test method getSelectMultipleLabel avec dataKey', () => {

    component.col = tableCellColumn8;
    component.value = RowData2;
    fixture.detectChanges();

    expect(component.getSelectMultipleLabel("value 3", tableCellColumn8)).toEqual("Label 3");
  });

  it('Test method _castCheckbox', fakeAsync(() => {

    component.col = tableCellColumn9;

    component.value = { id: "1", name: "AA", description: "ss", groupedField: "LALALA", selectmultipleField: [], checkboxField: '0' };
    tick(200); fixture.detectChanges(); tick();
    component.ngOnInit(); tick();
    expect(component.value.checkboxField).toEqual(false);

    component.value = { id: "1", name: "AA", description: "ss", groupedField: "LALALA", selectmultipleField: [], checkboxField: null };
    tick(200); fixture.detectChanges(); tick();
    component.ngOnInit(); tick();
    expect(component.value.checkboxField).toEqual(false);

    component.value = { id: "1", name: "AA", description: "ss", groupedField: "LALALA", selectmultipleField: [], checkboxField: 0 };
    tick(200); fixture.detectChanges(); tick();
    component.ngOnInit(); tick();
    expect(component.value.checkboxField).toEqual(false);

    component.value = { id: "1", name: "AA", description: "ss", groupedField: "LALALA", selectmultipleField: [], checkboxField: '1' };
    tick(200); fixture.detectChanges(); tick();
    component.ngOnInit(); tick();
    expect(component.value.checkboxField).toEqual(true);

  }));

});