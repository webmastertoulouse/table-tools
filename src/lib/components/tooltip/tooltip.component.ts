import { Component, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { langage } from '../../config/langage';
import { TableToolsComponent } from '../table/table.component';
import { Column } from '../../models/column';
import { Observable } from 'rxjs';
import { ContextMenuHeader } from '../../models/cm-header';
import { DomHandler } from 'primeng/dom';

@Component({
  selector: 'tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnDestroy {

  public subscription: Subscription;

  public subscriptionColHeader: Subscription;

  public LANGAGE: any = langage;

  public tooltip: any = { text: "", onEvent: "focus", position: "top" };

  private _subscriberCallback: any;

  private _uniqID: number;

  @ViewChild('tooltipElement') tooltipElement: ElementRef;

  constructor(private _tb: TableToolsComponent) {

    this.subscription = this._tb.tableService.tooltip$.subscribe((data: { col: Column, event: MouseEvent, value: any, tableCell: any }) => {

      if (this.tooltipElement && this.tooltipElement.nativeElement && event instanceof MouseEvent) {
      
        this.tooltipElement.nativeElement.blur();
        let tooltip = data.col.tooltip;

        if (tooltip && tooltip.text) {

          this.tooltip.text = tooltip.text;
          this._calcul(event, data.tableCell);
          this._show();

        } else if (tooltip && tooltip.callback) {
          this._callback(tooltip.callback, data, event, data.tableCell);
        }
      }

      if (this._tb.tableService.showLog === true) {
        console.log('TooltipComponent::tooltip$::data', data);
      }
    });

    this.subscriptionColHeader = this._tb.tableService.contextMenuHeader$.subscribe((data: ContextMenuHeader) => {
      if (this.tooltipElement) this._hide();

      if (this._tb.tableService.showLog === true) {
        console.log('TooltipComponent::contextMenuHeader$::data', data);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  /**
   * call callback from user
   * @param cb 
   * @param data 
   */
  private _callback(cb, data, event: MouseEvent, tableCell): void {

    this.tooltip.text = '<i class="pi pi-spin pi-spinner" style="font-size: 3em"></i>';
    this._calcul(event, tableCell);
    this._show();

    if (this._subscriberCallback) {
      this._subscriberCallback.unsubscribe();
      this._subscriberCallback = null;
    }

    this._uniqID = new Date().getTime();

    let cb$ = new Observable(observer => {
      observer.next({
        cb: cb(data.col, data.value),
        id: this._uniqID
      });
    });

    this._subscriberCallback = cb$.subscribe({
      next: (promise: any) => {
        promise.cb.then((result: any) => {
          if (promise.id == this._uniqID) {
            this.tooltip.text = result;
          }
        });
      },
      error: err => {
        if (this._tb.tableService.showLog === true) {
          // console.log(err);
        }
      }
    });
  }

  /**
   * calcul position tooltip
   * @param event 
   */
  private _calcul(event, tableCell): void {

      setTimeout(() => {
        let position = this._position(event);
  
        // Si la tooltip est à afficher dans un tableau contenu dans un p-dialog 
        // Les calculs sont différents
        let pDialog = tableCell._el.nativeElement.closest('p-dialog .ui-dialog');
        if (pDialog) {
          let offset = DomHandler.getOffset(pDialog);
          this.tooltipElement.nativeElement.style.position  = 'absolute';
          this.tooltipElement.nativeElement.style.left      = `${(position.left - offset.left)}px`;
          this.tooltipElement.nativeElement.style.top       = `${(position.top - offset.top)}px`;
        } else {
          this.tooltipElement.nativeElement.style.position  = 'absolute';
          this.tooltipElement.nativeElement.style.left      = `${position.left}px`;
          this.tooltipElement.nativeElement.style.top       = `${position.top}px`;
          setTimeout(() => this.tooltipElement.nativeElement.focus(), 200);
        }
      }, 0);
  }

  /**
   * Calcul top position tooltip
   * @param event 
   */
  private _position(event): { top: number, left: number } {
    let pageX = event.pageX;
    let l = 69+10; // 69: (ce n'est pas une année érotique mais) la taille du tooltip divisée par 2 + 10px de margin
    let left = pageX + l;

    if (left > window.innerWidth) {
      return { top: event.pageY, left: (pageX - l) };
    } else {
      return { top: event.pageY, left: left };
    }
  }

  /**
   * show tooltip
   */
  private _show(): void {
    setTimeout(() => this.tooltipElement.nativeElement.focus(), 0);
  }

  /**
   * hide tooltip
   */
  private _hide(): void {
    this.tooltipElement.nativeElement.blur();
  }
}