import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TableToolsComponent } from './table.component';
import { TableToolsModule } from 'table-tools';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableModule, Table as PTable, TableService as PTableService } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from 'primeng';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PaginatorModule } from 'primeng/paginator';
import { SpinnerModule } from 'primeng/spinner';
import { TableCellComponent } from '../table-cell/table-cell.component';
import { CmHeaderComponent } from '../cm-header/cm-header.component';
import { SortAzComponent } from '../filter/sort-az/sort-az.component';
import { FilterDateComponent } from '../filter/filter-date/filter-date.component';
import { ContextMenuHeader } from '../../directives/cm-header.directive';
import { FilterValueComponent } from '../filter/filter-value/filter-value.component';
import { FilterMultiselectComponent } from '../filter/filter-multiselect/filter-multiselect.component';
import { TooltipDirective } from '../../directives/tooltip.directive';
import { TooltipComponent } from '../tooltip/tooltip.component';
import { AddModalComponent } from '../add-modal/add-modal.component';
import { OnRejectDirective } from '../../directives/on-reject.directive';
import { OnUpdateDirective } from '../../directives/on-update.directive';
import { OnAddDirective } from '../../directives/on-add.directive';
import { OnDeleteDirective } from '../../directives/on-delete.directive';
import { OnDblclickRowDirective } from '../../directives/on-dblclick-row.directive';
import { ToggleComponent } from '../filter/toggle/filter-toggle.component';
import { ConfirmationService } from 'primeng/api';
import { DebugElement } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { customEvent } from '../../const';
import { By } from '@angular/platform-browser';
import { Column } from '../../models/column';
import { rowTest1, rowTest2, columns, columnTestSelect1, simpleColumns, listStandard_Sort_A_Z, listStandard_Sort_A_Z__SORTED, listStandard_Sort_Date, listStandard_Sort_Date__SORTED, OnsortTestValue_AZ, OnsortTestValue_DATE, OnsortTestValue_STRING, listStandard_Sort_String__SORTED, listStandard_Sort_String, listStandard_Sort_Multiselect, listStandard_Sort_Multiselect__SORTED, OnsortTestValue_MULTISELECT, listStandard_Sort_Multiselect2, listStandard_Sort_Multiselect2__SORTED, OnsortTestValue_MULTISELECT2, OnsortTestValueWhithCallback_DATE, listStandard_Sort_DateWithCallback__SORTED, OnsortTestValueWhithCallback_STRING, listStandard_Sort_StringWithCallback__SORTED, OnsortTestValueWhithCallback_MULTISELECT, listStandard_Sort_MultiselectWithCallback__SORTED, listStandard_Sort_A_Z_OnpageChange_Result, listStandard_Grouped, simpleGroupedColumns, iconColumns } from '../../jasmine-test/table';
import { getARightClick } from '../../jasmine-test/event-function';

describe('TableToolsComponent', () => {
  let component: TableToolsComponent;
  let fixture: ComponentFixture<TableToolsComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TableToolsModule,
        CommonModule,
        FormsModule,
        TableModule,
        DropdownModule,
        AutoCompleteModule,
        CheckboxModule,
        ContextMenuModule,
        DynamicDialogModule,
        InputTextareaModule,
        InputTextModule,
        MultiSelectModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        PanelModule,
        TooltipModule,
        CommonModule,
        TableModule,
        SharedModule,
        RadioButtonModule,
        CalendarModule,
        ToastModule,
        ConfirmDialogModule,
        PaginatorModule,
        SpinnerModule,
        BrowserAnimationsModule
      ],
      declarations: [ 
        TableCellComponent,
        TableToolsComponent,
        CmHeaderComponent, 
        SortAzComponent, 
        FilterDateComponent,
        ContextMenuHeader,
        FilterValueComponent,
        FilterMultiselectComponent,
        TooltipDirective,
        TooltipComponent,
        AddModalComponent,
        OnRejectDirective,
        OnUpdateDirective,
        OnAddDirective,
        OnDeleteDirective,
        OnDblclickRowDirective,
        ToggleComponent
      ],
      providers: [ ConfirmationService, PTable, PTableService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableToolsComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
  });

  afterEach(async(() => {
    fixture.destroy();
  }));

  it('Ajout permit', () => {
    component.bodyTable = { rows: [], columns: [], canAdd: true, indexColumn: "id" };
    component.onAdd();
    fixture.detectChanges();

    expect(component.displayAddDialog).toBe(true);
  });

  it('Test DOM pour l\'affichage de la modal add', async(() => {
    component.bodyTable = { rows: [], columns: [], canAdd: true, indexColumn: "id" };
    debugElement = fixture.debugElement.query(By.css('add-modal'));

    fixture.detectChanges();
    expect(debugElement.nativeElement.outerHTML).toMatch('ng\-reflect\-display\-add\-dialog\=\"false\"');

    component.onAdd();
    fixture.detectChanges();
    expect(debugElement.nativeElement.outerHTML).toMatch('ng\-reflect\-display\-add\-dialog\=\"true\"');
  }));

  it('Ajout non permis', () => {
    component.bodyTable = { rows: [], columns: [], canAdd: false, indexColumn: "id" };
    component.onAdd();
    fixture.detectChanges();

    expect(component.displayAddDialog).toBe(false);
  });

  it('bodyTable.canAdd === false & bodyTable.controlAdd === true, test emit customEvent', () => {
    component.bodyTable = { rows: [], columns: [], canAdd: false, controlAdd: true, indexColumn: "id" };

    spyOn(component.customEvent, 'emit');

    component.onAdd();
    expect(component.customEvent.emit).toHaveBeenCalled();
  });

  it('bodyTable.canAdd === false & bodyTable.controlAdd === true, test value customEvent', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], canAdd: false, controlAdd: true, indexColumn: "id" };

    component.customEvent.subscribe(value => {
      expect(value).toEqual(jasmine.objectContaining({
        id: customEvent.__ADD__,
        value: customEvent.__ADD__
      }));
    });

    component.onAdd();
  }));

  it('onClose adModal', () => {
    component.bodyTable = { rows: [], columns: [], canAdd: false, controlAdd: true, indexColumn: "id" };
    component.displayAddDialog = true;
    component.onCloseAdd();

    expect(component.displayAddDialog).toBe(false);
  });

  it('Test onAddRow avec canAdd == false', () => {
    component.bodyTable = { rows: [], columns: [], canAdd: false, indexColumn: "id" };

    spyOn(component.addEvent, 'emit');
    component.onAddRow({ name: 'Test name', description: 'Test description' });
    expect(component.addEvent.emit).not.toHaveBeenCalled();
  });

  it('Test onAddRow avec canAdd == true', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], canAdd: true, indexColumn: "id" };

    let dataToAdd = { name: 'Test name', description: 'Test description' };

    component.addEvent.subscribe(value => {
      expect(value).toEqual(jasmine.objectContaining(dataToAdd));
    });

    component.onAddRow(dataToAdd);
  }));

  it('Test onUpdate sans id', () => {
    component.bodyTable = { rows: [], columns: [], canEdit: false, indexColumn: "id" };   

    spyOn(component.updateEvent, 'emit');
    component.onUpdate(<any>{ rowData: <any>{ name: 'Test name', description: 'Test description' } });
    expect(component.updateEvent.emit).not.toHaveBeenCalled();
  });
  
  it('Test onUpdate avec id', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], canEdit: false, indexColumn: "id" };   

    let dataToUpd = <any>{ rowData: <any>{ id: 65656, name: 'Test name', description: 'Test description' } };

    component.updateEvent.subscribe(value => {
      expect(value).toEqual(jasmine.objectContaining({ id: 65656, name: 'Test name', description: 'Test description' }));
    });

    component.onUpdate(dataToUpd);

  }));

  it('test isAddRow, lorsqu\'une ligne est considérée comme une ligne valide à mettre à jour ou pas', () => {
    component.bodyTable = { rows: [], columns: [], canEdit: false, indexColumn: "id" };

    let data = <any>{ rowData: <any>{ name: 'Test name', description: 'Test description' } };

    let result = null;
    const _isAddRow = spyOn<any>(component, '_isAddRow').and.callThrough();
    result = _isAddRow.call(component, data);
    expect(result).toEqual(true);

    data.rowData.id = 65656;
    result = _isAddRow.call(component, data);
    expect(result).toEqual(false);

  });

  it('test clean select function quand onUpdate est appelé', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], canEdit: false, indexColumn: "id" };
    component.columns = columns;

    let dataToUpd = <any>{ rowData: <any>rowTest1, col: <Column>columnTestSelect1 };

    component.updateEvent.subscribe(value => expect(rowTest2).toEqual(jasmine.objectContaining(value)));

    component.onUpdate(dataToUpd);
  }));

  it('Test onAutoComplete event emitter', () => {
    component.bodyTable = { rows: [], columns: [], canEdit: false, indexColumn: "id" };   

    spyOn(component.autoCompleteEvent, 'emit');
    component.onAutoComplete(<any>{ rowData: <any>{ query: 'Test name' }, col: columnTestSelect1 });
    expect(component.autoCompleteEvent.emit).toHaveBeenCalled();
  });

  it('Test bodyTable.callbacks.onSort', fakeAsync(() => {
    let newTitle = 'new title';

    component.bodyTable = { rows: [], columns: [], config: { title: "Test title" },
      callbacks: {
        onSort: {
          title: (onSort) => new Promise((resolve, reject) => resolve(newTitle))
        }
      }, indexColumn: "id" 
    };

    component.dispatchOnSort({col: <Column>columnTestSelect1});

    fixture.detectChanges();
    tick(200);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      debugElement = fixture.debugElement.query(By.css('.title-table'));
      expect(debugElement.nativeElement.textContent.trim()).toEqual(newTitle);
    });

    tick();
  }));

  it('Test bodyTable.callbacks.onSort A-Z', () => {
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_A_Z;
    component.columns = columns;
    component.dispatchOnSort(OnsortTestValue_AZ);
    expect(component.value).toEqual(listStandard_Sort_A_Z__SORTED);
  });

  it('Test bodyTable.callbacks.onSort DATE', () => {
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_Date;
    component.columns = columns;
    component.dispatchOnSort(OnsortTestValue_DATE);
    expect(component.value).toEqual(listStandard_Sort_Date__SORTED);
  });

  it('Test bodyTable.callbacks.onSort STRING', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_String;
    component.columns = columns;
    component.dispatchOnSort(OnsortTestValue_STRING);

    tick(500);
    expect(component.value).toEqual(listStandard_Sort_String__SORTED);
    tick();
  }));

  it('Test bodyTable.callbacks.onSort MULTISELECT', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_Multiselect;
    component.columns = columns;
    component.dispatchOnSort(OnsortTestValue_MULTISELECT);

    tick(500);
    expect(component.value).toEqual(listStandard_Sort_Multiselect__SORTED);
    tick();
  }));

  it('Test bodyTable.callbacks.onSort MULTISELECT 2 (items séparés par des virgules', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_Multiselect2;
    component.columns = columns;
    component.dispatchOnSort(OnsortTestValue_MULTISELECT2);

    expect(component.value).toEqual(listStandard_Sort_Multiselect2__SORTED);
  }));

  it('Test callback on filter DATE', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_Date;
    component.columns = columns;
    component.dispatchOnSort(OnsortTestValueWhithCallback_DATE);
    
    tick(500);
    expect(component.value).toEqual(listStandard_Sort_DateWithCallback__SORTED);
    tick();
  }));

  it('Test callback on filter MULTISELECT', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_Multiselect2;
    component.columns = columns;
    component.dispatchOnSort(OnsortTestValueWhithCallback_MULTISELECT);
    
    tick(500);
    expect(component.value).toEqual(listStandard_Sort_MultiselectWithCallback__SORTED);
    tick();
  }));

  it('Test callback context menu on show (contextMenuSelection())', fakeAsync(() => {
    component.bodyTable = { rows: [], columns: [],
      callbacks: {
        contextMenu: {
          show: (value) => false
        }
      }, indexColumn: "id"
    };

    component.contextMenuSelection({id: 12, row: 'any', slected: true});

    fixture.detectChanges();
    tick(200);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      debugElement = fixture.debugElement.query(By.css('.ui-contextmenu'));
      expect(debugElement.nativeElement.style.display).toEqual('none');
      expect(debugElement.nativeElement.style.visibility).toEqual('hidden');
    });

    tick();
  }));

  it('Test changement de tableau au click sur paginator (onPageChange())', fakeAsync(() => {
    component.bodyTable = { rows: listStandard_Sort_String, columns: columns,
      paginator: {
        rows: 3
      }, indexColumn: "id" 
    };

    component.onPageChange({page: 1, first: 3, rows: 3, pageCount: 2});
    
    tick(500);
    expect(component.rows).toEqual(3);
    expect(component.first).toEqual(3);
    tick();
  }));

  it('Test subscription doubleClick row (onDblClickRow())', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_A_Z;
    component.columns = columns;

    spyOn(component.customEvent, 'emit');

    fixture.detectChanges();

    const doubleClickEl: DebugElement[] =  fixture.debugElement.queryAll(By.css(".ui-table table tbody tr"));
    doubleClickEl[4].triggerEventHandler("dblclick", new MouseEvent("dblclick"));

    fixture.detectChanges();
    
    expect(component.customEvent.emit).toHaveBeenCalledWith(jasmine.objectContaining({
      id: customEvent.__DBLCLICK__,
      value: listStandard_Sort_A_Z[4]
    }));

    tick();
  }));

  it('Test call onEditInit quand une input vient de paraître en mode édition (onEditInit())', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_A_Z;
    component.columns = simpleColumns;
    component.bodyTable.callbacks = { onEditInit: (event) => {} }

    spyOn(component, 'onEditInit');
    fixture.detectChanges();

    const clickEl: DebugElement[] =  fixture.debugElement.queryAll(By.css(".ui-table table tbody tr > td .ui-editable-column span"));
    clickEl[3].triggerEventHandler("click", null);

    fixture.detectChanges();
    expect(component.onEditInit).toHaveBeenCalledWith({ field: "name", rowData: listStandard_Sort_A_Z[3] });

    tick();
  }));

  it('Test groupement de colonne', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Grouped;
    component.columns = simpleGroupedColumns;
    fixture.detectChanges();

    component.onGroupColumns({ field: "groupedField", header: "groupedField", inputType: "input" });
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component['_groupedColumns'][0]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'AA', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][4]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'BB', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][7]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'CC', inputType: 'grouped_edit'}));

    component.onUnGroupColumn();
    
    tick(500);
    fixture.detectChanges()
    tick();
    
    expect(component.columns[0][3].isGrouped).toBe(false);
    expect(component['_groupedColumns']).toEqual(null);

  }));

  it('Test getColSpan()', () => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Grouped;
    component.columns = simpleGroupedColumns;
    fixture.detectChanges();

    expect(component.getColSpan() === 4).toBe(true);
  });

  it('Test Montrer/cacher les lignes d\'un groupement de colonne', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Grouped;
    component.columns = simpleGroupedColumns;
    fixture.detectChanges();

    component.onGroupColumns({ field: "groupedField", header: "groupedField", inputType: "input" });
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component['_groupedColumns'].length.toString()).toBe('9');
    
    component.toggleGrouped('groupedField', 'BB');
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component['_toggleGroupedColumnsKeys']['BB'].length.toString()).toEqual('2');
    expect(component['_toggleGroupedColumnsKeys']['BB']).toEqual([
      {id: '6', name: 'OP FAUTPASREVER AGEE (AMPA)', description: 'POKDoid dspoids disopdifruooi fipfi zoef ep', groupedField: 'BB'}, 
      {id: '2', name: 'AM BLABLA AGEE (AMPA)', description: 'POKDoid dspoids disopdifruooi fipfi zoef ep', groupedField: 'BB'}
    ]);
    expect(component['_groupedColumns'].length === 7).toBe(true);
    expect(component['_groupedColumns'][4]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'BB', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][5]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'CC', inputType: 'grouped_edit'}));
    
    component.toggleGrouped('groupedField', 'BB');
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component['_toggleGroupedColumnsKeys']['BB'].length.toString()).toEqual('0');
    expect(component['_groupedColumns'].length.toString()).toBe('9');
    expect(component['_groupedColumns'][5]).toEqual(jasmine.objectContaining({id: '6', name: 'OP FAUTPASREVER AGEE (AMPA)', description: 'POKDoid dspoids disopdifruooi fipfi zoef ep', groupedField: 'BB'}));
    expect(component['_groupedColumns'][7]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'CC', inputType: 'grouped_edit'}));
    
    component.toggleGrouped('groupedField', 'AA');
    component.toggleGrouped('groupedField', 'CC');
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component['_toggleGroupedColumnsKeys']['AA'].length.toString()).toEqual('3');
    expect(component['_toggleGroupedColumnsKeys']['CC'].length.toString()).toEqual('1');
    expect(component['_groupedColumns'].length.toString()).toBe('5');
    expect(component['_groupedColumns'][0]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'AA', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][1]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'BB', inputType: 'grouped_edit'}));
  }));

  it('Test expand or hide all rows', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Grouped;
    component.columns = simpleGroupedColumns;
    
    let clickEl = debugElement.query(By.css("#sortrows-up"));
    expect(clickEl).toBe(null);

    component.onGroupColumns({ field: "groupedField", header: "groupedField", inputType: "input" });
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component['_groupedColumns'].length.toString()).toBe('9');
    expect(component['_groupedColumns'][0]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'AA', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][4]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'BB', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][7]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'CC', inputType: 'grouped_edit'}));

    clickEl = debugElement.query(By.css("#sortrows-up"));
    clickEl.triggerEventHandler("click", null);
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component['_groupedColumns'].length.toString()).toBe('3');
    expect(component['_groupedColumns'][0]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'AA', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][1]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'BB', inputType: 'grouped_edit'}));
    expect(component['_groupedColumns'][2]).toEqual(jasmine.objectContaining({header: 'groupedField', field: 'groupedField', key: 'CC', inputType: 'grouped_edit'}));
    expect(component['_toggleGroupedColumnsKeys']['AA'].length.toString()).toEqual('3');
    expect(component['_toggleGroupedColumnsKeys']['BB'].length.toString()).toEqual('2');
    expect(component['_toggleGroupedColumnsKeys']['CC'].length.toString()).toEqual('1');

  }));

  it('Test isAListingColumn()', () => {

    expect(component.isAListingColumn({field: 'name', header: "Nom", inputType: "select"})).toBe(true);
    expect(component.isAListingColumn({field: 'name', header: "Nom", inputType: "select", display: false})).toBe(false);
    expect(component.isAListingColumn({inputType: "select", header: "DropDown"})).toBe(false);
  });

  it('Test _changeIsGroupedValue()', () => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Grouped;
    component.columns = simpleGroupedColumns;

    component['_changeIsGroupedValue']({ field: "description", header: "description", inputType: "input" }, true);
    expect(simpleGroupedColumns[0][2].isGrouped).toBe(true);

    component['_changeIsGroupedValue']({ field: "description", header: "description", inputType: "input" }, false);
    expect(simpleGroupedColumns[0][2].isGrouped).toBe(false);
  });

  it('Test _onAfterValueChange()', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_A_Z_OnpageChange_Result;
    component.columns = iconColumns;
    fixture.detectChanges();
    
    tick(500);
    fixture.detectChanges()
    tick();

    expect(component.value[0].checked).toEqual('<i class="fas fa-times-circle"></i>');
  }));

  it('Test _getFilteredValueColumns()', () => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_A_Z_OnpageChange_Result;
    component.columns = columns;

    const filteredValueColumns = component['_getFilteredValueColumns'](columns);
    expect(filteredValueColumns).toEqual([
      'id', 
      'code_service', 
      'type_service', 
      'name', 
      'product_name', 
      'type_unit_name', 
      'type_cadre_intervention_name', 
      'with_kilometer', 
      'with_duration', 
      'is_holiday',
      'tg_type_transaction', 
      'is_not_synced', 
      'is_reconcile', 
      'is_sequence'
    ]);
  });

  it('Test _getDropDownColumns()', () => {
    
    const dropDownColumns = component['_getDropDownColumns'](columns);
    expect(dropDownColumns).toEqual([
      {label: 'ID', value: 'id'}, 
      {label: 'Code service', value: 'code_service'}, 
      {label: 'Type de service', value: 'type_service'}, 
      {label: 'Libellé', value: 'name'}, 
      {label: 'Nom', value: 'product_name'}, 
      {label: 'Unité', value: 'type_unit_name'}, 
      {label: 'Mode d\'intervention', value: 'type_cadre_intervention_name'}, 
      {label: 'Pas de kms ?', value: 'with_kilometer'}, 
      {label: 'Avec une durée ?', value: 'with_duration'}, 
      {label: 'Congé ?', value: 'is_holiday'}, 
      {label: 'Type transaction', value: 'tg_type_transaction'}, 
      {label: 'Pas synchro mobile', value: 'is_not_synced'}, 
      {label: 'Est rapproché au planning ?', value: 'is_reconcile'}, 
      {label: 'Est séquencé ?', value: 'is_sequence'}
    ]);
  });

  it('Test bodyTable.contextMenu pour les lignes du tableau', fakeAsync(() => {
    
    component.bodyTable = { 
      rows: [], 
      columns: [], 
      contextMenu: {delete: true,
        actions: [
          { id: "testAction", name: "Test action", icon: "fas fa-truck-loading" }, 
          { id: "testAction2", name: "Test action 2", icon: "fas fa-upload" }
        ]
      }, indexColumn: "id"
    };
    component.value = listStandard_Sort_A_Z;
    component.columns = columns;

    tick(500);
    fixture.detectChanges();
    tick();

    const rightClick = getARightClick();

    const tableRowsEl: DebugElement[] = fixture.debugElement.queryAll(By.css(".ui-table table tbody tr"));
    tableRowsEl[1].nativeElement.dispatchEvent(rightClick);
    
    tick(500);
    fixture.detectChanges();
    tick();

    const contextMenuList: DebugElement[] = fixture.debugElement.queryAll(By.css(".ui-contextmenu ul.ui-menu-list li.ui-menuitem"));
    expect(contextMenuList[0].nativeElement.textContent.trim().toLowerCase()).toEqual('supprimer');
    expect(contextMenuList[1].nativeElement.textContent.trim()).toEqual('Test action');
    expect(contextMenuList[2].nativeElement.textContent.trim()).toEqual('Test action 2');
  }));

  it('Test delete row via le contextMenu', fakeAsync(() => {
    
    component.bodyTable = { rows: [], columns: [], indexColumn: "id" };
    component.value = listStandard_Sort_A_Z;
    component.columns = columns;
    component.bodyTable.contextMenu = { delete: true };
    component.bodyTable.canDelete = true;

    spyOn(component.deleteEvent, 'emit');

    tick(500);
    fixture.detectChanges();
    tick();

    const rightClick = getARightClick();

    const tableRowsEl: DebugElement[] = debugElement.queryAll(By.css(".ui-table table tbody tr"));
    tableRowsEl[1].nativeElement.dispatchEvent(rightClick);
    fixture.detectChanges();

    const contextMenuList: DebugElement = debugElement.query(By.css(".ui-contextmenu ul.ui-menu-list li.ui-menuitem a.ui-menuitem-link"));
    contextMenuList.nativeElement.click();
    fixture.detectChanges();

    const debugEl: HTMLElement = fixture.debugElement.nativeElement;
    const btns: NodeListOf<HTMLElement> = debugEl.querySelectorAll('.ui-confirmdialog p-footer button');
    btns[1].click();

    expect(component.deleteEvent.emit).toHaveBeenCalled();

  }));

});
