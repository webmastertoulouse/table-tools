import { Component, OnInit, Input, OnChanges, SimpleChanges, AfterViewInit, OnDestroy, EventEmitter, Output, ViewChild, DoCheck, KeyValueDiffers, KeyValueDiffer, KeyValueChangeRecord, NgZone } from '@angular/core';
import { Table as PTable, TableService as PTableService } from 'primeng/table';
import { BodyTable } from '../../models/body-table';
import { TableService } from '../../services/table.service';
import { Subscription, throwError } from 'rxjs';
import { langage } from '../../config/langage';
import { MenuItem, ConfirmationService, DialogService, Message } from 'primeng';
import { ContextMenu } from 'primeng/contextmenu';
import { FormValidator } from '../../models/form-validator';
import { Column } from '../../models/column';
import { OnSort } from '../../models/on-sort';
import { ContextMenuHeader } from '../../models/cm-header';
import { Callbacks } from '../../models/callbacks';
import { SortType } from '../../models/sort-type';
import { ContextMenuRow } from '../../models/cm-row';
import { ConfigList } from '../../models/config-list';
import { select } from '../../shared/select';
import { customEvent } from '../../const';
import { groupedColumn } from '../../models/grouped-column';

@Component({
  selector: 'table-tools',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [
    DialogService,
    TableService,
    PTable,
    PTableService,
    ConfirmationService
  ]
})
export class TableToolsComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges, DoCheck {

  public LANGAGE: any = langage;

  @Input() showLog: boolean = false;
  @Input() bodyTable: BodyTable = { indexColumn: null, columns: [[]], rows: [] };

  @Output() addEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() updateEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() autoCompleteEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() customEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() resetFilterEvent: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild("cm", { static: true }) public contextMenu: ContextMenu;
  
  /**
   * Subscriptions
   */
  private _onUpdateSubscription: Subscription;
  private _onAutoCompleteSubscription: Subscription;
  private _onAddRowSubscription: Subscription;

  /**
   * Paramètre de configuration général pour le tableau
   * Context menu header items
   */
  public selectedRow: any;
  public contextMenuItems: MenuItem[] = [];
  
  /**
   * Relatif à l'ajout d'éléments
   */
  public displayAddDialog: boolean = false;
  public validatorAddFields: FormValidator[] = [];
  public msgsAdd: Message[] = [];

  /**
   * Pour cacher/montrer les colonnes
   */
  public selectedColumns: any[] = [];
  public cleanedColumns: any[] = [];

  /**
   * Détection des valeurs qui changent dans bodyTable
   */
  private _differ: KeyValueDiffer<any, any>;
  
  /**
   * Relatif au groupement des colonnes
   * 
   * _groupedColumns: variable ou se trouvent les items de la liste correctement ordonnés
   */
  private _groupedColumns: any[] = null;
  private _toggleGroupedColumnsKeys: any[] = [];
  public groupedRowExpand: boolean = null;

  /**
   * Paginator
   */
  private _paginator = <{
    first: number,
    rowsPerPageOptions: number[],
    pageLinkSize: number,
    rows: number,
    showCurrentPageReport: boolean,
    currentPageReportTemplate: string
  }>{
    first: 0,
    rowsPerPageOptions: [5, 10, 20, 50],
    pageLinkSize: 5,
    rows: 50,
    showCurrentPageReport: true,
    currentPageReportTemplate: `${this.LANGAGE.PAGINATOR.NUMBER_ELEMENT} {currentPage} ${this.LANGAGE.PAGINATOR.ON} {totalPages}`
  };

  /**
   * Si les colonnes du header table ont des cellules imbriquées (rowspan, colspan)
   * Obligé d'ordonner les tableaux de cellules (bodyTable.columns)
   */
  public bodyColumns: Column[] = [];

  constructor(public tableService: TableService,
    private _confirmationService: ConfirmationService,
    private _pTable: PTable,
    private _differs: KeyValueDiffers,
    private _ngZone: NgZone) {
    this._initSubscribers();
    this.value = this.value || [];
    this.columns = this.columns || [];

    this._differ = this._differs.find(this.bodyTable).create();
  }

  /**
   * return pTable
   */
  get pTable(): PTable {
    return this._pTable;
  }

  set indexColumn(indexColumn: string) {
    this.bodyTable.indexColumn = indexColumn;
  }

  get indexColumn(): string {
    return this.bodyTable.indexColumn;
  }

  /**
   * set rows pTable
   */
  set value(rows: any[]) {
    this.pTable.value  = rows;
  }

  /**
   * get rows pTable
   */
  get value(): any[] {
    return this.pTable.filteredValue || this._groupedColumns || this.pTable.value;
  }

  /**
   * get groupedColumns
   */
  get groupedColumns() {
    return this._groupedColumns;
  }

  /**
   * set filteredValue pTable
   */
  set filteredValue(rows: any[]) {
    this.pTable.filteredValue  = rows;
  }

  /***
   * get filteredValue pTable
   */
  get filteredValue() {
    return this.pTable.filteredValue;
  }

  /**
   * set columns pTable
   */
  set columns(columns: Array<Column[]>) {
    this.pTable.columns= columns;
  }

  /**
   * get columns pTable
   */
  get columns(): Array<Column[]> {
    return this.pTable.columns;
  }

  /**
   * set sortField pTable
   */
  set sortField(sortField: string) {
    this.pTable.sortField= sortField;
  }

  /**
   * get sortField pTable
   */
  get sortField(): string {
    return this.pTable.sortField;
  }

  /**
   * set sortOrder pTable
   */
  set sortOrder(sortOrder: number) {
    this.pTable.sortOrder= sortOrder;
  }

  /**
   * get sortOrder pTable
   */
  get sortOrder(): number {
    return this.pTable.sortOrder;
  }

  /**
   * set sortMode pTable
   */
  set sortMode(sortMode: string) {
    this.pTable.sortMode= sortMode;
  }

  /**
   * get sortMode pTable
   */
  get sortMode(): string {
    return this.pTable.sortMode;
  }

  /**
   * set reorderableColumns pTable
   */
  set reorderableColumns(reorderableColumns: boolean) {
    this.pTable.reorderableColumns= reorderableColumns;
  }

  /**
   * get reorderableColumns pTable
   */
  get reorderableColumns(): boolean {
    return this.pTable.reorderableColumns;
  }

  /**
   * set resetPageOnSort pTable
   */
  set resetPageOnSort(resetPageOnSort: boolean) {
    this.pTable.resetPageOnSort= resetPageOnSort;
  }

  /**
   * get resetPageOnSort pTable
   */
  get resetPageOnSort(): boolean {
    return this.pTable.resetPageOnSort;
  }
  /**
   * get ContextMenuRow from bodyTable
   */
  get contextMenuRow(): ContextMenuRow {
    return (this.bodyTable && this.bodyTable.contextMenu) ? this.bodyTable.contextMenu : {};
  }
  
  /**
   * SET Callbacks définis par l'utilisateur
   */
  set callbacks(callbacks: Callbacks) {
    this.bodyTable.callbacks = callbacks;
  }

  /**
   * GET Callbacks définis par l'utilisateur
   */
  get callbacks(): Callbacks {
    return (this.bodyTable && this.bodyTable.callbacks) ? this.bodyTable.callbacks : {};
  }

  /**
   * configList définis par l'utilisateur
   */
  get configList(): ConfigList {
    return (this.bodyTable && this.bodyTable.config) ? this.bodyTable.config : {};
  }

  /**
   * get pTable paginator
   */
  get paginator(): boolean {
    return this.paginable;
  }

  get paginable(): boolean {
    return (
      this.bodyTable.paginator && this.bodyTable.paginator.rows && this.bodyTable.paginator.rows > 0 && this.bodyTable.paginator.rows < this.value.length
    );
  }

  /**
   * pTable paginator getTotalRecords
   */
  set totalRecords(totalRecords: number) {
    this.pTable.totalRecords = totalRecords;
  }

  /**
   * pTable paginator getTotalRecords
   */
  get totalRecords(): number {
    return this.pTable.totalRecords;
  }

  /**
   * set paginator first
   */
  set first(first: number) {
    this.pTable.first = first;
  }

  /**
   * get paginator first
   */
  get first(): number {
    return this.pTable.first || this._paginator.first;
  }

  /**
   * set paginator rowsPerPageOptions
   */
  set rowsPerPageOptions(rowsPerPageOptions: number[]) {
    this.pTable.rowsPerPageOptions = rowsPerPageOptions;
  }

  /**
   * get paginator rowsPerPageOptions
   */
  get rowsPerPageOptions(): number[] {
    return this.pTable.rowsPerPageOptions || this._paginator.rowsPerPageOptions;
            
  }

  /**
   * set paginator pageLinkSize
   */
  set pageLinkSize(pageLinkSize: number) {
    this.pTable.pageLinks = pageLinkSize;
  }

  /**
   * get paginator pageLinkSize
   */
  get pageLinkSize(): number {
    return this.pTable.pageLinks || this._paginator.pageLinkSize;
  }

  /**
   * get paginator pageLinkSize
   */
  get showCurrentPageReport(): boolean {
    return this._paginator.showCurrentPageReport;
  }

  /**
   * get paginator pageLinkSize
   */
  get currentPageReportTemplate(): string {
    return this._paginator.currentPageReportTemplate;
  }

  /**
   * set paginator row
   */
  set rows(rows: number) {
    this.pTable.rows = rows;
  }

  /**
   * get paginator row
   */
  get rows(): number {
    return this.pTable.rows || this._paginator.rows;
  }

  set globalFilterFields(globalFilterFields: any[]) {
    this.pTable.globalFilterFields = globalFilterFields;
  }

  get groupedColumn(): groupedColumn {
    return this.bodyTable.groupedColumn;
  }

  ngOnInit(): void {
    this.tableService.showLog = this.showLog;
  }

  ngAfterViewInit(): void {
    this._ngZone.runOutsideAngular(() => {
      this.sortMode           = 'single';
      this.reorderableColumns = true;
      this.resetPageOnSort    = false;
      setTimeout(() => {
        this._loadContextMenuRow();
        this._onAfterValueChange();
      }, 0);
    });
  }

  /**
   * onChange @Input() values
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges): void {

    this._ngZone.runOutsideAngular(() => {
      if (changes.bodyTable && changes.bodyTable.currentValue) {

        let currentValue: BodyTable = changes.bodyTable.currentValue;
        if (currentValue.rows && currentValue.rows.length) {
          this.value            = currentValue.rows;
          this.totalRecords     = currentValue.rows.length;
          this._onAfterValueChange();
        }

        if (currentValue.columns && currentValue.columns.length) {
          this.columns          = currentValue.columns;
          this.selectedColumns  = this._getFilteredValueColumns(currentValue.columns);
          this.cleanedColumns   = this._getDropDownColumns(currentValue.columns);

          // On set cette valeur du pTable pour le filtre global
          let globalFilterFields = [];
          for (let i=0; i<currentValue.columns.length; i++) {
            for (let y=0; y<currentValue.columns[i].length; y++) {
              if (this.isAListingColumn(currentValue.columns[i][y])) globalFilterFields.push({
                header: currentValue.columns[i][y].header,
                field: currentValue.columns[i][y].field
              });

              if (currentValue.columns[i][y].position || currentValue.columns[i][y].position === 0) {
                this.bodyColumns.splice(currentValue.columns[i][y].position, 0, currentValue.columns[i][y]);
              }
            }
          }
          this.globalFilterFields = globalFilterFields;
        }

        if (!currentValue.config) {
          this.bodyTable.config = {};
        }

        if (currentValue.canAdd !== false) {
          this.bodyTable.canAdd = true;
        }

        if (currentValue.canDelete !== false) {
          this.bodyTable.canDelete = true;
        }
        
        if (currentValue.groupedColumn && currentValue.groupedColumn.column && currentValue.groupedColumn.active === true)  {
            const interval = setInterval(() => {
              if (this.value && this.value.length > 0) {
                this.onGroupColumns(currentValue.groupedColumn.column, true);
                clearInterval(interval);
              }
            }, 1000);
        }

        if (currentValue.paginator) {
          this.rowsPerPageOptions = currentValue.paginator.rowsPerPageOptions || this._paginator.rowsPerPageOptions;
          this.pageLinkSize       = currentValue.paginator.pageLinkSize || this._paginator.pageLinkSize;
          this.rows               = currentValue.paginator.rows || this._paginator.rows;
        }
      }

      if (this.showLog === true) {
        console.log('TableToolsComponent::ngOnChanges::changes', changes);
        console.log('TableToolsComponent::ngOnChanges::bodyTable', this.bodyTable);
      }

    });
  }

  /**
   * vérifier les changements 
   */
  ngDoCheck(): void {

    this._ngZone.runOutsideAngular(() => {
      var changesBodyTable = this._differ.diff(this.bodyTable);

      if(changesBodyTable) {

        changesBodyTable.forEachChangedItem((record: KeyValueChangeRecord<any, any>) => {
        
          if (this.showLog === true) {
            console.log('TableToolsComponent::ngDoCheck::forEachChangedItem::record', record);
          }

          if (record.key) {
            if (record.key == 'rows') {
              this.value = record.currentValue;
              if (this.groupedColumn && this.groupedColumn.column && this.groupedColumn.active === true)  {
                this.onGroupColumns(this.groupedColumn.column, true);
              } else {
                this.totalRecords = this.value.length;
                this.first = 0;
              }
            } else if (record.key == 'columns') {
              this.columns = record.currentValue;
              this._onAfterValueChange();
            } else if (record.key == 'onDisableRow') {
              this.bodyTable.onDisableRow = record.currentValue;
              this._onAfterValueChange();
            } else if (record.key == 'closeModal') {
              this.displayAddDialog = false;
              this.bodyTable.closeModal = false;
              this._onAfterValueChange();
            } else if (record.key && Object.keys(this.bodyTable).indexOf(record.key) >= 0) { // Moui... Moui...
              this.bodyTable[record.key] = record.currentValue;
              this._onAfterValueChange();
            }
          }
        });

        changesBodyTable.forEachAddedItem((record: KeyValueChangeRecord<any, any>) => {
        
          if (this.showLog === true) {
            console.log('TableToolsComponent::ngDoCheck::forEachAddedItem::record', record);
          }

          if (record.key) {
            if (record.key == 'rows') {
              this.value = record.currentValue;
              this.totalRecords = this.value.length;
              this.first = 0;
            } else if (record.key == 'columns') {
              this.columns = record.currentValue;
            } else if (record.key == 'onDisableRow') {
              this.bodyTable.onDisableRow = record.currentValue;
            } else if (record.key == 'closeModal') {
              this.displayAddDialog = false;
              this.bodyTable.closeModal = false;
            } else if (record.key && Object.keys(this.bodyTable).indexOf(record.key) >= 0) { // Moui... Moui...
              this.bodyTable[record.key] = record.currentValue;
            }/*  else if (this[record.key] !== undefined && typeof this[record.key] != 'function') {
              this[record.key] = record.currentValue;
            } */

            this._onAfterValueChange();
          }
        });

        /* changesBodyTable.forEachRemovedItem((record: KeyValueChangeRecord<any, any>) => {
          console.log('forEachRemovedItem', record)
        }); */
      }
    });
  }

  /**
   * onDestroy view
   */
  ngOnDestroy(): void {
    this._ngZone.runOutsideAngular(() => {
      this._unsubscribeSubscribers();
    });
  }
  
  /**
   * Add a row
   */
  onAdd(): void {
    this._ngZone.runOutsideAngular(() => {
      if (this.bodyTable.canAdd === true) {
        this.displayAddDialog = true;
      } else if (this.bodyTable.controlAdd == true) {
        this._onCustomEventAction({
          id: customEvent.__ADD__,
          value: customEvent.__ADD__
        });
      }
    });
  }

  onCloseAdd(): void {
    this.displayAddDialog = false;
  }

  /**
   * Click on add button into modal
   * @param row 
   */
  onAddRow(row: any): void {
    this._ngZone.runOutsideAngular(() => {
      if (this.bodyTable.canAdd === true) {
        this._treatOnAdd({ rowData: row });
      }
    });
  }

  /**
   * @param event
   */
  onUpdate(event): void {
    this._ngZone.runOutsideAngular(() => {
      if (!this._isAddRow(event)) {
        this._treatOnUpdate(event);
        this.updateDectectChange();

      } else if ((this.bodyTable.canAdd === true || this.bodyTable.controlAdd === true) && event.rowData) {
        this._treatOnAdd({ rowData: event.rowData });
        this.updateDectectChange();
      }
    });
  }

  /**
   * Event emited when a autocomplete change
   * On add row by line and by modal
   * @param event 
   */
  onAutoComplete(event, col?: Column): void {
    if (col) {
      this.autoCompleteEvent.emit({ rowData: event, col: col });
    } else {
      this.autoCompleteEvent.emit(event);
    }
  }

  /**
   * Mise à jour du dom manuellement
   */
  updateDectectChange(callback?: () => Promise<any>) {
    if (callback) {
      callback().then(data => {
        setTimeout(() => this.pTable.cd.detectChanges(), 0);
        if (this.showLog === true) {
          console.log('TableManagerComponent::updateDectectChange::data', data);
        }
      });
    } else {
      setTimeout(() => this.pTable.cd.detectChanges(), 0);
    }

    if (this.showLog === true) {
      console.log('TableManagerComponent::updateDectectChange', this.value);
    }
  }

  /**
   * Show/hide contexmenu header
   * @param data 
   */
  onContextMenuHeader(data: ContextMenuHeader) {
    this.tableService.onContextMenuHeader(data);
  }

  /**
   * Show/hide cell tooltip
   * @param data 
   */
  onTooltip(data: any) {
    this.tableService.onTooltip(data);
  }

  /**
   * Sort table by cell value
   * @param onSort OnSort
   */
  dispatchOnSort(onSort: OnSort): void {
    
    if ((onSort.sortType === 'STRING' && !onSort.input)
      || (onSort.sortType === 'MULTISELECT' && onSort.values.length == 0)) {

      this.filteredValue = null;
      if (this.paginator) {
        this.totalRecords = this.value ? this.value.length : 0;
        this.first = 0;
      }
    } else {
      
      /**
       * On sauve les éléments de la liste avant de traiter le sort
       * car si on sort la liste et qu'ensuite on fait un groupement de colonne,
       * un bogue se produit.
       */
      if (onSort.col.contextMenuHeader
        && onSort.col.contextMenuHeader[onSort.sortType]
        && onSort.col.contextMenuHeader[onSort.sortType].callback) {
        this._switchSort(onSort, onSort.col.contextMenuHeader[onSort.sortType].callback);
      } else {
        this._switchSort(onSort);
      }

      if (this.callbacks 
        && this.callbacks.onSort
        && this.callbacks.onSort.title) {
        this.callbacks.onSort.title(onSort).then((newTitle) => {
          this.configList.title = newTitle;
        });
      }
    }
  }

  /**
   * Action quand le contextMenu est affiché
   * Le montrer ? Le cacher ?...
   * @param value 
   */
  contextMenuSelection(value: any): void {
    if (this.callbacks && this.callbacks.contextMenu && this.callbacks.contextMenu.show) {
      let showContextMenu = this.callbacks.contextMenu.show(value);

      if (showContextMenu === false) {
        setTimeout(() => {
          this.contextMenu.containerViewChild.nativeElement.style.display = 'none';
          this.contextMenu.containerViewChild.nativeElement.style.visibility = 'hidden';
        }, 0);
      }
    }
  }

  /**
   * reset all filters
   */
  onResetFilter(): void {

    this._ngZone.runOutsideAngular(() => {

      const arrayChange = [...this.filteredValue];
      this.pTable.reset();

      setTimeout(() => {
        if (arrayChange && arrayChange.length) {
          let nbr = 0;
          dance:
          for (let i=0; i<this.value.length; i++) {
            for (let y=0; y<arrayChange.length; y++) {
              if (this.value[i][this.indexColumn] == arrayChange[y][this.indexColumn]) {
                this.value[i] = arrayChange[y];
                nbr++;
    
                if (nbr == arrayChange.length) {
                  break dance;
                } else {
                  break;
                }
              }
            }
          }
        }

        this.value = [].concat(this.value);
        this._ngZone.run(() => this.updateDectectChange());
      }, 0);
    });
  }

  /**
   * onPage : appelé quand on clique sur un bouton de la pagination
   * @param event 
   */
  onPageChange(event: {page: number, first: number, rows: number, pageCount: number}): void {
    if (this.bodyTable.paginator && this.bodyTable.paginator.callback) {
      this.bodyTable.paginator.callback({
        first: event.first, 
        rows: event.rows,
        hasFilter: this.pTable.hasFilter(),
        filter: this.pTable.filters
      }).then(result => {
        if (this.pTable.hasFilter()) {
          this.filteredValue = result;
        } else {
          this.value = result;
        }
      });
    } else {
      event.first = (event.first > 0) ? event.first : 0;
      this.pTable.onPageChange(event);
    }
  }

  /**
   * Custom event emit on double click
   * @param rowData 
   */
  onDblClickRow(rowData: any): void {
    this._onCustomEventAction({
      id: customEvent.__DBLCLICK__,
      value: rowData
    });
  }

  /**
   * onEditinit
   * @param event 
   */
  onEditInit(event: { field: string; rowData: any }) {

    if (this.showLog === true) {
      console.log('TableTools::onEditInit::event', event);
    }

    if (this.callbacks && this.callbacks.onEditInit) {
      this.callbacks.onEditInit(event);
    }
  }

  onEditComplete(event: { field: string; rowData: any }) {

    if (this.showLog === true) {
      console.log('TableTools::onEditComplete::event', event);
    }

    if (this.callbacks && this.callbacks.onEditComplete) {
      this.callbacks.onEditComplete(event);
    }
  }

  onEditCancel(event: { field: string; rowData: any }) {

    if (this.showLog === true) {
      console.log('TableTools::onEditCancel::event', event);
    }

    if (this.callbacks && this.callbacks.onEditCancel) {
      this.callbacks.onEditCancel(event);
    }
  }

  /**
   * onClickMultiSelectColumn
   * @param event
   */
  onClickMultiSelectColumn(event?: any): void {
    let intersection = this.selectedColumns.filter(x => this.cleanedColumns.some(z => x == z.value));

    for (let columns of this.columns) {
      for (let i=0; i<columns.length; i++) {
        if (columns[i].field && intersection.indexOf(columns[i].field) == -1) {
          columns[i].display = false;
        } else if (columns[i].field) {
          columns[i].display = true;
        }
      }
    }
  }

  /**
   * 
   * @param column 
   */
  onHideColumn(column: Column): void {
    this.selectedColumns = this.selectedColumns.filter(o => o != column.field);
    this.onClickMultiSelectColumn();
  }

  /**
   * Mise à jour du groupement par ligne
   * rowGroup
   */
  onGroupColumns(column?: Column, force?: boolean) {

    this._ngZone.runOutsideAngular(() => {
      this._groupedColumns = null;
      if (column.isGrouped === true && !force) {
        // document.getElementsByClassName('grouped-rows')[0].classList.remove('clignote');
        this.groupedRowExpand = null;
        this._changeIsGroupedValue(column, false);
        this.totalRecords = this.value.length;
        this.first = 0;
        this._onAfterValueChange();
      } else {
        // document.getElementsByClassName('grouped-rows')[0].classList.add('clignote');
        this.pTable.reset();
        this._changeIsGroupedValue(column, true);
        setTimeout(() => {
          new Promise((resolve, reject) => {
            
            let colsValues = this.value.map(v => {
              if (column.inputType == 'checkbox') {
                let sorterKey = v[column.field];
                if (sorterKey == 0 || !sorterKey || sorterKey == '0' || sorterKey == ''
                    || (!isNaN(sorterKey) && parseInt(sorterKey) == 0)) {
                  v[column.field] = false;
                } else {
                  v[column.field] = true;
                }
              }
              return v;
            });
        
            colsValues = colsValues.sort((a, b) => {
              let comparison = -1;
              let sorterKeyA = null;
              let sorterKeyB = null;
              if (column.inputType == 'checkbox') {
                sorterKeyA = a[column.field];
                sorterKeyB = b[column.field];
                return (sorterKeyA === sorterKeyB) ? 0 : (sorterKeyA === true) ? -1 : 1;
              } else if (!isNaN(a[column.field]) && !isNaN(b[column.field])) {
                return a[column.field] === b[column.field] ? 0 : (parseFloat(a[column.field]) < parseFloat(b[column.field])) ? -1 : 1;
              } else if (a[column.field] && b[column.field]) {
                if (column.inputType == 'select') {
                  sorterKeyA = select.clean(a, column);
                  sorterKeyB = select.clean(b, column);
                  if (typeof sorterKeyA == 'string') { sorterKeyA = sorterKeyA.toUpperCase(); }
                  if (typeof sorterKeyB == 'string') { sorterKeyB = sorterKeyB.toUpperCase(); }
                } else {
                  sorterKeyA = a[column.field].toUpperCase();
                  sorterKeyB = b[column.field].toUpperCase();
                }
                if (sorterKeyA > sorterKeyB) {
                  comparison = (this.groupedColumn.reverse === true) ? -1 : 1;
                } else if (sorterKeyA < sorterKeyB) {
                  comparison = (this.groupedColumn.reverse === true) ? 1 : -1;
                }
                return comparison;
              } else if (!a[column.field]) {
                return 1;
              } else if (!b[column.field]) {
                return -1;
              } else {
                return comparison;
              }
            });
        
            let header              = null;
            let groupedRows: any[]  = [];
            let numberOfEditLine = 0;
            for (let i=0; i<colsValues.length; i++) {
              let colValue = (column.inputType == 'select') ? select.getLabel(colsValues[i], column) : colsValues[i][column.field];

              if (header != colValue) {
                // Nouveau groupe de lignes
                if (header && this.callbacks && this.callbacks.onGroupedRows) {
                  ((i, groupedRows, colsValues) => {
                    colsValues.splice(i, 0, { inputType: langage.TABLE_TYPE.GROUPED_EDIT_FOOTER, cells: [] });
                    // On lance les callbacks qui permettent de faire des calculs sur une ligne du footer
                    for (let columns of this.columns) {
                      for (let col of columns) {
                        this.callbacks.onGroupedRows({ column: col, groupedRows: groupedRows, groupedField: column.field }).then(result => {
                          colsValues[i].cells.push({
                            colspan: result.colspan || 1, 
                            text: result.text || '', 
                            bgColor: result.bgColor || '' , 
                            color: result.color || '' 
                          });
                        }).catch(err => {
                          // console.log(err);
                        });
                      }
                    }
                  })(i, groupedRows, colsValues);

                  i++;
                }

                if (this.callbacks && this.callbacks.onGroupedRows) {
                  groupedRows = [];
                  groupedRows.push(colsValues[i]); 
                }

                header = colValue;
                // On ajoute la ligne qui montre le libellé du tri
                ((i, colsValues, column, colValue) => {
                  colsValues.splice(i, 0, {
                    header: column.header,
                    field: column.field,
                    key: colValue,
                    inputType: langage.TABLE_TYPE.GROUPED_EDIT
                  });
                  numberOfEditLine++;
                })(i, colsValues, column, colValue);
      
                i++;
              } else if (this.callbacks && this.callbacks.onGroupedRows) {
                groupedRows.push(colsValues[i]);
              }
            }
            return resolve({colsValues: colsValues, numberOfEditLine: numberOfEditLine});
          }).then((result: {colsValues: any[], numberOfEditLine: number}) => {
            this._groupedColumns = result.colsValues;
            this.totalRecords = result.colsValues.length - result.numberOfEditLine;
            this.first = 0;
            this._onAfterValueChange();
          });
        }, 0);
      }
    });
  }

  /**
   * onUnGroupColumn
   */
  onUnGroupColumn(): void {
    
    this._ngZone.runOutsideAngular(() => {
      let founded = false;
      for (let columns of this.columns) {
        for (let column of columns) {
          if (column.isGrouped === true) {
            this.onGroupColumns(column);
            founded = true;
            break;
          }
        }
      }

      if (!founded 
        && this.bodyTable.groupedColumn 
        && this.bodyTable.groupedColumn.active
        && this.bodyTable.groupedColumn.column) {
          this.onGroupColumns(this.bodyTable.groupedColumn.column);
        }
    });
  }

  /**
   * colspan nécessaire pour les titres de groupement de colonne
   */
  getColSpan() {
    let colSpan = 0;
    for (let columns of this.columns) {
      for (let column of columns) {
        if (this.isAListingColumn(column)) { colSpan++; }
      }
    }
    return colSpan;
  }

  /**
   * Montrer/cacher les lignes d'un groupement de colonne
   * @param field 
   * @param key 
   */
  toggleGrouped(field: string, key: string) {
    
    this._ngZone.runOutsideAngular(() => {
      if (this._toggleGroupedColumnsKeys[key] && this._toggleGroupedColumnsKeys[key].length > 0) {
        for (let i=0; i<this._groupedColumns.length; i++) {
          if (this._groupedColumns[i].inputType == langage.TABLE_TYPE.GROUPED_EDIT
            && this._groupedColumns[i].key == key) {
            this._groupedColumns.splice((i+1), 0, ...this._toggleGroupedColumnsKeys[key]);
            this._toggleGroupedColumnsKeys[key] = [];
            break;
          }
        }
      } else {
        this._toggleGroupedColumnsKeys[key] = [];
        if (this._groupedColumns && this._groupedColumns.length) {
          for (let i=0; i<this._groupedColumns.length; i++) {
            if (this._groupedColumns[i].inputType != langage.TABLE_TYPE.GROUPED_EDIT
              && this._groupedColumns[i][field] 
              && this._groupedColumns[i][field] == key) {
              this._toggleGroupedColumnsKeys[key].push(this._groupedColumns[i]);
              this._groupedColumns.splice(i, 1);
              i--;
            }
          }
        }
      }
    });
  }

  /**
   * 
   * @param key 
   */
  toggleGroupedIcon(key: string) {
    return (this._toggleGroupedColumnsKeys[key] && this._toggleGroupedColumnsKeys[key].length > 0);
  }

  /**
   * expand all rows
   * toggleRowExpand
   */
  toggleRowExpand(expand?: boolean): void {
    
    this._ngZone.runOutsideAngular(() => {
      if (expand === true) {
        this.groupedRowExpand = true;
        if (Object.keys(this._toggleGroupedColumnsKeys).length > 0) {
          let keys = Object.keys(this._toggleGroupedColumnsKeys);
          for (let i=0; i<keys.length; i++) {
            for (let y=0; y<this._groupedColumns.length; y++) {
              if (this._groupedColumns[y].inputType == langage.TABLE_TYPE.GROUPED_EDIT
                && this._groupedColumns[y].key == keys[i]) {
                  this._groupedColumns.splice((y+1), 0, ...this._toggleGroupedColumnsKeys[keys[i]]);
                  this._toggleGroupedColumnsKeys[keys[i]] = [];
                  break;
              }
            }
          }
        }
      } else {
        this.groupedRowExpand = false;
        let key = null;
        let field = null;
        for (let i=0; i<this._groupedColumns.length; i++) {
          if (this._groupedColumns[i].inputType == langage.TABLE_TYPE.GROUPED_EDIT) {
            key = this._groupedColumns[i].key;
            field = this._groupedColumns[i].field;
            if (!this._toggleGroupedColumnsKeys[key]) {
              this._toggleGroupedColumnsKeys[key] = [];
            }
          } else if (field && key && this._groupedColumns[i][field] == key) {
            this._toggleGroupedColumnsKeys[key].push(this._groupedColumns[i]);
            this._groupedColumns.splice(i, 1);
            i--;
          }
        }
      }
    });
  }
  
  /**
   * Est-ce une colonne à afficher (une vraie colonne présente dans le listing et non une display false par ex.) ?
   * @param column 
   */
  isAListingColumn(column: Column) {
    return (column.field && column.inputType != 'hidden' && column.display !== false) === true;
  }

  /**
   * optimise primeng table avec trackBy pour *ngFor
   * 
   * @param index 
   * @param row 
   */
  trackByIndexColumn(index, row) {
    return row[this.indexColumn];
  }

  /**
   * 
   */
  hideContextMenuHeader(event: any) {
    event.target.querySelector('cm-header').style.display = 'none'
    event.target.classList.remove(this.LANGAGE.CLASS_LIST.ACTIVE);
    this.tableService.onContextMenuHeader({ hide: true });
  }

  /**
   * 
   * @param column 
   * @param grouped 
   */
  private _changeIsGroupedValue(column: Column, grouped: boolean) {
    this._ngZone.runOutsideAngular(() => {
      for (let i=0; i<this.columns.length; i++) {
        for (let y=0; y<this.columns[i].length; y++) {
          if (column.field == this.columns[i][y].field) {
            this.columns[i][y].isGrouped = grouped;
            break;
          }
        }
      }
    });
  }

  /**
   * _onAfterValueChange
   * @param rows 
   */
  private _onAfterValueChange(rows?: any[], filtered?: boolean) {
    this._ngZone.runOutsideAngular(() => {
      if (this.showLog === true) { console.log('_onAfterValueChange', 'called'); }

      this.bodyColumns = [];
      for (let columns of this.columns) {
        for (let column of columns) {
          if (column.position || column.position === 0) {
            this.bodyColumns.splice(column.position, 0, column);
          }

          if (rows && rows.length && column.valueChange instanceof Function) {
            for (let i=0; i<rows.length; i++) {
              rows[i] = column.valueChange(column, rows[i]);
            }

            if (filtered === true) {
              this.filteredValue = rows;
            } else {
              this.value = rows;
            }
          }
        }
      }

      this.totalRecords = (this.totalRecords == 0) ? this.value.length : this.totalRecords;
      this._ngZone.run(() => this.updateDectectChange());
    });
  }

  /**
   * Permet d'avoir les valeurs des fields pour les colonnes de la liste
   * @param columns 
   */
  private _getFilteredValueColumns(columns: Array<Column[]>): string[] {
    
    let filteredValue = [];
    for (let column of columns) {
      for (let col of column) {
        if (this.isAListingColumn(col)) filteredValue.push(col.field);
      }
    }

    return filteredValue;
  }

  /**
   * Permet d'avoir les colonnes à lister correctement intégrées dans un array
   * @param columns 
   */
  private _getDropDownColumns(columns: Array<Column[]>): Array<{label: string, value: any}> {
    
    let cleanedColumns = [];
    for (let column of columns) {
      for (let col of column) {
        if (this.isAListingColumn(col)) cleanedColumns.push({
          label: col.header,
          value: col.field
        });
      }
    }

    return cleanedColumns;
  }

  /**
   * 
   * @param onSort 
   * @param callback 
   */
  private _switchSort(onSort: OnSort, callback?: any): void {

    switch (onSort.sortType) {
      case this.LANGAGE.SORT_TYPE.ASC:
      case this.LANGAGE.SORT_TYPE.DESC:
        this._onSortOrder(onSort);
        break;
      case this.LANGAGE.SORT_TYPE.DATE:
        if (callback) this._onSortByDateWithCallback(onSort, callback)
        else this._onSortDate(onSort);
        break;
      case this.LANGAGE.SORT_TYPE.STRING:
        if (callback) this._onSortByStringWithCallback(onSort, callback)
        else this._onSortByString(onSort);
        break;
      case this.LANGAGE.SORT_TYPE.MULTISELECT:
        if (callback) this._onSortByMultiSelectWithCallback(onSort, callback)
        else this._onSortByMultiSelect(onSort);
        break;
      case this.LANGAGE.SORT_TYPE.BINARY:
        if (callback) this._onSortByBinaryWithCallback(onSort, callback);
        else this._onSortByBinary(onSort);
        break;
    }
  }

  /**
   * on sort by order ASC/DESC
   * @param onSort 
   */
  private _onSortOrder(onSort: OnSort): void {
    this.sortField    = onSort.col.field;
    this.sortOrder    = (onSort.sortType === this.LANGAGE.SORT_TYPE.ASC) ? -1 : 1;
    this.pTable.sort({ originalEvent: {}, field: onSort.col.field });
    this._hideContextMenuHeader(onSort);
  }

  /**
   * on sort by date
   * @param onSort 
   */
  private _onSortDate(onSort: OnSort): void {
    this.filteredValue = [].concat(this.value.filter(item => {
      let date = new Date(item[onSort.col.field]);
      return date.getTime() >= onSort.start_date.getTime() && date.getTime() <= onSort.end_date.getTime();
    }));
    this._hideContextMenuHeader(onSort);
  }

  /**
   * Sort by date with a callback defined by user
   * @param onSort 
   * @param callback 
   */
  private _onSortByDateWithCallback(onSort: OnSort, callback?: (sortType: SortType) => Promise<any[]>) {
    callback({column: onSort.col.field, value: {
      start_date: onSort.start_date, 
      end_date: onSort.end_date
    }, sortType: onSort.sortType}).then(result => {
      this.filteredValue = result;
      this.totalRecords = result.length;
      this.first = 0;
      // this._onAfterValueChange(result, true);
    }).catch(err => {
      if (this.showLog === true) {
        console.log('Table::_onSortByDateWithCallback:err', err);
      }
    });
    this._hideContextMenuHeader(onSort);
  }

  /**
   * on sort by string on column
   * @param onSort 
   */
  private _onSortByString(onSort: OnSort): void {
    this.pTable.filter(onSort.input, onSort.col.field, onSort.filterMatchMode);
    this._hideContextMenuHeader(onSort);
  }

  /**
   * Sort by string with a callback defined by user
   * @param onSort 
   * @param callback 
   */
  private _onSortByStringWithCallback(onSort: OnSort, callback?: (sortType: SortType) => Promise<any[]>) {
    callback({column: onSort.col.field, value: onSort.input, sortType: onSort.sortType}).then(result => {
      this.filteredValue = result;
      this.totalRecords = result.length;
      this.first = 0;
      // this._onAfterValueChange(result, true);
    }).catch(err => {
      if (this.showLog === true) {
        console.log('Table::_onSortByStringWithCallback:err', err);
      }
    });
    this._hideContextMenuHeader(onSort);
  }

  /**
   * on sort by multiselection items on column
   * @param onSort 
   */
  private _onSortByMultiSelect(onSort: OnSort): void {
    if (onSort.col.inputType == langage.COLUMNS.INPUT_TYPE.MULTISELECT) {
      let items: any[] = [];
      
      if (onSort.values.length == 0) {
        this.filteredValue = null;
        if (this.paginator) {
          this.totalRecords = this.value ? this.value.length : 0;
          this.first = 0;
        }
      } else {
        for (let item of this.value) {
          if (item[onSort.col.field]) {
            let it = Array.isArray(item[onSort.col.field]) ? item[onSort.col.field] : item[onSort.col.field].split(langage.COLUMNS.MULTI_SELECT_SEPARATOR);
            value:
            for (let value of it) {
              for (let option of onSort.values) {
                if (value == option) {
                  items.push(item);
                  break value;
                }
              }
            }
          }
        }
  
        this.filteredValue = items;
        if (this.paginator) {
          this.totalRecords = Array.isArray(this.filteredValue) ? this.filteredValue.length : Array.isArray(this.value) ? this.value.length : 0;
          this.first = 0;
        }
      }

    } else {
      this.pTable.filter(onSort.values, onSort.col.field, onSort.filterMatchMode);
    }
    this._hideContextMenuHeader(onSort);

    if (this.showLog === true) {
      console.log('Table::_onSortByMultiSelect:onSort', onSort);
    }
  }

  private _onSortByBinary(onSort: OnSort): void {
    this.pTable.filter(onSort.input, onSort.col.field, null);
    this._hideContextMenuHeader(onSort);
  }

  private _onSortByBinaryWithCallback(onSort: OnSort, callback?: (sortType: SortType) => Promise<any[]>): void {
    callback({column: onSort.col.field, value: onSort.input, sortType: onSort.sortType}).then(result => {
      this.filteredValue = result;
      this.totalRecords = result.length;
      this.first = 0;
      // this._onAfterValueChange(result, true);
    }).catch(err => {
      if (this.showLog === true) {
        console.log('Table::_onSortByBinaryWithCallback:err', err);
      }
    });
    this._hideContextMenuHeader(onSort);
  }

  /**
   * Sort by string[] with a callback defined by user
   * @param onSort 
   * @param callback 
   */
  private _onSortByMultiSelectWithCallback(onSort: OnSort, callback?: (sortType: SortType) => Promise<any[]>): void {
    callback({column: onSort.col.field, value: onSort.values, sortType: onSort.sortType}).then(result => {
      this.filteredValue = result;
      this.totalRecords = result.length;
      this.first = 0;
      // this._onAfterValueChange(result, true);
    }).catch(err => {
      if (this.showLog === true) {
        console.log('Table::_onSortByMultiSelectWithCallback:err', err);
      }
    });
    this._hideContextMenuHeader(onSort);
  }

  /**
   * Hide contextMenu header when open
   */
  private _hideContextMenuHeader(onSort: OnSort): void {
    this.tableService.onContextMenuHeader({ hide: true, parent: onSort.parent });
  }

  /**
   * Confirm delete
   * @param domain 
   */
  private _confirm(rowData: any): void {
    this._confirmationService.confirm({
      message: this.LANGAGE.CONFIRM_DELETE,
      header: this.LANGAGE.CONFIRM,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        for (let columns of this.columns) {
          for (let col of columns) {
            rowData = this._cleanRowData(rowData, col);
          }
        }
        this.deleteEvent.emit(rowData);
      }
    });
  }

  /**
   * Si c'est une ligne que l'on est en train d'ajouter on ne veut pas faire un emit update event
   * @param event
   */
  private _isAddRow(event): boolean {
    if (this.indexColumn) {
      return !event.rowData || ( event.rowData && !event.rowData[this.indexColumn] );
    } else {
      throwError('Missing bodyTable.indexColumn (index of rows)');
    }
  }

  /**
   * emit a update event
   * 
   * @param event
   */
  private _treatOnAdd(event): void {

    if (this.showLog === true) {
      console.log('Table::_treatOnAdd::event', event);
    }

    let rowData = Object.assign({}, event.rowData);
    if (rowData) {
      this.addEvent.emit(this._cleanRowDataToAdd(rowData));
      setTimeout(() => this.updateDectectChange(), 1000);
      // this.displayAddDialog = false;
    }
  }

  /**
   * emit a update event
   * 
   * @param event
   */
  private _treatOnUpdate(event): void {

    if (this.showLog === true) {
      console.log('Table::_treatOnUpdate::event', event);
    }

    let rowData = Object.assign({}, event.rowData);
    let col = event.col;

    if (rowData && col) {
      for (let columns of this.columns) {
        for (let col of columns) {
          rowData = this._cleanRowData(rowData, col);
        }
      }
      this.updateEvent.emit(rowData);
    } else if (rowData) {
      this.updateEvent.emit(rowData);
    }
  }

  /**
   * Voir _cleanRowData
   * @param rowData 
   */
  private _cleanRowDataToAdd(rowData: any) {
    for (let columns of this.columns) {
      for (let col of columns) {
        rowData = this._cleanRowData(rowData, col);
      }
    }

    return rowData;
  }

  /**
   * Nettoie la valeur d'une dropdown pour ne renvoyer que 
   * l'ID (value) de la dropdown sélectionnée
   * 
   * @param rowData 
   * @param col 
   */
  private _cleanRowData(rowData: any, col: Column) {

    if (col.inputType == 'select' && col.cleanValue !== false) {
      rowData[col.field] = select.clean(rowData, col);

      if (rowData[col.field] && typeof rowData[col.field] == 'object' && rowData[col.field].length == 0) {
        rowData[col.field] = null;
      }

      /* if (rowData[col.field] === null) {
        delete rowData[col.field];
      } */
    }

    if (!col.inputType || col.permitUpdade === false || col.disabled === true) {
      delete rowData[col.field];
    }

    return rowData;
  }

  /**
   * Get context menu to rows table
   */
  private _loadContextMenuRow() {
    this._ngZone.runOutsideAngular(() => {
      if (this.contextMenuRow) {
        if (this.contextMenuRow.delete !== false) {
          this.contextMenuItems = [{
            label: this.LANGAGE.REMOVE, 
            icon: this.LANGAGE.ICON.REMOVE,
            command: (event) => this._onDelete(this.selectedRow)
          }];
        }

        if (this.contextMenuRow.actions && this.contextMenuRow.actions.length) {
          for (let action of this.contextMenuRow.actions) {
            this.contextMenuItems.push({
              id: action.id,
              label: action.name, 
              icon: action.icon,
              command: (event) => this._onCustomEventAction({
                id: action.id,
                value: this.selectedRow
              })
            })
          }
        }
      }

      setTimeout(() => {
        if (this.contextMenuItems.length == 0 && this.contextMenu.containerViewChild) {
          this.contextMenu.containerViewChild.nativeElement.remove();
        }
      }, 0);
    });
  }

  /**
   * @param rowData 
   */
  private _onDelete(rowData: any): void {
    if (this.bodyTable.canDelete === true) {
      this._confirm(rowData);
    }
  }

  /**
   * actions spécifiques du contextMenu définient par l'utilisateur
   * @param rowData 
   */
  private _onCustomEventAction(event: {id: string, value: any}): void {
    this.customEvent.emit(event);
  }

  /**
   * init all subscribers
   */
  private _initSubscribers() {
    this._onUpdateSubscription = this.tableService.updateCell$.subscribe(event => {
      this.onUpdate(event);

      if (this.showLog === true) {
        console.log('TableToolsComponent::_initSubscribers::_onUpdateSubscription', event);
      }
    });
    
    this._onAutoCompleteSubscription = this.tableService.autoCompleteCell$.subscribe(event => {
      this.onAutoComplete(event);
      if (this.showLog === true) {
        console.log('TableToolsComponent::_initSubscribers::_onAutoCompleteSubscription', event);
      }
    });
    
    this._onAddRowSubscription = this.tableService.addRow$.subscribe(event => {
      this.onAddRow(event);
      if (this.showLog === true) {
        console.log('TableToolsComponent::_initSubscribers::_onAddRowSubscription', event);
      }
    });
  }

  /**
   * unsubscribe everything
   */
  private _unsubscribeSubscribers() {

    if (this._onUpdateSubscription) {
      this._onUpdateSubscription.unsubscribe();
      this._onUpdateSubscription = null;
    }

    if (this._onAutoCompleteSubscription) {
      this._onAutoCompleteSubscription.unsubscribe();
      this._onAutoCompleteSubscription = null;
    }

    if (this._onAddRowSubscription) {
      this._onAddRowSubscription.unsubscribe();
      this._onAddRowSubscription = null;
    }
  }
}