import { Component, Input, SimpleChanges, EventEmitter, Output, AfterContentInit, OnChanges } from '@angular/core';
import { langage } from '../../config/langage';
import { FormValidator } from '../../models/form-validator';
import { Message } from 'primeng/api';
import { FormControl } from '@angular/forms';
import { ValidatorService } from '../../services/validator.service';
import { TableToolsComponent } from '../table/table.component';
import { Column } from '../../models/column';
import { fr } from '../../config/locale';

@Component({
  selector: 'add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.scss'],
  providers: []
})
export class AddModalComponent implements AfterContentInit, OnChanges {

  @Input() displayAddDialog: boolean = false;
  @Input() groupPanel: string[] = [];
  @Input() columns: Array<Column[]>;
  
  @Output() onClose: EventEmitter<any> = new EventEmitter<any>();
  
  public LANGAGE: any = langage;
  public locale = fr;

  /**
   * Valeur créée car il y a un problème avec les erreurs (provider)
   * Il faut les supprimer avant d'afficher la modal (ngOnChanges -> warningMsgs)
   */
  public displayModal: boolean = false;

  /**
   * Relatif to addForm dialog
   */
  public itemToAdd: any = {};
  public validatorAddFields: FormValidator[] = [];
  public warningMsgs: Message[] = [];
  public groupAddPanelForm: any[] = [];

  /**
   * 
   * @param _translateService 
   * @param _tb 
   */
  constructor(private _translateService: ValidatorService,
    private _tb: TableToolsComponent) {
      this.itemToAdd    = {};
      this.warningMsgs  = [];
  }

  /**
   * init default value after view loaded
   */
  ngAfterContentInit(): void {
    this._initFormFields();
  }

  /**
   * watch change values
   * 
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges): void {

    if (changes.displayAddDialog) {
      this.warningMsgs = [];
      setTimeout(() => this.displayModal = changes.displayAddDialog.currentValue, 0);
    }

    if (changes.columns
      && changes.columns.currentValue
      && changes.columns.currentValue.length) {

      if (Object.keys(this.groupAddPanelForm).length == 0) {
        this._buildGroupPanel();
      }
    }

    if (this._tb.showLog === true) {
      console.log('AddModalComponent::ngOnChanges::currentValue', changes);
      console.log('AddModalComponent::ngOnChanges::columns', this.columns);
    }
  }

  /**
   * Called on ngModelChange (textarea, dropdown)
   * 
   * @param cb 
   * @param value 
   * @param item 
   */
  onDialogAddChange(cb: any, value: Column, item: any): void {
    if (cb instanceof Function) {
      cb(value, item).then((result: any) => {
        if (result && Object.keys(result).length > 0) {
          for (let colGroup of this.columns) {
            for (let col of colGroup) {
              if (result[col.field] !== undefined) {
                this.itemToAdd[col.field] = result[col.field];
              }
            }
          }
        }
      });
    }

    if (this.validatorAddFields[value.field]
      && this.validatorAddFields[value.field].required == true) {
      if (!item[value.field]) {
        this.validatorAddFields[value.field].isValid = false;
      } else {
        this.validatorAddFields[value.field].isValid = true;
      }
    }
  }

  /**
   * hide add modal
   */
  onAddModalClose(): void {
    this.itemToAdd          = {};
    this.warningMsgs        = [];
    this.displayModal       = false;
    this.onClose.emit();
  }

  /**
   * Event emited when a autocomplete change
   * On add row by line and by modal
   * @param $event 
   */
  onAutoComplete($event, col?: Column): void {
    if (col) {
      this._tb.tableService.onAutoCompleteCell({ rowData: $event, col: col });
    } else {
      this._tb.tableService.onAutoCompleteCell($event);
    }
  }

  /**
   * emit a update event
   * 
   * @param rowData
   */
  onAddRowClick(rowData: any): void {
    let rowToAdd = Object.assign({}, rowData);
    rowToAdd = this._treatRowData(rowToAdd);
    
    if (rowToAdd) {
      this._tb.tableService.onAddRow(rowToAdd);
      // this.onAddModalClose();
    }
  }

  /*
   * check if a field is signified invalid
   * @param field 
   */
  checkValidField(field: string): boolean {
    return (this.validatorAddFields[field] && this.validatorAddFields[field].isValid === false);
  }

  /**
   * Lorsque la modal est montrée
   * @param event 
   */
  onShow(event: any) {
    this._buildGroupPanel();
  }

  /**
   * Construit le group panel pour afficher la forme avec ces éléments classés
   * Voir Column.panelGroup
   */
  private _buildGroupPanel() {
    this.groupAddPanelForm  = [];
    this.groupPanel         = [];

    for (let colGroup of this.columns) {
      for (let col of colGroup) {
        if (col.field) {
          if (col.panelGroup) {
            if (!this.groupAddPanelForm[col.panelGroup]) {
              this.groupAddPanelForm[col.panelGroup] = [];
              this.groupPanel.push(col.panelGroup);
              this.groupAddPanelForm[col.panelGroup].push(col);
            } else {
              this.groupAddPanelForm[col.panelGroup].push(col);
            }
          } else if (col.inputType && col.inputType != 'hidden') {
            if (!this.groupAddPanelForm[this.LANGAGE.DEFAUT_PANEL_ADD_TITLE]) {
              this.groupAddPanelForm[this.LANGAGE.DEFAUT_PANEL_ADD_TITLE] = [];
              this.groupPanel.push(this.LANGAGE.DEFAUT_PANEL_ADD_TITLE);
              this.groupAddPanelForm[this.LANGAGE.DEFAUT_PANEL_ADD_TITLE].push(col);
            } else {
              this.groupAddPanelForm[this.LANGAGE.DEFAUT_PANEL_ADD_TITLE].push(col);
            }
          }
        }
      }
    }
  }

  /**
   * Traite les différents clés/valeurs de la forme afin de rendre un objet valide 
   * @param rowData
   */
  private _treatRowData(rowData): any {
    return (this._controlForm(rowData) === true) ? rowData : false;
  }

  /**
   * Control form before send
   * @param rowData
   */
  private _controlForm(rowData) {
    let validForm     = true;
    let messages      = [];
    this.warningMsgs  = [];

    for (let colGroup of this.columns) {
      for (let col of colGroup) {
        if (col.validators && col.validators.length > 0) {
          let control = new FormControl(rowData[col.field], col.validators);
          if (control.status.toLowerCase() == 'invalid') {
            this.validatorAddFields[col.field].isValid = false;
            validForm = false;
            messages.push(this._translateService.translateFormControlError(col.header, control.errors));
          } else {
            this.validatorAddFields[col.field].isValid = true;
          }
        }
      }
    }

    if (validForm === false) {
      this.warningMsgs.push({
        severity: 'warn', 
        summary: this.LANGAGE.ERROR_FIELD, 
        detail: messages.join("<br />\n")
      });
    }
    return validForm;
  }

  /**
   * Initialisation required fields
   */
  private _initFormFields(): void {
    for (let colGroup of this.columns) {
      for (let col of colGroup) {
        this.validatorAddFields[col.field] = { required: col.required, isValid: true }
      }
    }
  }
}