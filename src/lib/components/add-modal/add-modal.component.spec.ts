import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TableToolsComponent } from '../table/table.component';
import { CommonModule } from '@angular/common';
import { FormsModule, Validators } from '@angular/forms';
import { TableModule, Table as PTable, TableService as PTableService, Table } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from 'primeng';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PaginatorModule } from 'primeng/paginator';
import { SpinnerModule } from 'primeng/spinner';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DebugElement, ElementRef, ChangeDetectorRef } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { AddModalComponent } from './add-modal.component';
import { defaultValues, defaultColumns } from '../../jasmine-test/add-modal';
import { columns as columnsUtils } from '../../shared/columns';
import { Column } from '../../models/column';
import { number_pattern, date_ddmmyyyy_pattern } from '../../const';

export class MockElementRef extends ElementRef {
    constructor() { super(null); }
}

export class MockChangeDetectorRef extends ChangeDetectorRef {
    checkNoChanges() {}
    detach() {}
    detectChanges() {}
    markForCheck() {}
    reattach() {}
}

export class MockMessageService extends MessageService {}

describe('AddModalComponent', () => {
  let component: AddModalComponent;
  let fixture: ComponentFixture<AddModalComponent>;
  let debugElement: DebugElement;
  
  let componentTableTools: TableToolsComponent;
  let fixtureTableTools: ComponentFixture<TableToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        TableModule,
        DropdownModule,
        AutoCompleteModule,
        CheckboxModule,
        ContextMenuModule,
        DynamicDialogModule,
        InputTextareaModule,
        InputTextModule,
        MultiSelectModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        PanelModule,
        TooltipModule,
        CommonModule,
        TableModule,
        SharedModule,
        RadioButtonModule,
        CalendarModule,
        ToastModule,
        ConfirmDialogModule,
        PaginatorModule,
        SpinnerModule,
        BrowserAnimationsModule
      ],
      declarations: [ 
        AddModalComponent
      ],
      providers: [ 
          TableToolsComponent, 
          ConfirmationService, 
          PTable,
          PTableService,
          { provide: ElementRef, useClass: MockElementRef },
          { provide: ChangeDetectorRef, useClass: MockChangeDetectorRef },
          MessageService
        ]
    }).compileComponents();

  }));

  
  beforeEach(async(() => {
    
    fixture                                 = TestBed.createComponent(AddModalComponent);
    component                               = fixture.componentInstance;
    debugElement                            = fixture.debugElement;

    componentTableTools                       = fixture.debugElement.injector.get(TableToolsComponent);

    componentTableTools.bodyTable             = { rows: [], columns: [], indexColumn: "id" };
    componentTableTools.value                 = defaultValues;
    componentTableTools.columns               = defaultColumns;

    const privateGetFilteredValueColumns    = spyOn<any>(componentTableTools, '_getFilteredValueColumns').and.callThrough();
    componentTableTools.selectedColumns       = privateGetFilteredValueColumns.call(componentTableTools, defaultColumns);

    const privateGetDropDownColumns         = spyOn<any>(componentTableTools, '_getDropDownColumns').and.callThrough();
    componentTableTools.cleanedColumns        = privateGetDropDownColumns.call(componentTableTools, defaultColumns);

    component.columns                       = componentTableTools.columns;

    component.displayModal = true;
    component.ngAfterContentInit();
    component.onShow(null);

    fixture.detectChanges();

  }));

  afterEach(async(() => {
    fixture.destroy();
  }));

  it('La modal devrait être visible', () => {

    const modalEl: DebugElement = fixture.debugElement.query(By.css(".ui-widget.ui-widget-content"));

    expect(modalEl).toBeTruthy();
  });

  it('Test onDialogAddChange()', () => {

    let spy = spyOn(component, 'onDialogAddChange');

    const inputEl: DebugElement[] = fixture.debugElement.queryAll(By.css("input"));
    inputEl[0].nativeElement.value = 'Nouveau nom';
    inputEl[0].nativeElement.dispatchEvent(new Event('input'));

    fixture.whenStable().then(() => {
        expect(spy).toHaveBeenCalled();
    });
  });

  it('Test onDialogAddChange() avec retour de résultat', () => {

    const inputEl: DebugElement[] = fixture.debugElement.queryAll(By.css("input"));
    inputEl[0].nativeElement.value = 'Nouveau nom';
    inputEl[0].nativeElement.dispatchEvent(new Event('input'));

    component.onDialogAddChange(component.columns[0][1].onchange, component.columns[0][1], {name: "Nouveau nom"});

    expect(component.itemToAdd).toEqual(jasmine.objectContaining({name: 'Nouveau nom', unity: 999}));
  });

  it('Test onAutoComplete()', fakeAsync(() => {

    spyOn(componentTableTools.autoCompleteEvent, 'emit');

    component.onAutoComplete({query: "Test"}, component.columns[0][4]);

    expect(componentTableTools.autoCompleteEvent.emit).toHaveBeenCalled();
  }));

  it('Test maxlength validator', () => {
    
    component.columns[0][1].validators = [Validators.maxLength(6)];
    fixture.detectChanges();

    component.onAddRowClick({ name: "Je suis trop long" });

    expect(component.warningMsgs[0]).toEqual(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.maxlength.replace('[requiredLength]', 6).replace('[actualLength]', 17).replace('[field]', component.columns[0][1].header)
    }));
  });

  it('Test minlength validator', () => {
    
    component.columns[0][1].validators = [Validators.minLength(6)];
    fixture.detectChanges();

    component.onAddRowClick({ name: "mini" });

    expect(component.warningMsgs[0]).toEqual(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD, 
        detail: component.LANGAGE.TRANSLATE.minlength.replace('[requiredLength]', 6).replace('[actualLength]', 4).replace('[field]', component.columns[0][1].header)
    }));
  });

  it('Test required validator', fakeAsync(() => {
    
    component.columns[0][1].validators = [Validators.required];
    fixture.detectChanges();

    component.onAddRowClick({ name: "" });

    expect(component.warningMsgs[0]).toEqual(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD,
        detail: component.LANGAGE.TRANSLATE.required.replace('[field]', component.columns[0][1].header)
    }));
  }));

  it('Test numberPattern validator', fakeAsync(() => {
    
    component.columns[0][1].validators = [Validators.pattern(number_pattern)];
    fixture.detectChanges();

    component.onAddRowClick({ name: "OIJDS2" });

    expect(component.warningMsgs[0]).toEqual(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD,
        detail: component.LANGAGE.TRANSLATE.numberPattern.replace('[field]', component.columns[0][1].header)
    }));
  }));

  it('Test date_ddmmyyyy_Pattern validator (faux)', fakeAsync(() => {
    
    component.columns[0][1].validators = [Validators.pattern(date_ddmmyyyy_pattern)];
    fixture.detectChanges();

    component.onAddRowClick({ name: "2019/02/01" });

    expect(component.warningMsgs[0]).toEqual(jasmine.objectContaining({
        severity: 'warn', 
        summary: component.LANGAGE.ERROR_FIELD,
        detail: component.LANGAGE.TRANSLATE.date_ddmmyyyy_Pattern.replace('[field]', component.columns[0][1].header)
    }));
  }));

  it('Test date_ddmmyyyy_Pattern validator (vrai)', fakeAsync(() => {
    
    component.columns[0][1].validators = [Validators.pattern(date_ddmmyyyy_pattern)];
    fixture.detectChanges();

    component.onAddRowClick({ name: "31/12/2019" });

    expect(component.warningMsgs[0]).toBeFalsy();
  }));

  it('Test group panel', fakeAsync(() => {
    
    component.columns[0][1].panelGroup = "Group 1";
    component.columns[0][2].panelGroup = "Group 2";
    component.columns[0][3].panelGroup = "Group 1";
    component.columns[0][4].panelGroup = "Group 3";
    fixture.detectChanges();

    component['_buildGroupPanel']();

    expect(component.groupPanel).toEqual(['Group 1', 'Group 2', 'Group 3']);
    expect(component.groupAddPanelForm['Group 1'].length === 2).toBe(true);
    expect(component.groupAddPanelForm['Group 2'][0]).toEqual(jasmine.objectContaining({ field: 'unity', header: 'Unité', panelGroup: 'Group 2' }));
    expect(component.groupAddPanelForm['Group 3'][0]).toEqual(jasmine.objectContaining({
        field: 'autoCompleteField', 
        header: 'Auto Complete', 
        inputType: 'autocomplete', 
        panelGroup: 'Group 3'
    }));
  }));

});
  