# HOW TO USE 

<pre>
    <lib-add-modal [displayAddDialog]="displayAddDialog"
        [groupPanel]="groupPanel"
        [colsHeader]="colsHeader"
        [showLog]="showLog"
        (autoCompleteEvent)="onAutoComplete($event)"
        (addEvent)="onAddRowClick($event)">
    </lib-add-modal>
</pre>