import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TableToolsComponent } from '../table/table.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableModule, Table as PTable, TableService as PTableService, EditableColumn, Table } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from 'primeng';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PaginatorModule } from 'primeng/paginator';
import { SpinnerModule } from 'primeng/spinner';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DebugElement, ElementRef, ChangeDetectorRef, Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CmHeaderComponent } from './cm-header.component';
import { defaultColumns, defaultValues } from '../../jasmine-test/cm-header';
import { FilterMultiselectComponent } from '../filter/filter-multiselect/filter-multiselect.component';
import { FilterDateComponent } from '../filter/filter-date/filter-date.component';
import { FilterValueComponent } from '../filter/filter-value/filter-value.component';
import { SortAzComponent } from '../filter/sort-az/sort-az.component';
import { ToggleComponent } from '../filter/toggle/filter-toggle.component';
import { TableCellComponent } from '../table-cell/table-cell.component';
import { ContextMenuHeader } from '../../directives/cm-header.directive';
import { TooltipDirective } from '../../directives/tooltip.directive';
import { TooltipComponent } from '../tooltip/tooltip.component';
import { AddModalComponent } from '../add-modal/add-modal.component';
import { OnRejectDirective } from '../../directives/on-reject.directive';
import { OnUpdateDirective } from '../../directives/on-update.directive';
import { OnAddDirective } from '../../directives/on-add.directive';
import { OnDeleteDirective } from '../../directives/on-delete.directive';
import { OnDblclickRowDirective } from '../../directives/on-dblclick-row.directive';
import { By } from '@angular/platform-browser';

export class MockElementRef extends ElementRef {
    constructor() { super(null); }
}

export class MockChangeDetectorRef extends ChangeDetectorRef {
    checkNoChanges() {}
    detach() {}
    detectChanges() {}
    markForCheck() {}
    reattach() {}
}

export class MockMessageService extends MessageService {}

describe('CmHeaderComponent', () => {

  let component: CmHeaderComponent;
  let debugElement: DebugElement;

  let componentTableTools: TableToolsComponent;
  let fixture: ComponentFixture<TableToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        TableModule,
        DropdownModule,
        AutoCompleteModule,
        CheckboxModule,
        ContextMenuModule,
        DynamicDialogModule,
        InputTextareaModule,
        InputTextModule,
        MultiSelectModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        PanelModule,
        TooltipModule,
        CommonModule,
        TableModule,
        SharedModule,
        RadioButtonModule,
        CalendarModule,
        ToastModule,
        ConfirmDialogModule,
        PaginatorModule,
        SpinnerModule,
        BrowserAnimationsModule
      ],
      declarations: [
        TableCellComponent,
        TableToolsComponent,
        CmHeaderComponent, 
        SortAzComponent, 
        FilterDateComponent,
        ContextMenuHeader,
        FilterValueComponent,
        FilterMultiselectComponent,
        TooltipDirective,
        TooltipComponent,
        AddModalComponent,
        OnRejectDirective,
        OnUpdateDirective,
        OnAddDirective,
        OnDeleteDirective,
        OnDblclickRowDirective,
        ToggleComponent
      ],
      providers: [ 
          TableToolsComponent, 
          ConfirmationService, 
          PTable,
          PTableService,
          { provide: ElementRef, useClass: MockElementRef },
          { provide: ChangeDetectorRef, useClass: MockChangeDetectorRef },
          MessageService
        ]
    }).compileComponents().then(value => {

      fixture                                 = TestBed.createComponent(TableToolsComponent);
      componentTableTools                       = fixture.debugElement.injector.get(TableToolsComponent);
      debugElement                            = fixture.debugElement;

      component                               = TestBed.createComponent(CmHeaderComponent).componentInstance;

      componentTableTools.bodyTable             = { rows: [], columns: [], indexColumn: "id" };
      componentTableTools.value                 = defaultValues;
      componentTableTools.columns               = defaultColumns;

      const privateGetFilteredValueColumns    = spyOn<any>(componentTableTools, '_getFilteredValueColumns').and.callThrough();
      componentTableTools.selectedColumns       = privateGetFilteredValueColumns.call(componentTableTools, defaultColumns);

      const privateGetDropDownColumns         = spyOn<any>(componentTableTools, '_getDropDownColumns').and.callThrough();
      componentTableTools.cleanedColumns        = privateGetDropDownColumns.call(componentTableTools, defaultColumns);

      component.col                           = defaultColumns[0][1];
      component.tableElement                  = componentTableTools.pTable;
      component.dataTable                     = componentTableTools.value;

      fixture.detectChanges();
    });

  }));

  afterEach(async(() => {
    fixture.destroy();
  }));

  it('Test over element', () => {

    componentTableTools.tableService.contextMenuHeader$.subscribe(value => {
      expect(value).toEqual(jasmine.objectContaining({ col: defaultColumns[0][1], hide: false }));

    });

    let headerCell: DebugElement[] = debugElement.queryAll(By.css('p-table table tr th'));
    headerCell[1].triggerEventHandler('mouseover', null);
  });

});