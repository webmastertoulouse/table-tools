import { Component, OnDestroy, ElementRef, HostListener, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { TableToolsComponent } from '../table/table.component';
import { DomHandler } from 'primeng/dom';
import { ContextMenuHeader } from '../../models/cm-header';
import { Column } from '../../models/column';
import { Table as PTable } from 'primeng/table';
import { langage } from '../../config/langage';

@Component({
  selector: 'cm-header',
  templateUrl: './cm-header.component.html',
  styleUrls: ['./cm-header.component.scss']
})
export class CmHeaderComponent implements OnDestroy {

  @Input("cMHeader") col: Column;

  @Input("cMParent") parentElement: HTMLElement;

  @Input("cMTableParent") tableElement: PTable;

  @Input("cMDataTable") dataTable: any[];

  subscription: Subscription;

  public hideContextMenu: boolean = true;

  public LANGAGE: any = langage;

  constructor(private _tb: TableToolsComponent,
    private _el: ElementRef) {

    this.subscription = this._tb.tableService.contextMenuHeader$.subscribe((data: ContextMenuHeader) => {
      if (data.col && data.col.field == this.col.field) {
        this._position();
      } else {
        this._hidden();
      }
    });
  }

  @HostListener('mouseleave', ['$event']) mouseleave(event: Event) {
    this._hidden();

    if (this._tb.showLog === true) {
      console.log('CmHeaderComponent::HostListener::event', event);
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onHidden() {
    this._hidden();
  }

  resetFilter() {
    this._tb.onResetFilter();
  }

  private _isOneOfFilterActive(): boolean {
    return this.col.contextMenuHeader && (
      ((this.col.contextMenuHeader.DATE && this.col.contextMenuHeader.DATE.active)
      || (this.col.contextMenuHeader.MULTISELECT && this.col.contextMenuHeader.MULTISELECT.active)
      || (this.col.contextMenuHeader.STRING && this.col.contextMenuHeader.STRING.active)
      || (this.col.contextMenuHeader.ASC && this.col.contextMenuHeader.ASC.active)
      || (this.col.contextMenuHeader.DESC && this.col.contextMenuHeader.DESC.active)
      || (this.col.contextMenuHeader.BINARY && this.col.contextMenuHeader.BINARY.active))
    );
  }

  private _position(): void {
    if (this._isOneOfFilterActive()) {
      if (this.parentElement && this.hideContextMenu) {
  
          let colHeaderHeight                     = DomHandler.getOuterHeight(this.parentElement);
          this.parentElement.classList.add(this.LANGAGE.CLASS_LIST.ACTIVE);
          this._el.nativeElement.style.top        = `${colHeaderHeight}px`
          this._el.nativeElement.style.display    = 'block';
  
        setTimeout(() => {
  
          let body                                = this._el.nativeElement.closest('BODY');
          if (body) {
            let offsetParent                      = DomHandler.getOffset(this.parentElement);
            let parentWidth                       = DomHandler.getWidth(this.parentElement);
            let outerWidth                        = DomHandler.getOuterWidth(this._el.nativeElement);
            let widthElem                         = DomHandler.getWidth(this._el.nativeElement);
            let bodyWidth                         = DomHandler.getWidth(body);

            if ((offsetParent.left + outerWidth) > bodyWidth) {
              // widthElem++;
              this._el.nativeElement.style.left   = `-${(widthElem - parentWidth)}px`;
            } else {
              this._el.nativeElement.style.left   = 0;
            }
          }
        }, 0);
  
      }
      this.hideContextMenu                        = false;
    } else {
      this.hideContextMenu                        = true;
    }

    if (this._tb.showLog === true) {
      console.log('CmHeaderComponent::_position::_isOneOfFilterActive', this._isOneOfFilterActive());
    }
  }

  private _hidden(): void {
    if (this.parentElement && this._el) {
      this.hideContextMenu                          = true;
      this._el.nativeElement.style.display          = 'none';
  
      this.parentElement.classList.remove(this.LANGAGE.CLASS_LIST.ACTIVE);
    }
  }
}