import { Component, Input } from '@angular/core';
import { TableToolsComponent } from '../../table/table.component';
import { Column } from '../../../models/column';
import { langage } from '../../../config/langage';

@Component({
  selector: 'sort-az',
  templateUrl: './sort-az.component.html',
  styleUrls: ['./sort-az.component.scss']
})
export class SortAzComponent {

  @Input() col: Column;
  @Input() parent: HTMLElement;

  public LANGAGE: any = langage;

  public sortByValue: string;

  constructor(private _tb: TableToolsComponent) { }

  onClick(sortType: string) {
    let ths: NodeListOf<HTMLTableHeaderCellElement> = this.parent.parentElement.querySelectorAll('th');
    for (let th of ths[Symbol.iterator]()) {
      th.classList.remove(this.LANGAGE.CLASS_LIST.SORT_AZ);
      th.classList.remove(this.LANGAGE.CLASS_LIST.SORT_ZA);
    }

    this._tb.dispatchOnSort({ col: this.col, sortType: sortType, parent: this.parent });
    
    if (sortType == this.LANGAGE.SORT_TYPE.ASC) {
      this.parent.classList.add(this.LANGAGE.CLASS_LIST.SORT_AZ);
    } else if (sortType == this.LANGAGE.SORT_TYPE.DESC) { 
      this.parent.classList.add(this.LANGAGE.CLASS_LIST.SORT_ZA);
    }
  }

  public isActive() {
    if (this.col && this.col.contextMenuHeader && (
      (this.col.contextMenuHeader.ASC && this.col.contextMenuHeader.ASC.active === true)
      || (this.col.contextMenuHeader.DESC && this.col.contextMenuHeader.DESC.active === true)
    )) {
      return true;
    } else {
      return false;
    }
  }
}