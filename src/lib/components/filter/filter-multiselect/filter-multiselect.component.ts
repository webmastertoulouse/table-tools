import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { langage } from '../../../config/langage';
import { TableToolsComponent } from '../../table/table.component';
import { delay, removeDuplicate } from '../../../shared/utils';
import { Column } from '../../../models/column';

@Component({
  selector: 'filter-multiselect',
  templateUrl: './filter-multiselect.component.html',
  styleUrls: ['./filter-multiselect.component.scss']
})
export class FilterMultiselectComponent implements OnChanges {

  @Input() col: Column;
  @Input() parent: HTMLElement;

  @Input() dataTable: any[] = [];

  public LANGAGE: any = langage;

  public items: any[] = [];

  constructor(private _tb: TableToolsComponent) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.items.length == 0 && this.col && this.dataTable.length) {
      if (this.col.inputType == langage.COLUMNS.INPUT_TYPE.MULTISELECT) {
        this._getMultiSelectValue();
      } else {
        if (this.col.options && this.col.options.length > 0) {
          
          let label: string = this.col.keyField ? this.col.keyField : 'label';
          let value: string = this.col.dataKey ? this.col.dataKey : 'value';

          for (const opt of this.col.options) {
            this.items.push({ label: opt[label], value: opt[value] });
          }

        } else {

          for (let item of this.dataTable) {
            if (item[this.col.field]) {
              this.items.push({ label: item[this.col.field], value: item[this.col.field] });
            }
          }
        }
  
        this.items = removeDuplicate(this.items, 'label');
        this.items = [].concat(this.items);
      }
    }
  }

  onMultiSelectChange(event: any) {
    if (event) {
      delay(() => {
        this._tb.dispatchOnSort({
          values: (event.value && event.value.length) ? event.value.map(d => d.value) : [], 
          col: this.col, 
          sortType: this.LANGAGE.SORT_TYPE.MULTISELECT,
          filterMatchMode: 'in',
          parent: this.parent
        });
      }, 2500);
    }
  }

  public isActive() {
    return this.col && this.col.contextMenuHeader && this.col.contextMenuHeader.MULTISELECT && this.col.contextMenuHeader.MULTISELECT.active;
  }

  private _getMultiSelectValue(): void {
    if (this.col.options && this.col.options.length) {
      let label: string = this.col.keyField ? this.col.keyField : 'label';
      let value: string = this.col.dataKey ? this.col.dataKey : 'value';

      for (let item of this.dataTable) {

        if (item[this.col.field] && item[this.col.field].length) {
          for (let v of item[this.col.field]) {
            for (let option of this.col.options) {
              if (v == option[value]) {
                this.items = this.items.concat([{ label: option[label], value: option[value] }]);
                break;
              }
            }
          }
        }
      }
    }
    this.items = removeDuplicate(this.items, 'label');
  }
}