import { Component, Input } from '@angular/core';
import { Column } from '../../../models/column';
import { langage } from '../../../config/langage';
import { TableToolsComponent } from '../../table/table.component';

@Component({
  selector: 'filter-toggle',
  templateUrl: './filter-toggle.component.html',
  styleUrls: ['./filter-toggle.component.scss']
})
export class ToggleComponent {

  public LANGAGE: any = langage;

  @Input() col: Column;

  constructor(private _tb: TableToolsComponent) {}

  toggle(col: Column) {
    this._tb.onHideColumn(col);
  }

  group(col: Column) {
    this._tb.onGroupColumns(col);
  }
}