import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TableToolsComponent } from '../../table/table.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableModule, Table as PTable, TableService as PTableService } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from 'primeng';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PaginatorModule } from 'primeng/paginator';
import { SpinnerModule } from 'primeng/spinner';
import { ToggleComponent } from './filter-toggle.component';
import { ConfirmationService } from 'primeng/api';
import { DebugElement, ElementRef, ChangeDetectorRef } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { simpleColumns, filterToggleColumn, listStandard_Grouped } from '../../../jasmine-test/filter-toggle';
import { langage } from '../../../config/langage';

export class MockElementRef extends ElementRef {
    constructor() { super(null); }
}

export class MockChangeDetectorRef extends ChangeDetectorRef {
    checkNoChanges() {}
    detach() {}
    detectChanges() {}
    markForCheck() {}
    reattach() {}
}

describe('ToggleComponent', () => {
  let component: ToggleComponent;
  let fixture: ComponentFixture<ToggleComponent>;
  let debugElement: DebugElement;
  
  let componentTableTools: TableToolsComponent;
  let fixtureNsiTable: ComponentFixture<TableToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        TableModule,
        DropdownModule,
        AutoCompleteModule,
        CheckboxModule,
        ContextMenuModule,
        DynamicDialogModule,
        InputTextareaModule,
        InputTextModule,
        MultiSelectModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        PanelModule,
        TooltipModule,
        CommonModule,
        TableModule,
        SharedModule,
        RadioButtonModule,
        CalendarModule,
        ToastModule,
        ConfirmDialogModule,
        PaginatorModule,
        SpinnerModule,
        BrowserAnimationsModule
      ],
      declarations: [ 
        ToggleComponent
      ],
      providers: [ 
          TableToolsComponent, 
          ConfirmationService, 
          PTable,
          PTableService,
          { provide: ElementRef, useClass: MockElementRef },
          { provide: ChangeDetectorRef, useClass: MockChangeDetectorRef } 
        ]
    }).compileComponents();
    
    fixture = TestBed.createComponent(ToggleComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;

  }));

  
  beforeEach(async(() => {
    componentTableTools = fixture.debugElement.injector.get(TableToolsComponent);
        
    componentTableTools.bodyTable             = { rows: [], columns: [], indexColumn: "id" };
    componentTableTools.value                 = listStandard_Grouped;
    componentTableTools.columns               = simpleColumns;

    const privateGetFilteredValueColumns    = spyOn<any>(componentTableTools, '_getFilteredValueColumns').and.callThrough();
    componentTableTools.selectedColumns       = privateGetFilteredValueColumns.call(componentTableTools, simpleColumns);

    const privateGetDropDownColumns         = spyOn<any>(componentTableTools, '_getDropDownColumns').and.callThrough();
    componentTableTools.cleanedColumns        = privateGetDropDownColumns.call(componentTableTools, simpleColumns);

    component.LANGAGE                       = langage;
    component.col                           = filterToggleColumn;

    fixture.detectChanges();

  }));

  afterEach(async(() => {
    fixture.destroy();
  }));

  it('should have a defined component', () => {
    expect(component).toBeDefined();
  });

  it('should have a defined TableToolsComponent', () => {
    componentTableTools = fixture.debugElement.injector.get(TableToolsComponent);
    expect(componentTableTools).toBeDefined();
  });

  it('TableToolsComponent should have a defined component', () => {
    componentTableTools = fixture.debugElement.injector.get(TableToolsComponent);
    expect(componentTableTools).toBeDefined();
  });

  it('Test groupement de colonne', fakeAsync(() => {

    let clickEl: DebugElement = fixture.debugElement.query(By.css("#grouped-click"));
    clickEl.triggerEventHandler("click", null);
    
    tick(500);
    fixture.detectChanges();
    tick();

    expect(componentTableTools['_groupedColumns'][0]).toEqual(jasmine.objectContaining({header: 'Nom', field: 'name', key: 'AA', inputType: 'grouped_edit'}));
    expect(componentTableTools['_groupedColumns'][4]).toEqual(jasmine.objectContaining({header: 'Nom', field: 'name', key: 'BB', inputType: 'grouped_edit'}));
    expect(componentTableTools['_groupedColumns'][7]).toEqual(jasmine.objectContaining({header: 'Nom', field: 'name', key: 'CC', inputType: 'grouped_edit'}));
    expect(componentTableTools.columns[0][1].isGrouped).toBe(true);
    
    component.col = componentTableTools.columns[0][1];
    fixture.detectChanges();

    clickEl = fixture.debugElement.query(By.css("#ungrouped-click"));
    clickEl.triggerEventHandler("click", null);
    
    expect(componentTableTools.columns[0][1].isGrouped).toBe(false);
    expect(componentTableTools['_groupedColumns']).toEqual(null);

  }));

  it('Test cacher/montrer une colonne', fakeAsync(() => {

    let clickEl: DebugElement = fixture.debugElement.query(By.css("#showhide-col"));
    clickEl.triggerEventHandler("click", null);
    
    expect(componentTableTools.selectedColumns).toEqual(['id', 'description']);
    expect(componentTableTools.columns[0][1].display).toBe(false);
  }));

});