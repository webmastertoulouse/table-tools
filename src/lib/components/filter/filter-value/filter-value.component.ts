import { Component, Input } from '@angular/core';
import { TableToolsComponent } from '../../table/table.component';
import { langage } from '../../../config/langage';
import { Column } from '../../../models/column';
import { delay } from '../../../shared/utils';

@Component({
  selector: 'filter-value',
  templateUrl: './filter-value.component.html',
  styleUrls: ['./filter-value.component.scss']
})
export class FilterValueComponent {

  @Input() col: Column;
  @Input() parent: HTMLElement;

  public LANGAGE: any = langage;

  public inputText: string;

  constructor(private _tb: TableToolsComponent) { }

  onFilter(value: string): void {
    delay(() => {
      this._tb.dispatchOnSort({
        col: this.col, 
        input: value,
        filterMatchMode: 'contains',
        sortType: this.LANGAGE.SORT_TYPE.STRING,
        parent: this.parent
      });
    }, 1000);
  }

  public isActive() {
    if (this.col 
      && this.col.contextMenuHeader 
      && this.col.contextMenuHeader.STRING 
      && this.col.contextMenuHeader.STRING.active === true) {
      return true;
    } else {
      return false;
    }
  }
}