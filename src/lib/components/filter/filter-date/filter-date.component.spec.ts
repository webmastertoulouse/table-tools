import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TableToolsComponent } from '../../table/table.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TableModule, Table as PTable, TableService as PTableService, EditableColumn, Table } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { TooltipModule } from 'primeng/tooltip';
import { SharedModule } from 'primeng';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { PaginatorModule } from 'primeng/paginator';
import { SpinnerModule } from 'primeng/spinner';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DebugElement, ElementRef, ChangeDetectorRef, Component, IterableDiffers } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CmHeaderComponent } from '../../cm-header/cm-header.component';
import { defaultColumns, defaultValues } from '../../../jasmine-test/filter-date';
import { FilterMultiselectComponent } from '../../filter/filter-multiselect/filter-multiselect.component';
import { FilterValueComponent } from '../../filter/filter-value/filter-value.component';
import { SortAzComponent } from '../../filter/sort-az/sort-az.component';
import { ToggleComponent } from '../../filter/toggle/filter-toggle.component';
import { TableCellComponent } from '../../table-cell/table-cell.component';
import { ContextMenuHeader } from '../../../directives/cm-header.directive';
import { TooltipDirective } from '../../../directives/tooltip.directive';
import { TooltipComponent } from '../../tooltip/tooltip.component';
import { AddModalComponent } from '../../add-modal/add-modal.component';
import { OnRejectDirective } from '../../../directives/on-reject.directive';
import { OnUpdateDirective } from '../../../directives/on-update.directive';
import { OnAddDirective } from '../../../directives/on-add.directive';
import { OnDeleteDirective } from '../../../directives/on-delete.directive';
import { OnDblclickRowDirective } from '../../../directives/on-dblclick-row.directive';
import { By } from '@angular/platform-browser';
import { FilterDateComponent } from './filter-date.component';

export class MockElementRef extends ElementRef {
    constructor() { super(null); }
}

export class MockChangeDetectorRef extends ChangeDetectorRef {
    checkNoChanges() {}
    detach() {}
    detectChanges() {}
    markForCheck() {}
    reattach() {}
}

export class MockMessageService extends MessageService {}

describe('FilterDateComponent', () => {

  let component: FilterDateComponent;
  let debugElement: DebugElement;

  let componentCmHeader: CmHeaderComponent;

  let componentTableTools: TableToolsComponent;
  let fixture: ComponentFixture<TableToolsComponent>;

  let headerCell: DebugElement[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        TableModule,
        DropdownModule,
        AutoCompleteModule,
        CheckboxModule,
        ContextMenuModule,
        DynamicDialogModule,
        InputTextareaModule,
        InputTextModule,
        MultiSelectModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        PanelModule,
        TooltipModule,
        CommonModule,
        TableModule,
        SharedModule,
        RadioButtonModule,
        CalendarModule,
        ToastModule,
        ConfirmDialogModule,
        PaginatorModule,
        SpinnerModule,
        BrowserAnimationsModule
      ],
      declarations: [
        TableCellComponent,
        TableToolsComponent,
        CmHeaderComponent, 
        SortAzComponent, 
        FilterDateComponent,
        ContextMenuHeader,
        FilterValueComponent,
        FilterMultiselectComponent,
        TooltipDirective,
        TooltipComponent,
        AddModalComponent,
        OnRejectDirective,
        OnUpdateDirective,
        OnAddDirective,
        OnDeleteDirective,
        OnDblclickRowDirective,
        ToggleComponent
      ],
      providers: [ 
          TableToolsComponent, 
          ConfirmationService, 
          PTable,
          PTableService,
          { provide: ElementRef, useClass: MockElementRef },
          { provide: ChangeDetectorRef, useClass: MockChangeDetectorRef },
          MessageService
        ]
    }).compileComponents().then(value => {

      fixture                                 = TestBed.createComponent(TableToolsComponent);
      componentTableTools                       = fixture.debugElement.injector.get(TableToolsComponent);
      debugElement                            = fixture.debugElement;

      componentCmHeader                       = TestBed.createComponent(CmHeaderComponent).componentInstance;
      component                               = TestBed.createComponent(FilterDateComponent).componentInstance;

      componentTableTools.bodyTable             = { rows: [], columns: [], indexColumn: "id" };
      componentTableTools.value                 = defaultValues;
      componentTableTools.columns               = defaultColumns;

      const privateGetFilteredValueColumns    = spyOn<any>(componentTableTools, '_getFilteredValueColumns').and.callThrough();
      componentTableTools.selectedColumns       = privateGetFilteredValueColumns.call(componentTableTools, defaultColumns);

      const privateGetDropDownColumns         = spyOn<any>(componentTableTools, '_getDropDownColumns').and.callThrough();
      componentTableTools.cleanedColumns        = privateGetDropDownColumns.call(componentTableTools, defaultColumns);

      fixture.detectChanges();

      headerCell = debugElement.queryAll(By.css('p-table table tr th'));

      component.col                           = defaultColumns[0][1];
      component.parent                        = headerCell[2].nativeElement;
      componentCmHeader.parentElement         = headerCell[2].nativeElement;

      fixture.detectChanges();
    });

  }));

  afterEach(async(() => {
    fixture.destroy();
  }));

  it('p-calendar devrait avoir 2 inputs', () => {

    headerCell[2].triggerEventHandler('mouseover', null);
    fixture.detectChanges();

    let hCell: DebugElement[] = debugElement.queryAll(By.css('p-table table thead tr th.active cm-header p-calendar input'));

    expect(hCell.length === 2).toBe(true);

  });

  /* it('Test tri sur tableau par date', () => {

    component.start_date = new Date(2019, 3, 1);
    component['_start_date'] = component.start_date;
    component.end_date = new Date(2019, 3, 31);

    component.onClose(null);

    fixture.whenStable().then(value => {
      console.log('<{componentTableTools.value.length}>', componentTableTools.value.length)
    });

  }); */
});