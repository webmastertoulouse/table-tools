import { Component, OnInit, Input } from '@angular/core';
import { Column } from '../../../models/column';
import { TableToolsComponent } from '../../table/table.component';
import { delay } from '../../../shared/utils';
import { langage } from '../../../config/langage';

@Component({
  selector: 'filter-date',
  templateUrl: './filter-date.component.html',
  styleUrls: ['./filter-date.component.scss']
})
export class FilterDateComponent implements OnInit {

  @Input() col: Column;
  @Input() parent: HTMLElement;

  public LANGAGE: any = langage;

  public dateFormat: string = "dd/mm/yy";

  public start_date: any;
  public end_date: any;

  private _start_date: any;

  constructor(private _tb: TableToolsComponent) { }

  ngOnInit() { }

  onInput(event) {
    if (this._tb.tableService.showLog) {
      console.log('FilterDateComponent::onInput::event', event);
    }
  }

  onClose(event) {
    delay(() => {
      if (this._start_date != this.start_date) {
        this._start_date = this.start_date;
        this.end_date = null;
      } else if (this.start_date instanceof Date 
        && this.end_date instanceof Date) {
        if (this.start_date.getTime() < this.end_date.getTime()) {
          this._tb.dispatchOnSort({
            col: this.col, 
            sortType: this.LANGAGE.SORT_TYPE.DATE, 
            start_date: this.start_date,
            end_date: this.end_date,
            parent: this.parent
          });
        }
      }
    }, 1000);
  }

  public isActive() {
    return this.col && this.col.contextMenuHeader && this.col.contextMenuHeader.DATE && this.col.contextMenuHeader.DATE.active;
  }
}