import { Component, OnInit, Input } from '@angular/core';
import { Column } from '../../../models/column';
import { TableToolsComponent } from '../../table/table.component';
import { langage } from '../../../config/langage';

@Component({
  selector: 'filter-binary',
  templateUrl: './binary.component.html',
  styleUrls: ['./binary.component.scss']
})
export class BinaryComponent {

  @Input() col: Column;
  @Input() parent: HTMLElement;

  public LANGAGE: any = langage;

  constructor(private _tb: TableToolsComponent) { }

  onFilter(value: boolean): void {
    this._tb.dispatchOnSort({
      col: this.col, 
      input: value,
      sortType: this.LANGAGE.SORT_TYPE.BINARY,
      parent: this.parent
    });
  }

  public isActive() {
    if (this.col 
      && this.col.contextMenuHeader 
      && this.col.contextMenuHeader.BINARY 
      && this.col.contextMenuHeader.BINARY.active === true) {
      return true;
    } else {
      return false;
    }
  }

}
