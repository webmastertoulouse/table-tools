import { Column } from '../models/column';

export const icon = {
  onClick: (columns: Array<Column[]>, cb: (col: Column, rowData: any) => void, field: string): Array<Column[]> => {
    
    for (let i=0; i<columns.length; i++) {
      columns[i] = columns[i].map(o => o.field == field ? { ...o, icon: {...o.icon, onClick: cb } } : o);
    }

    return columns;
  }
}