import { columns } from '../jasmine-test/select';
import { columns as columnsUtils } from './columns';
import { BodyTable } from '../models/body-table';
import { select } from './select';

describe('select', () => {

    it('test clean select function', () => {
        let bodyTable: BodyTable = { rows: [], columns: columns, indexColumn: "id" };
        bodyTable.columns = columnsUtils.options(bodyTable.columns, [
            { value: 1, label: "label 1" },
            { value: 2, label: "label 2" },
            { value: 3, label: "label 3" },
            { value: 4, label: "label 4" },
            { value: 5, label: "label 5" },
            { value: 6, label: "label 6" },
            { value: 7, label: "label 7" },
            { value: 8, label: "label 8" },
            { value: 9, label: "label 9" }
        ], 'type_service');

        expect(select.clean({type_service: columns[0][3].options[1]}, columns[0][3]) === 2).toBe(true);
        expect(select.clean({type_service: 3}, columns[0][3]) === 3).toBe(true);
    });

    it('test clean select function 2', () => {
        let bodyTable: BodyTable = { rows: [], columns: columns, indexColumn: "id" };
        bodyTable.columns = columnsUtils.options(bodyTable.columns, [
            { id: 1, name: "label 1" },
            { id: 2, name: "label 2" },
            { id: 3, name: "label 3" },
            { id: 4, name: "label 4" },
            { id: 5, name: "label 5" },
            { id: 6, name: "label 6" },
            { id: 7, name: "label 7" },
            { id: 8, name: "label 8" },
            { id: 9, name: "label 9" }
        ], 'type_unit_name');

        expect(select.clean({type_unit_name: columns[0][6].options[2]}, columns[0][6]) === 3).toBe(true);
    });
});