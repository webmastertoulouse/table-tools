import { Column } from "../models/column";
import { ContextMenuHeaderConfig } from '../models/cm-header-config';

export const columns = {
    options: (columns: Array<Column[]>, options: any[], field: string) => {
        for (let i=0; i<columns.length; i++) {
            columns[i] = columns[i].map(o => o.field == field ? { ...o, options: options} : o);
        }
        return columns;
    },
    onChange: (columns: Array<Column[]>, onChange: (value: Column, item: any) => Promise<any>, field: string) => {
        dance:
        for (let i=0; i<columns.length; i++) {
            for (let y=0; y<columns[i].length; y++) {
                if (field == columns[i][y].field) {
                    columns[i][y].onchange = onChange;
                    break dance;
                }
            }
        }
        return columns;
    },
    valueChange: (columns: Array<Column[]>, valueChange: (value: Column, item: any) => any, field: string) => {
        dance:
        for (let i=0; i<columns.length; i++) {
            for (let y=0; y<columns[i].length; y++) {
                if (field == columns[i][y].field) {
                    columns[i][y].valueChange = valueChange;
                    break dance;
                }
            }
        }
        return columns;
    },
    contextMenuHeader: (columns: Array<Column[]>, contextMenuHeaderConfig: ContextMenuHeaderConfig, field: string) => {
        dance:
        for (let i=0; i<columns.length; i++) {
            for (let y=0; y<columns[i].length; y++) {
                if (field == columns[i][y].field) {
                    columns[i][y].contextMenuHeader = contextMenuHeaderConfig;
                    break dance;
                }
            }
        }
        return columns;
    }
}