import { Column } from "../models/column";

export const select = {
  clean: (rowData: any, col: Column): string|number|boolean => {

    let findValue = [];
    const _value = col.dataKey ? col.dataKey : 'value';
    const _label = col.keyField ? col.keyField : 'label';

    if (col.options && col.options.length && rowData[col.field]) {

      findValue = col.options.filter(value => {
        if (typeof rowData[col.field] == 'object' 
          && rowData[col.field][value] == value[_value]) {
          return value[_value];
        } else if (rowData[col.field] == value[_value]) {
          return value[_value];
        } else if (rowData[col.field] == value[_label]) {
          return value[_value];
        }
      });

    }
    
    if (rowData[col.field] && rowData[col.field][_value]) {
      return rowData[col.field][_value];
    } else if (findValue.length > 0 && findValue[0] !== false) {
      return (typeof findValue[0] == 'object') ? (findValue[0].value ? findValue[0].value : findValue[0][_value]) : findValue[0];
    } else if (rowData[col.field]) {
      return rowData[col.field];
    } else {
      return null;
    }
  },
  getLabel: (value: any, col: Column): string|boolean => {
    let findValue = [];

    if (col && col.options && col.options.length) {
      findValue = col.options.filter(o => {
        if ((col.dataKey && col.keyField) && value[col.field] && o[col.dataKey] == value[col.field].dataKey) {
          return value[col.field].keyField;
        } else if ((col.dataKey && col.keyField) && value[col.field] && o.value == value[col.field].dataKey) {
          return value[col.field].keyField;
        } else if ((col.dataKey && o.value) && value[col.field] && o.value == value[col.field].value) {
          return value[col.field].keyField;
        } else if ((o.value && o.value == value[col.field]) || (value[col.field] && o[col.dataKey] && o[col.dataKey] == value[col.field])) {
          return value[col.field];
        } else {
          return false;
        }
      });
    }

    if (col.keyField && value[col.field] && value[col.field][col.keyField]) {
      return value[col.field][col.keyField];
    } else if (value[col.field] && value[col.field].label) {
      return value[col.field].label;
    } else if (value[col.field] && value[col.field].name) {
      return value[col.field].name;
    } else if (findValue.length > 0) {
      return (typeof findValue[0] == 'object') ? (findValue[0].header ? findValue[0].header : findValue[0][col.keyField]) : findValue[0];
    } else if (typeof value[col.field] == 'string') {
      return value[col.field];
    } else {
      return false;
    }
  },
  cleanAll: (value: any, columns: Array<Column[]>): any => {
    for (let column of columns) {
      for (let col of column) {
        if (value[col.field]) value[col.field] = select.clean(value, col);
      }
    }

    return value;
  }
}