/**
 * Used when a request need a delay auto reset before sending to server
 */
export const delay = (function() {
    let timer: any = null;
    return function(callback: any, ms: number) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

/**
 * Supprime les objets en double dans un tableau
 */
export const removeDuplicate = (arr, comp) => {

    const unique = arr
        .map(e => e[comp])
        // store the keys of the unique objects
        .map((e, i, final) => final.indexOf(e) === i && i)
        // eliminate the dead keys & store unique objects
        .filter(e => arr[e]).map(e => arr[e])
    ;
  
     return unique;
}