export const langage = {
    "ADD": "Ajouter",
    "EXPORT": "Exporter",
    "UPDATE": "Mettre à jour",
    "PRINT": "Imprimer",
    "SAVE": "Ajouter",
    "CANCEL": "Annuler",
    "FILTER": "Filtre",
    "DESCRIPTION": "Description",
    "REMOVE": "Supprimer",
    "ICON": {
        "REMOVE": "pi pi-fw pi-trash"
    },
    "EMPTY_COL": "----",
    "EMPTY_VALUE": "Valeur non renseignée",
    "TRUE": "Oui",
    "FALSE": "Non",
    "CHOOSE_OPTION": "Choisissez une option",
    "TOGGLE_COLUMNS": {
        "SELECTED_ITEMS_LABELS": "{0} colonnes sélectionnées",
        "CHOOSE_COLUMN": "Choisir une colonne"
    },
    "REQUIRED_FIELD": "Champs requis",
    "ERROR_FIELD": "Mauvaise saisie",
    "REQUIRED_MESSAGE": "'Certain(s) champ(s) obligatoire(s) sont manquant(s).'",
    "DEFAUT_PANEL_ADD_TITLE": "Formulaire d'ajout",
    "TRANSLATE": {
        "maxlength": "[field] : ce champ doit être inférieur ou égal à [requiredLength] caractères (actuellement : [actualLength])",
        "minlength": "[field] : ce champ doit être supérieur ou égal à [requiredLength] caractères (actuellement : [actualLength])",
        "required": "[field] : ce champ est requis.",
        "numberPattern": "[field] : ce champ doit être un nombre",
        "date_ddmmyyyy_Pattern": "[field] : ce champ doit être une date au format dd/mm/yyyy"
    },
    "CONTEXT_MENU": {
        "GROUP_COLUMN": "Grouper la colonne",
        "UNGROUP_COLUMN": "Dégrouper la colonne",
        "GROUP_COLUMN_ICON": "pi pi-list",
        "GROUPED_CLASS": "grouped-col"
    },
    "TABLE_TYPE": {
        "SIMPLE_EDIT": "simple_edit",
        "GROUPED_EDIT": "grouped_edit",
        "UNGROUPED_EDIT": "ungrouped_edit",
        "GROUPED_EDIT_FOOTER": "grouped_edit_footer"
    },
    "CONFIRM_DELETE": "Confirmer la supression",
    "CONFIRM": "Confirmation",
    "SORT_TYPE": {
        "ASC": "ASC",
        "DESC": "DESC",
        "DATE": "DATE",
        "STRING": "STRING",
        "MULTISELECT": "MULTISELECT",
        "BINARY": "BINARY"
    },
    "CONTEXTMENU_HEADER": {
        "ICON_INFO": "pi pi-info-circle",
        "INFO": "Text de l'info bull pour expliciter les filtres de la colonne.",
        "REMOVE_FILTER": "Supprimer les filtres"
    },
    "CLASS_LIST": {
        "SORT_AZ": "sort-az",
        "SORT_ZA": "sort-za",
        "ACTIVE": "active"
    },
    "FILTERS": {
        "DATE": {
            "INFO": "Filtre sur date",
            "FROM": "Du :",
            "TO": "Au :"
        },
        "MULTIPLE": {
            "INFO": "Sélection multiple",
            "DEFAULT_LABEL": "Choisissez"
        },
        "STRING": {
            "INFO": "Filtre par valeur saisie"
        },
        "SORT": {
            "INFO": "Filtre par ordre croissant/décroissant"
        },
        "TOGGLE": {
            "INFO": "Cacher cette colonne"
        },
        "GROUP": {
            "INFO": "Grouper cette colonne",
            "INFO_UNGROUPED": "Dégrouper cette colonne",
            "EXPAND": "Étendre",
            "CONTRACT": "Rétrécir"
        },
        "BINARY": {
            "INFO": "Filtre par oui/non"
        }
    },
    "PAGINATOR": {
        "NUMBER_ELEMENT": "Éléments par page :",
        "ON": "sur"
    },
    "PLACEHOLDER_SEARCH": "Entrez votre recherche",
    "MESSAGES": {
        "ON_REJECT": "Une erreur est survenue.",
        "ON_ADD": "La ligne a bien été ajoutée.",
        "ON_DELETE": "La ligne a bien été supprimée.",
        "ON_UPDATE": "La mise à jour a bien été effectuée.",
        "ON_DISABLE_ROW": "La ligne n'est pas éditable.",
    },
    "COLUMNS": {
        "MULTI_SELECT_SEPARATOR": ",",
        "INPUT_TYPE": {
            "MULTISELECT": "selectmultiple"
        },
        "SELECTED": "{0} colonnes sélectionnées",
        "DEFAULT_LABEL": "Choisissez des colonnes"
    },
    "RESULT": "résultat",
    "RESULTS": "résultats"
};