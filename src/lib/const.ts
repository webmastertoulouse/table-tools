export const date_ddmmyyyy_pattern = '[0-9]{2}\/{1}[0-9]{2}\/{1}[0-9]{4}';

export const number_pattern = '[0-9]*';

export const customEvent: any = {
    __ADD__: '__ADD__',
    __DBLCLICK__: '__DBLCLICK__'
};