import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { TooltipModule } from 'primeng/tooltip';
import { PanelModule } from 'primeng/panel';
import {CalendarModule} from 'primeng/calendar';
import {ToastModule} from 'primeng/toast';
import {PaginatorModule} from 'primeng/paginator';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {SpinnerModule} from 'primeng/spinner';
import {PasswordModule} from 'primeng/password';
import {ColorPickerModule} from 'primeng/colorpicker';

import { FilterValueComponent } from './components/filter/filter-value/filter-value.component';
import { FilterMultiselectComponent } from './components/filter/filter-multiselect/filter-multiselect.component';
import { TooltipDirective } from './directives/tooltip.directive';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { OnRejectDirective } from './directives/on-reject.directive';
import { AddModalComponent } from './components/add-modal/add-modal.component';
import { OnUpdateDirective } from './directives/on-update.directive';
import { OnAddDirective } from './directives/on-add.directive';
import { OnDeleteDirective } from './directives/on-delete.directive';
import { OnDblclickRowDirective } from './directives/on-dblclick-row.directive';
import { ToggleComponent } from './components/filter/toggle/filter-toggle.component';
import { OnDisableRowDirective } from './directives/on-disable-row.directive';

import { TableToolsComponent } from './components/table/table.component';
import { CmHeaderComponent } from './components/cm-header/cm-header.component';
import { SortAzComponent } from './components/filter/sort-az/sort-az.component';
import { FilterDateComponent } from './components/filter/filter-date/filter-date.component';
import { TableCellComponent } from './components/table-cell/table-cell.component';

import { ContextMenuHeader } from './directives/cm-header.directive';
import { SharedModule } from './shared/shared.module';
import { BinaryComponent } from './components/filter/binary/binary.component';

@NgModule({
  declarations: [
    TableCellComponent,
    TableToolsComponent,
    CmHeaderComponent, 
    SortAzComponent, 
    FilterDateComponent,
    ContextMenuHeader,
    FilterValueComponent,
    FilterMultiselectComponent,
    TooltipDirective,
    TooltipComponent,
    AddModalComponent,
    OnRejectDirective,
    OnUpdateDirective,
    OnAddDirective,
    OnDeleteDirective,
    OnDblclickRowDirective,
    ToggleComponent,
    OnDisableRowDirective,
    BinaryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    DropdownModule,
    AutoCompleteModule,
    CheckboxModule,
    ContextMenuModule,
    DynamicDialogModule,
    InputTextareaModule,
    InputTextModule,
    MultiSelectModule,
    DialogModule,
    MessagesModule,
    MessageModule,
    PanelModule,
    TooltipModule,
    SharedModule,
    RadioButtonModule,
    CalendarModule,
    ToastModule,
    ConfirmDialogModule,
    PaginatorModule,
    SpinnerModule,
    PasswordModule,
    HttpClientModule,
    ColorPickerModule
  ],
  exports: [TableToolsComponent]
})
export class TableToolsModule { }
