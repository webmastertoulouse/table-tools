import { Validators } from "@angular/forms";
import { Column } from "../models/column";
import { OnSort } from "../models/on-sort";

// Exemple de ligne avant l'appel d'un update onUpdate()
export const rowTest1 = <any>{
    id: "363",
    pro_product_id: "121",
    code_service: "11",
    type_service: {label: "FD", value: "FD"},
    type_service_name: "FD",
    pro_heading_invoice: "11",
    name: "CG - Aide à Domicile PA",
    pro_type_unit_id: "2",
    type_unit_name: "jour",
    colibri_code_service: null,
    ord_type_cadre_intervention_id: "4",
    type_cadre_intervention_name: {id: "2", name: "Mandataire", shortName: "Mand."},
    tg_pro_product_id: "121",
    product_name: "AM PERSONNE AGEE (AMPA)",
    is_sequence: "1",
    is_reconcile: "1",
    is_overlap: "1",
    code_service_svi: null,
    with_kilometer: false,
    is_holiday: false,
    with_duration: false,
    sort_value: "1",
    tg_type_transaction: {id: "10", name: "Fiche présence SP"},
    gbl_mandant_id: "2",
    is_not_synced: false,
};

// Exemple de ligne qui doit sortir quand rowTest1 est transmit dans onUpdate() puis envoyé dans le emit()
// les select par exemple doivent être nettoyées et seule leur valeur doivent être transmises dans le emit()
export const rowTest2 = <any>{
    id: "363",
    pro_product_id: "121",
    code_service: "11",
    type_service: "FD",
    type_service_name: "FD",
    pro_heading_invoice: "11",
    name: "CG - Aide à Domicile PA",
    pro_type_unit_id: "2",
    type_unit_name: "jour",
    colibri_code_service: null,
    ord_type_cadre_intervention_id: "4",
    type_cadre_intervention_name: "2",
    tg_pro_product_id: "121",
    product_name: "AM PERSONNE AGEE (AMPA)",
    is_sequence: "1",
    is_reconcile: "1",
    is_overlap: "1",
    code_service_svi: null,
    with_kilometer: false,
    is_holiday: false,
    with_duration: false,
    sort_value: "1",
    tg_type_transaction: {id: "10", name: "Fiche présence SP"},
    gbl_mandant_id: "2",
    is_not_synced: false
};

// Tableau de résultat (liste standard)
export const listStandard_Grouped: any[] = [
    { id: "1", name: "ZA ENJOY AGEE (AMPA)", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA" },
    { id: "2", name: "AM BLABLA AGEE (AMPA)", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "BB" },
    { id: "3", name: "WT KALIKOVSKAIA AGEE (AMPA)", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA" },
    { id: "4", name: "ML MACHOUPITCHOU AGEE (AMPA)", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "CC" },
    { id: "5", name: "GH NANANERE AGEE (AMPA)", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA" },
    { id: "6", name: "OP FAUTPASREVER AGEE (AMPA)", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "BB" }
];

export const columns: Array<Column[]> = [
    [
        { field: "id", header: "ID", inputType: null },
        { field: "code_service", header: "Code service", inputType: "input", placeholder: "Code dans le logiciel de Paye", panelGroup: "Paramétrage du service", validators: [Validators.max(99), Validators.pattern("^[0-9]*$")], tooltip: {text: "Code du service dans le logiciel de Paye/Facturation externe" } },
        { field: "type_service", header: "Type de service", inputType: "select", options: [], panelGroup: "Paramétrage du service", validators: [Validators.required, Validators.maxLength(2)], tooltip: {text: "FD=Fiche présence, FA=Entrée/Sortie, F?=Elément facturable (se rapporter à la documentation), P ?=Hors intervention (se rapporter à la documentation)" }, contextMenuHeader: {MULTISELECT: {active: true}} },
        { field: "name", header: "Libellé", inputType: "input", placeholder: "Renseigner un libellé personnalisé", panelGroup: "Paramétrage du service", validators: [Validators.required, Validators.maxLength(55)] },
        { field: "product_name", header: "Nom", inputType: "autocomplete", placeholder: "Sélectionner un produit TG", options: [], keyField: "label", emptyMessage: "Aucun résultat", panelGroup: "Produit TG", validators: [Validators.required], tooltip: {text: "Sélectionner un produit dans le catalogue national.\n<br />Un produit TG est unique par association.\n<br />Sélection obligatoire." } },
        { field: "type_unit_name", header: "Unité", inputType: "select", options: [], keyField: "name", dataKey: "id", disabled: true, panelGroup: "Produit TG", validators: [Validators.required] },
        { field: "type_cadre_intervention_name", header: "Mode d'intervention", inputType: "select", placeholder: "Sélectionner le mode si nécessaire", options: [], dataKey: "id", keyField: "name", panelGroup: "Paramétrage du service", tooltip: {text: "Menu dans lequel sera affiché le service dans le formulaire du CRI" } },
        { field: "with_kilometer", header: "Pas de kms ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de bloquer la saisie de kilomètres (nombre et type de trajet)  pour un service.\n<br />Coché = l'intervenant ne pourra pas saisir de kilomètres." } },
        { field: "with_duration", header: "Avec une durée ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de déterminer le mode de saisie des heures d'interventions.\n<br />Coché = L'intervenant devra renseigner une durée en heure/minute.\n<br />Décoché = L'intervenant devra renseigner un début et un fin.\n<br />Ce paramètre ne s'applique qu'aux services d'unité \"Heure\" ou \"Minute\"." } },
        { field: "is_holiday", header: "Congé ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de définir un service comme étant une absence/un congé. Ceci est utile pour comptabiliser les congés/absences sont les impressions (fiche de présence).\n<br />Ce paramètre permet aussi de bloquer la saisie de kilomètres sur le mobile des intervenants." } },
        { field: "tg_type_transaction", header: "Type transaction", inputType: "select", options: [], keyField: "name", panelGroup: "Paramétrage du service", cleanValue: false },
        { field: "is_not_synced", header: "Pas synchro mobile", inputType: "checkbox", panelGroup: "Produit TG", disabled: true },
        { field: "is_reconcile", header: "Est rapproché au planning ?", inputType: "checkbox", panelGroup: "Produit TG", disabled: true, tooltip: {text: "Précise si les CRIs associés au produit TG sélectionné seront rapprochés au planning" } },
        { field: "is_sequence", header: "Est séquencé ?", inputType: "checkbox", panelGroup: "Produit TG", disabled: true }
    ]
];

export const iconColumns: Array<Column[]> = [
    [
        { field: "id", header: "ID", inputType: null },
        { field: "checked", header: "Est valide", inputType: null, icon: { cb: (col: Column, rowData) => {
            return '<i class="fas fa-times-circle"></i>';
         }, onClick: (col, rowData) => {
             console.log(col, rowData);
         } } },
        { field: "code_service", header: "Code service", inputType: "input", placeholder: "Code dans le logiciel de Paye", panelGroup: "Paramétrage du service", validators: [Validators.max(99), Validators.pattern("^[0-9]*$")], tooltip: {text: "Code du service dans le logiciel de Paye/Facturation externe" } },
        { field: "type_service", header: "Type de service", inputType: "select", options: [], panelGroup: "Paramétrage du service", validators: [Validators.required, Validators.maxLength(2)], tooltip: {text: "FD=Fiche présence, FA=Entrée/Sortie, F?=Elément facturable (se rapporter à la documentation), P ?=Hors intervention (se rapporter à la documentation)" }, contextMenuHeader: {MULTISELECT: {active: true}} },
        { field: "name", header: "Libellé", inputType: "input", placeholder: "Renseigner un libellé personnalisé", panelGroup: "Paramétrage du service", validators: [Validators.required, Validators.maxLength(55)] },
        { field: "product_name", header: "Nom", inputType: "autocomplete", placeholder: "Sélectionner un produit TG", options: [], keyField: "label", emptyMessage: "Aucun résultat", panelGroup: "Produit TG", validators: [Validators.required], tooltip: {text: "Sélectionner un produit dans le catalogue national.\n<br />Un produit TG est unique par association.\n<br />Sélection obligatoire." } },
        { field: "type_unit_name", header: "Unité", inputType: "select", options: [], keyField: "name", dataKey: "id", disabled: true, panelGroup: "Produit TG", validators: [Validators.required] },
        { field: "type_cadre_intervention_name", header: "Mode d'intervention", inputType: "select", placeholder: "Sélectionner le mode si nécessaire", options: [], dataKey: "id", keyField: "name", panelGroup: "Paramétrage du service", tooltip: {text: "Menu dans lequel sera affiché le service dans le formulaire du CRI" } },
        { field: "with_kilometer", header: "Pas de kms ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de bloquer la saisie de kilomètres (nombre et type de trajet)  pour un service.\n<br />Coché = l'intervenant ne pourra pas saisir de kilomètres." } },
        { field: "with_duration", header: "Avec une durée ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de déterminer le mode de saisie des heures d'interventions.\n<br />Coché = L'intervenant devra renseigner une durée en heure/minute.\n<br />Décoché = L'intervenant devra renseigner un début et un fin.\n<br />Ce paramètre ne s'applique qu'aux services d'unité \"Heure\" ou \"Minute\"." } },
        { field: "is_holiday", header: "Congé ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de définir un service comme étant une absence/un congé. Ceci est utile pour comptabiliser les congés/absences sont les impressions (fiche de présence).\n<br />Ce paramètre permet aussi de bloquer la saisie de kilomètres sur le mobile des intervenants." } },
        { field: "tg_type_transaction", header: "Type transaction", inputType: "select", options: [], keyField: "name", panelGroup: "Paramétrage du service", cleanValue: false },
        { field: "is_not_synced", header: "Pas synchro mobile", inputType: "checkbox", panelGroup: "Produit TG", disabled: true },
        { field: "is_reconcile", header: "Est rapproché au planning ?", inputType: "checkbox", panelGroup: "Produit TG", disabled: true, tooltip: {text: "Précise si les CRIs associés au produit TG sélectionné seront rapprochés au planning" } },
        { field: "is_sequence", header: "Est séquencé ?", inputType: "checkbox", panelGroup: "Produit TG", disabled: true }
    ]
];

export const simpleColumns: Array<Column[]> = [
    [
        { field: "id", header: "ID", inputType: null },
        { field: "name", header: "Libellé", inputType: "input", placeholder: "Renseigner un libellé personnalisé", panelGroup: "Paramétrage du service", validators: [Validators.required, Validators.maxLength(55)] },
    ]
];

export const simpleGroupedColumns: Array<Column[]> = [
    [
        { field: "id", header: "ID", inputType: null },
        { field: "name", header: "Libellé", inputType: "input" },
        { field: "description", header: "description", inputType: "input" },
        { field: "groupedField", header: "groupedField", inputType: "input" }
    ]
];

export const columnTestSelect1: Column = { 
    field: "type_unit_name", 
    header: "Unité", 
    inputType: "select", 
    options: [
        {id: "1", name: "Jour"},
        {id: "2", name: "Nuit"},
        {id: "3", name: "Machin"},
        {id: "4", name: "Nia nia nia"},
        {id: "5", name: "prout"}
    ], 
    keyField: "name", 
    dataKey: "id", 
    disabled: true, 
    validators: [Validators.required] 
};

export const filterToggleColumn = { 
    field: "name", 
    header: "Nom", 
    inputType: "input"
};

let addCheckboxDefaultValues = (list: any[]): any[] => {
    for (let i=0; i<list.length; i++) {
        list[i] = {...list[i], ...{ with_kilometer: false, with_duration: false, is_holiday: false, is_not_synced: false, is_reconcile: false, is_sequence: false } }
    }
    return list;
}

const columnTestInput1: Column = {
    field: "name", 
    header: "Libellé", 
    inputType: "input", 
    placeholder: "Renseigner un libellé personnalisé", 
    validators: [Validators.required, Validators.maxLength(55)]
};

const columnTestDate1: Column = {
    field: "date", 
    header: "Date", 
    inputType: "date", 
    placeholder: "Renseigner une date"
};

// Tableau de résultat (liste standard)
export const listStandard_Sort_A_Z: any[] = addCheckboxDefaultValues([
    { id: "1", name: "ZA ENJOY AGEE (AMPA)" },
    { id: "2", name: "AM BLABLA AGEE (AMPA)" },
    { id: "3", name: "WT KALIKOVSKAIA AGEE (AMPA)" },
    { id: "4", name: "ML MACHOUPITCHOU AGEE (AMPA)" },
    { id: "5", name: "GH NANANERE AGEE (AMPA)" },
    { id: "6", name: "OP FAUTPASREVER AGEE (AMPA)" }
]);

// Table sorted A-Z sur name
export const listStandard_Sort_A_Z__SORTED: any[] = addCheckboxDefaultValues([
    { id: "2", name: "AM BLABLA AGEE (AMPA)" },
    { id: "5", name: "GH NANANERE AGEE (AMPA)" },
    { id: "4", name: "ML MACHOUPITCHOU AGEE (AMPA)" },
    { id: "6", name: "OP FAUTPASREVER AGEE (AMPA)" },
    { id: "3", name: "WT KALIKOVSKAIA AGEE (AMPA)" },
    { id: "1", name: "ZA ENJOY AGEE (AMPA)" }
]);

export const OnsortTestValue_AZ: OnSort = {
    col: columnTestInput1,
    sortType: "ASC"
};

export const listStandard_Sort_Date: any[] = addCheckboxDefaultValues([
    { id: "1", date: "2019-08-01" },
    { id: "2", date: "2019-05-01" },
    { id: "3", date: "2019-04-01" },
    { id: "4", date: "2019-07-01" },
    { id: "5", date: "2019-09-01" },
    { id: "6", date: "2019-03-01" },
    { id: "7", date: "2019-06-01" }
]);

export const listStandard_Sort_Date__SORTED: any[] = addCheckboxDefaultValues([
    { id: "2", date: "2019-05-01" },
    { id: "3", date: "2019-04-01" },
    { id: "6", date: "2019-03-01" }
]);

export const OnsortTestValue_DATE: OnSort = {
    col: columnTestDate1,
    sortType: "DATE",
    start_date: new Date("2019-01-01"),
    end_date: new Date("2019-05-02")
};

export const listStandard_Sort_String: any[] = listStandard_Sort_A_Z;

export const listStandard_Sort_String__SORTED: any[] = addCheckboxDefaultValues([
    { id: "5", name: "GH NANANERE AGEE (AMPA)" }
]);

export const OnsortTestValue_STRING: OnSort = {
    col: columnTestInput1,
    input: "nananere",
    filterMatchMode: "contains",
    sortType: "STRING"
};

export const listStandard_Sort_Multiselect: any[] = addCheckboxDefaultValues([
    { id: "2", type_cadre_intervention_name: "FA" },
    { id: "3", type_cadre_intervention_name: "HG" },
    { id: "4", type_cadre_intervention_name: "OI" },
    { id: "1", type_cadre_intervention_name: "FD" },
    { id: "5", type_cadre_intervention_name: "TR" },
    { id: "6", type_cadre_intervention_name: "RE" },
    { id: "7", type_cadre_intervention_name: "GF" }
]);

export const listStandard_Sort_Multiselect2: any[] = addCheckboxDefaultValues([
    { id: "2", type_cadre_intervention_name: "FA,HG,JK,IU,MS" },
    { id: "3", type_cadre_intervention_name: "HG,IU,HG,OI,OC" },
    { id: "4", type_cadre_intervention_name: "OI,TY,FD,RE,KJ" },
    { id: "1", type_cadre_intervention_name: "FD,ML,DS,RE,ZE" },
    { id: "5", type_cadre_intervention_name: "TR,HG,JD,PQ,KL" },
    { id: "6", type_cadre_intervention_name: "RE,YH,BV,FD,RM" },
    { id: "7", type_cadre_intervention_name: "GF,IU,XS,QD,KJ" }
]);

export const listStandard_Sort_Multiselect__SORTED: any[] = addCheckboxDefaultValues([
    { id: "1", type_cadre_intervention_name: "FD" }
]);

export const listStandard_Sort_Multiselect2__SORTED: any[] = addCheckboxDefaultValues([
    { id: "4", type_cadre_intervention_name: "OI,TY,FD,RE,KJ" },
    { id: "1", type_cadre_intervention_name: "FD,ML,DS,RE,ZE" },
    { id: "6", type_cadre_intervention_name: "RE,YH,BV,FD,RM" }
]);

export const OnsortTestValue_MULTISELECT: OnSort = {
    values: [ "FD" ],
    col: { 
        field: "type_cadre_intervention_name", 
        header: "Mode d'intervention", 
        inputType: "select", 
        placeholder: "Sélectionner le mode si nécessaire", 
        options: [ {label: "FD", value: "FD"} ], 
        keyField: "name", 
        panelGroup: "Paramétrage du service", 
        tooltip: { text: "Menu dans lequel sera affiché le service dans le formulaire du CRI" }, 
        contextMenuHeader: { MULTISELECT: { active: true } }
    },
    sortType: "MULTISELECT",
    filterMatchMode: "in"
}

export const OnsortTestValue_MULTISELECT2: OnSort = {
    values: [ "FD" ],
    col: { 
        field: "type_cadre_intervention_name", 
        header: "Mode d'intervention", 
        inputType: "selectmultiple", 
        placeholder: "Sélectionner le mode si nécessaire", 
        options: [ {label: "FD", value: "FD"} ], 
        keyField: "name", 
        panelGroup: "Paramétrage du service", 
        tooltip: { text: "Menu dans lequel sera affiché le service dans le formulaire du CRI" }, 
        contextMenuHeader: { MULTISELECT: { active: true } }
    },
    sortType: "MULTISELECT",
    filterMatchMode: "in"
}

export const listStandard_Sort_DateWithCallback__SORTED = [{ id: "3", date: "2019-04-01" }];
const columnTestDateWhitCallback1: Column = {
    field: "date", 
    header: "Date", 
    inputType: "date", 
    placeholder: "Renseigner une date",
    contextMenuHeader: {
        DATE: {
            active: true,
            callback: (sortType) => new Promise((resolve, reject) => resolve(listStandard_Sort_DateWithCallback__SORTED))
        }
    }
};

export const OnsortTestValueWhithCallback_DATE: OnSort = {
    col: columnTestDateWhitCallback1,
    sortType: "DATE",
    start_date: new Date("2019-01-01"),
    end_date: new Date("2019-05-02")
};

export const listStandard_Sort_StringWithCallback__SORTED = addCheckboxDefaultValues([{ id: "3", name: "Nouveau name filter callback" }]);
const columnTestStringWhitCallback1: Column = {
    field: "name", 
    header: "Libellé", 
    inputType: "input", 
    placeholder: "Renseigner un nom",
    contextMenuHeader: {
        STRING: {
            active: true,
            callback: (sortType) => new Promise((resolve, reject) => resolve(listStandard_Sort_StringWithCallback__SORTED))
        }
    }
};

export const OnsortTestValueWhithCallback_STRING: OnSort = {
    col: columnTestStringWhitCallback1,
    input: "nananere",
    filterMatchMode: "contains",
    sortType: "STRING"
};


export const listStandard_Sort_MultiselectWithCallback__SORTED = addCheckboxDefaultValues([{ id: "4", type_cadre_intervention_name: "OI,TY,FD,RE,KJ" }]);
const columnTestMultiselectWhitCallback1: Column = {
    field: "type_cadre_intervention_name", 
    header: "Mode d'intervention", 
    inputType: "multiselect", 
    placeholder: "Sélectionner le mode si nécessaire", 
    options: [ {label: "FD", value: "FD"} ], 
    keyField: "name", 
    panelGroup: "Paramétrage du service", 
    tooltip: { text: "Menu dans lequel sera affiché le service dans le formulaire du CRI" }, 
    contextMenuHeader: {
        MULTISELECT: {
            active: true,
            callback: (sortType) => new Promise((resolve, reject) => resolve(listStandard_Sort_MultiselectWithCallback__SORTED))
        }
    }
};

export const OnsortTestValueWhithCallback_MULTISELECT: OnSort = {
    values: ["FD"],
    col: columnTestMultiselectWhitCallback1,
    sortType: "MULTISELECT",
    filterMatchMode: "in"
};

// Tableau de résultat (liste standard)
export const listStandard_Sort_A_Z_OnpageChange_Result: any[] = addCheckboxDefaultValues([
    { id: "5", name: "GH NANANERE AGEE (AMPA)" },
    { id: "6", name: "OP FAUTPASREVER AGEE (AMPA)" }
]);