import { Column } from "../models/column";

export const defaultValues = [
    { id: 1, name: "name 1", dropDownField: "6", unity: "4" },
    { id: 2, name: "name 2", dropDownField: "4", unity: "6" },
    { id: 3, name: "name 3", dropDownField: "2", unity: "9" },
    { id: 4, name: "name 4", dropDownField: "8", unity: "0" },
    { id: 5, name: "name 5", dropDownField: "0", unity: null },
    { id: 6, name: "name 6", dropDownField: "7", unity: null }
];

export const defaultColumns: Array<Column[]> = [
    [
        { field: "id", header: "ID" },
        { field: "name", header: "Nom", inputType: "input", onchange: (value, item) => Promise.resolve(item) },
        { field: "unity", header: "Unité" },
        { field: "dropDownField", header: "dropDown header", inputType: "select", options: [
            { value: 1, label: "l 1" },
            { value: 2, label: "l 2" },
            { value: 3, label: "l 3" },
            { value: 4, label: "l 4" },
            { value: 5, label: "l 5" }
        ] },
        { field: "autoCompleteField", header: "Auto Complete", inputType: "autocomplete" }
    ]
];