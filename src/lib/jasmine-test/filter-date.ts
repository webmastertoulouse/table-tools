import { Column } from "../models/column";

export const defaultValues = [
    { id: 1, name: "name 1", dropDownField: "6", date: "2019/01/01" },
    { id: 2, name: "name 2", dropDownField: "4", date: "2019/02/01" },
    { id: 3, name: "name 3", dropDownField: "2", date: "2019/02/31" },
    { id: 4, name: "name 4", dropDownField: "8", date: "2019/03/01" },
    { id: 5, name: "name 5", dropDownField: "0", date: "2019/03/25" },
    { id: 6, name: "name 6", dropDownField: "7", date: "2019/03/12" }
];

export const defaultColumns: Array<Column[]> = [
    [
        { field: "id", header: "ID" },
        { field: "name", header: "Nom", inputType: "input" },
        { field: "date", header: "Date", inputType: "pcalendar" },
        { field: "dropDownField", header: "dropDown header", inputType: "select", options: [
            { value: 1, label: "l 1" },
            { value: 2, label: "l 2" },
            { value: 3, label: "l 3" },
            { value: 4, label: "l 4" },
            { value: 5, label: "l 5" }
        ] }
    ]
];