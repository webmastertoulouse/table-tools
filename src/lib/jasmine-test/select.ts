import { Column } from "../models/column";
import { Validators } from "@angular/forms";

export const columns: Array<Column[]> = [
    [
        { field: "id", header: "ID", inputType: null },
        { field: "checked", header: "Est valide", inputType: null, icon: { cb: (col: Column, rowData) => {
            if (!Math.round(Math.random())) {
                return '<i class="fas fa-times-circle"></i>';
            } else {
                return '<i class="fas fa-plus"></i>';
            }
         }, onClick: (col, rowData) => {
             console.log(col, rowData);
         } } },
        { field: "code_service", header: "Code service", inputType: "input", placeholder: "Code dans le logiciel de Paye", panelGroup: "Paramétrage du service", validators: [Validators.max(99), Validators.pattern("^[0-9]*$")], tooltip: {text: "Code du service dans le logiciel de Paye/Facturation externe" } },
        { field: "type_service", header: "Type de service", inputType: "select", options: [], panelGroup: "Paramétrage du service", validators: [Validators.required, Validators.maxLength(2)], tooltip: {text: "FD=Fiche présence, FA=Entrée/Sortie, F?=Elément facturable (se rapporter à la documentation), P ?=Hors intervention (se rapporter à la documentation)" }, contextMenuHeader: {MULTISELECT: {active: true}} },
        { field: "name", header: "Libellé", inputType: "input", placeholder: "Renseigner un libellé personnalisé", panelGroup: "Paramétrage du service", validators: [Validators.required, Validators.maxLength(55)] },
        { field: "product_name", header: "Nom", inputType: "autocomplete", placeholder: "Sélectionner un produit TG", options: [], keyField: "label", emptyMessage: "Aucun résultat", panelGroup: "Produit TG", validators: [Validators.required], tooltip: {text: "Sélectionner un produit dans le catalogue national.\n<br />Un produit TG est unique par association.\n<br />Sélection obligatoire." } },
        { field: "type_unit_name", header: "Unité", inputType: "select", options: [], keyField: "name", dataKey: "id", disabled: true, panelGroup: "Produit TG", validators: [Validators.required] },
        { field: "type_cadre_intervention_name", header: "Mode d'intervention", inputType: "select", placeholder: "Sélectionner le mode si nécessaire", options: [], dataKey: "id", keyField: "name", panelGroup: "Paramétrage du service", tooltip: {text: "Menu dans lequel sera affiché le service dans le formulaire du CRI" } },
        { field: "with_kilometer", header: "Pas de kms ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de bloquer la saisie de kilomètres (nombre et type de trajet)  pour un service.\n<br />Coché = l'intervenant ne pourra pas saisir de kilomètres." } },
        { field: "with_duration", header: "Avec une durée ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de déterminer le mode de saisie des heures d'interventions.\n<br />Coché = L'intervenant devra renseigner une durée en heure/minute.\n<br />Décoché = L'intervenant devra renseigner un début et un fin.\n<br />Ce paramètre ne s'applique qu'aux services d'unité \"Heure\" ou \"Minute\"." } },
        { field: "is_holiday", header: "Congé ?", inputType: "checkbox", panelGroup: "Paramétrage du service", tooltip: {text: "Permet de définir un service comme étant une absence/un congé. Ceci est utile pour comptabiliser les congés/absences sont les impressions (fiche de présence).\n<br />Ce paramètre permet aussi de bloquer la saisie de kilomètres sur le mobile des intervenants." } },
        { field: "tg_type_transaction", header: "Type transaction", inputType: "select", options: [], keyField: "name", panelGroup: "Paramétrage du service", cleanValue: false },
        { field: "is_not_synced", header: "Pas synchro mobile", inputType: "checkbox", panelGroup: "Produit TG", disabled: true },
        { field: "is_reconcile", header: "Est rapproché au planning ?", inputType: "checkbox", panelGroup: "Produit TG", disabled: true, tooltip: {text: "Précise si les CRIs associés au produit TG sélectionné seront rapprochés au planning" } },
        { field: "is_sequence", header: "Est séquencé ?", inputType: "checkbox", panelGroup: "Produit TG", disabled: true }
    ]
];