export const getARightClick = () => {
    const rightClick = document.createEvent('MouseEvents');
    rightClick.initMouseEvent(
        'click', // type
        true,    // canBubble
        true,    // cancelable
        window,  // view - set to the window object
        1,       // detail - # of mouse clicks
        10,       // screenX - the page X coordinate
        10,       // screenY - the page Y coordinate
        10,       // clientX - the window X coordinate
        10,       // clientY - the window Y coordinate
        false,   // ctrlKey
        false,   // altKey
        false,   // shiftKey
        false,   // metaKey
        2,       // button - 1 = left, 2 = right
        null     // relatedTarget
    );

    return rightClick;
}