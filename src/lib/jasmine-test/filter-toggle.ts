import { Column } from "../models/column";

export const simpleColumns: Array<Column[]> = [
    [
        { field: "id", header: "ID", inputType: null },
        { field: "name", header: "Libellé", inputType: "input" },
        { field: "description", header: "Description", inputType: "input" },
    ]
];

export const filterToggleColumn = { 
    field: "name", 
    header: "Nom", 
    inputType: "input"
};

// Tableau de résultat (liste standard)
export const listStandard_Grouped: any[] = [
    { id: "1", name: "AA", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA" },
    { id: "2", name: "BB", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "BB" },
    { id: "3", name: "AA", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA" },
    { id: "4", name: "CC", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "CC" },
    { id: "5", name: "AA", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA" },
    { id: "6", name: "BB", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "BB" }
];