import { Column } from "../models/column";
import { Validators } from "@angular/forms";
import { number_pattern, date_ddmmyyyy_pattern } from "../const";

export const simpleColumns: Array<Column[]> = [
    [
        { field: "id", header: "ID", inputType: null },
        { field: "name", header: "Libellé", inputType: "input" },
        { field: "description", header: "Description", inputType: "input" },
        { field: "groupedField", header: "Grouped Field", inputType: "input" },
        { field: "autoCompleteField", header: "autoComplete Field", inputType: "autocomplete" },
        { field: "selectmultipleField", header: "selectmultiple Field", inputType: "selectmultiple" },
        { field: "checkboxField", header: "checkbox Field", inputType: "checkbox" }
    ]
];

export const tableCellColumn: Column = { 
    field: "name", 
    header: "Nom", 
    inputType: "input"
};

export const tableCellColumn2: Column = { 
    field: "groupedField", 
    header: "Grouped Field", 
    inputType: "input", 
    validators: [Validators.maxLength(6)]
};

export const tableCellColumn3: Column = { 
    field: "groupedField", 
    header: "Grouped Field", 
    inputType: "input", 
    validators: [Validators.minLength(6)]
};

export const tableCellColumn4: Column = { 
    field: "groupedField", 
    header: "Grouped Field", 
    inputType: "input", 
    validators: [Validators.required]
};

export const tableCellColumn5: Column = { 
    field: "groupedField", 
    header: "Grouped Field", 
    inputType: "input", 
    validators: [Validators.pattern(number_pattern)]
};

export const tableCellColumn6: Column = { 
    field: "groupedField", 
    header: "Grouped Field", 
    inputType: "input", 
    validators: [Validators.pattern(date_ddmmyyyy_pattern)]
};

export const tableCellColumn7: Column = { 
    field: "selectmultipleField", 
    header: "selectmultiple Field", 
    inputType: "selectmultiple",
    options: [
        { label: "Label 1", value: "Value 1" },
        { label: "Label 2", value: "Value 2" },
        { label: "Label 3", value: "Value 3" },
        { label: "Label 4", value: "Value 4" },
        { label: "Label 5", value: "Value 5" },
        { label: "Label 6", value: "Value 6" }
    ]
};

export const tableCellColumn8: Column = { 
    field: "selectmultipleField", 
    header: "selectmultiple Field", 
    inputType: "selectmultiple",
    dataKey: "dezoui",
    keyField: "jcpsod",
    options: [
        { jcpsod: "Label 1", dezoui: "Value 1" },
        { jcpsod: "Label 2", dezoui: "Value 2" },
        { jcpsod: "Label 3", dezoui: "Value 3" },
        { jcpsod: "Label 4", dezoui: "Value 4" },
        { jcpsod: "Label 5", dezoui: "Value 5" },
        { jcpsod: "Label 6", dezoui: "Value 6" }
    ]
};

export const tableCellColumn9: Column = { 
    field: "checkboxField", 
    header: "checkbox Field", 
    inputType: "checkbox"
};

// Tableau de résultat (liste standard)
export const listStandard_Grouped: any[] = [
    { id: "1", name: "AA", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA", autoCompleteField: "RDEYE" },
    { id: "2", name: "BB", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "BB", autoCompleteField: "RDEYE" },
    { id: "3", name: "AA", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA", autoCompleteField: "RDEYE" },
    { id: "4", name: "CC", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "CC", autoCompleteField: "RDEYE" },
    { id: "5", name: "AA", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "AA", autoCompleteField: "RDEYE" },
    { id: "6", name: "BB", description: "POKDoid dspoids disopdifruooi fipfi zoef ep", groupedField: "BB", autoCompleteField: "RDEYE" }
];

export const RowData: any = { 
    id: "1", 
    name: "AA", 
    description: "POKDoid dspoids disopdifruooi fipfi zoef ep", 
    groupedField: "LALALA", 
    autoCompleteField: "RDEYE", 
    selectmultipleField: [] 
};

export const RowData2: any = { 
    id: "1", 
    name: "AA", 
    description: "POKDoid dspoids disopdifruooi fipfi zoef ep", 
    groupedField: "LALALA", 
    selectmultipleField: [
        "value 1",
        "value 2",
        "value 3",
        "value 4",
        "value 5"
    ]
};