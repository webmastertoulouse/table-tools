import { SortType } from "./sort-type";

export interface ContextMenuHeaderConfig {
    ASC?: { active: boolean };
    DESC?: { active: boolean };
    DATE?: { active: boolean, callback?: (sort: SortType) => Promise<any[]> };
    STRING?: { active: boolean, callback?: (sort: SortType) => Promise<any[]> };
    MULTISELECT?: { active: boolean, callback?: (sort: SortType) => Promise<any[]> };
    BINARY?: { active: boolean, callback?: (sort: SortType) => Promise<any[]> };
}