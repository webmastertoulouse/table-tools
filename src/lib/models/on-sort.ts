import { Column } from "./column";

export interface OnSort {
    col: Column;
    sortType?: string;
    values?: any[];
    start_date?: Date, 
    end_date?: Date,
    filterMatchMode?: string;
    input?: string | boolean;
    event?: any;
    parent?: HTMLElement;
}