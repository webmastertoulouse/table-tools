import { Column } from "./column";
import { OnNotify } from "./on-notify";
import { ContextMenuRow } from "./cm-row";
import { ConfigList } from "./config-list";
import { Callbacks } from "./callbacks";
import { Paginator } from "./paginator";
import { groupedColumn } from './grouped-column';

export interface BodyTable {
    indexColumn: string;
    rows: any[];
    columns: Array<Column[]>;
    canEdit?: boolean;
    canAdd?: boolean;
    controlAdd?: boolean;
    canDelete?: boolean;
    onReject?: OnNotify;
    onUpdate?: OnNotify;
    onDelete?: OnNotify;
    onDisableRow?: OnNotify | null;
    onAdd?: OnNotify;
    contextMenu?: ContextMenuRow;
    config?: ConfigList;
    callbacks?: Callbacks;
    paginator?: Paginator;
    closeModal?: boolean;
    groupedColumn?: groupedColumn;
}