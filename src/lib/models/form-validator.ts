export interface FormValidator {
    required: boolean;
    isValid: boolean;
}