import { Column } from "./column";

export interface Icon {
    onClick?: (col: Column, rowData) => Promise<any> | any;
    cb: (col: Column, rowData) => string | void; // l'icon à afficher conditionné (<i class="..."></i>, <img src="..." />)
}