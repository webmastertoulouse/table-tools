import { Column } from "./column";

export interface Tooltip {
    text?: string;
    // position?: string;
    // onEvent?: string;
    callback?: (col: Column, value: any) => Promise<string>;
}