export interface OnNotify {
    rowData: any;
    message?: string;
}