import { Column } from "./column";
import { OnSort } from "./on-sort";

export interface Callbacks {
    onSort?: {
        title: (onSort: OnSort) => Promise<string>;
    };
    contextMenu?: {
        show: (value: any) => boolean;
    };
    onEditInit?: (event: {field: string, rowData: any}) => void;
    onEditComplete?: (event: {field: string, rowData: any}) => void;
    onEditCancel?: (event: {field: string, rowData: any}) => void;
    onGroupedRows?: (event: { column: Column, groupedRows: any[], groupedField: string }) => Promise<{text?: any, colspan?: number, bgColor?: string, color?: string}>;
}