import { Column } from "./column";
import { Table as PTable } from 'primeng/table';

export interface ContextMenuHeader {
    col?: Column;
    event?: MouseEvent;
    hide: boolean;
    parent?: HTMLElement;
    dataTable?: any;
    table?: PTable;
}