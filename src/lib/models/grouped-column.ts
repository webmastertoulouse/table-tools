import { Column } from './column';

export interface groupedColumn {
  active: boolean;
  column: Column;
  reverse?: boolean;
}