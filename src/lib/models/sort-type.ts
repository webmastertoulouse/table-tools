export interface SortType {
    column: string;
    value: {
        start_date: Date;
        end_date: Date;
    } | string | any[] | boolean;
    sortType: string; // 'ASC' | 'DESC' | 'DATE' | 'STRING' | 'MULTISELECT';
}