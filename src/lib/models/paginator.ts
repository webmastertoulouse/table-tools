import { CallbackPaginator } from "./callback-paginator";

export interface Paginator {
    rows: number;
    callback?: (value: CallbackPaginator) => Promise<any[]>;
    rowsPerPageOptions?: number[];
    pageLinkSize?: number;
}