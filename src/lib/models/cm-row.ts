export interface ContextMenuRow {
    delete?: boolean;
    grouped?: boolean;
    actions?: Array<{id: string; name: string; icon: string; }>;
}