import { FilterMetadata } from "primeng/api";

export interface CallbackPaginator {
    first: number; 
    rows: number;
    hasFilter: boolean;
    filter: {
        [s: string]: FilterMetadata;
    };
}