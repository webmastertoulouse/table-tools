import { ContextMenuHeaderConfig } from "./cm-header-config";
import { Tooltip } from "./tooltip";
import { Icon } from "./icon";

export interface Column {
    field?: string;
    header: string;
    inputType?:string;
    keyField?: string; // optionLabel --> défaut : label
    dataKey?: string; // optionValue --> defaut : value
    options?: any[];
    required?: boolean;
    emptyMessage?: string;

    width?: string;
    rowspan?: number;
    colspan?: number;
    contextMenuHeader?: ContextMenuHeaderConfig; // config des tries sur chaque colonne
    validators?: any[]; // array of angular validator
    tooltip?: Tooltip;
    panelGroup?: string;
    disabled?: boolean;
    placeholder?: string;
    // Utilisé dans add-modal pour faire des changement via promise sur des clés de l'objet
    onchange?: (value: Column, item: any) => Promise<any>; 
    // Utilisé dans table-cell pour modifier des clés de l'objet immédiatement à la saisie
    valueChange?: (value: Column, item: any) => any;
    display?: boolean; // display cell
    locale?: any; // date p-calendar locale var
    cleanValue?: boolean; // clean value select before send to controller
    permitUpdade?: boolean; // permit update on listing
    icon?: Icon;
    isGrouped?: boolean; // Valeur interne au composant
    // Utilisé pour les headers table imbriqués (colspan, rowspan). 
    // Si non renseigné l'affichage n'est pas correct
    position?: number;
    editable?: boolean; // if a select id editable, default true
    ngStyle?: (rowData: any) => any; // n'importe quel css class
    thStyle?: any; // Style pour les contenus des <th> du tableau
    requestOptions?: { url: string; keyField: string; dataKey: string; };
    ngClass?: any;
    updateDelay?: number; // Délai en millisecondes avant mise à jour ligne
    // outerKeyField: {
    //     field: string;
    //     value: string;
    //     key: string;
    // };
    // visibleList: boolean;
    // properties: any;
    // ngClass: any;
    // expanded: boolean;
    // asHTML: boolean;
};