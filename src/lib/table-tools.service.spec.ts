import { TestBed } from '@angular/core/testing';

import { TableToolsService } from './table-tools.service';

describe('TableToolsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableToolsService = TestBed.get(TableToolsService);
    expect(service).toBeTruthy();
  });
});
