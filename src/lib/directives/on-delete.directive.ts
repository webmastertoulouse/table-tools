import { Directive, Input, OnChanges, SimpleChanges, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TableToolsComponent } from '../components/table/table.component';
import { Column } from '../models/column';
import  { langage } from '../config/langage';
import { OnNotify } from '../models/on-notify';

@Directive({
  selector: '[onDelete]'
})
export class OnDeleteDirective implements OnChanges, OnDestroy {

  @Input('onDelete') OnDelete?: OnNotify;
  @Input('nsiOnNotifyRow') row: any;
  @Input('nsiOnNotifyColumns') columns: Column;
  @Input('indexColumn') indexColumn: string;

  private _notifyRows: any[] = [];

  private _onUpdateSubscription: Subscription;

  constructor(private _tb: TableToolsComponent,
    private _el: ElementRef) {
      
    this._onUpdateSubscription = this._tb.tableService.updateCell$.subscribe(event => {
      this.onUpdate(event);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.OnDelete && changes.OnDelete.currentValue !== undefined) {
      if (changes.OnDelete.firstChange === false && changes.OnDelete.currentValue) {
        let rowData       = changes.OnDelete.currentValue.rowData;
        let message       = JSON.stringify(changes.OnDelete.currentValue.message);
        let currentRow    = JSON.stringify(this.row);

        if (!message) message = langage.MESSAGES.ON_REJECT;

        if ((rowData && rowData[this.indexColumn] 
          && this.row && this.row[this.indexColumn] 
          && this.row[this.indexColumn] == rowData[this.indexColumn])
          || this._notifyRows.indexOf(currentRow) > -1) {

          this._addClass(currentRow, message);
        }
      }
  
      if (this._tb.showLog === true) {
        console.log("onDelete::ngOnChanges::currentValue", changes.OnDelete.currentValue);
      }
    }
  }

  onUpdate(event) {
    if (event.previousData) {
      this._removeClass(event.previousData);
    }
  }

  ngOnDestroy(): void {
    if (this._onUpdateSubscription) {
      this._onUpdateSubscription.unsubscribe();
      this._onUpdateSubscription = null;
    }
  }

  private _addClass(currentRow: any, message: string): void {
    let tableCell     = this._el.nativeElement.querySelector('td');
    if (this._notifyRows.indexOf(currentRow) == -1) {
      this._notifyRows.push(currentRow);
      this._el.nativeElement.className += ' delete-row tooltip';
      if (tableCell) {
        tableCell.setAttribute('data-tooltip', message);
      }
    }
  }

  private _removeClass(data: any): void {
    let tableCell   = this._el.nativeElement.querySelector('td');
    let currentRow  = JSON.stringify(data);
    let index       = this._notifyRows.indexOf(currentRow);
    if (index > -1) {
      this._notifyRows.splice(index, 1);
      this._el.nativeElement.className = this._el.nativeElement.className.replace('delete-row', '');
      this._el.nativeElement.className = this._el.nativeElement.className.replace('tooltip', '');
      this._el.nativeElement.className.trim();
      if (tableCell) {
        tableCell.setAttribute('data-tooltip', '');
      }
    }
  }
}
