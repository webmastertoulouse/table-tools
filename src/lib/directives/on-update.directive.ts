import { Directive, Input, OnChanges, SimpleChanges, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TableToolsComponent } from '../components/table/table.component';
import { Column } from '../models/column';
import  { langage } from '../config/langage';
import { OnNotify } from '../models/on-notify';

@Directive({
  selector: '[onUpdate]'
})
export class OnUpdateDirective implements OnChanges, OnDestroy {

  @Input('onUpdate') OnUpdate?: OnNotify;
  @Input('nsiOnNotifyRow') row: any;
  @Input('nsiOnNotifyColumns') columns: Array<Column[]>;
  @Input('indexColumn') indexColumn: string;

  private _notifyRows: any[] = [];

  private _onUpdateSubscription: Subscription;

  constructor(private _tb: TableToolsComponent,
    private _el: ElementRef) {
      
    this._onUpdateSubscription = this._tb.tableService.updateCell$.subscribe(event => {
      this.onUpdate(event);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.OnUpdate && changes.OnUpdate.currentValue !== undefined) {
      if (changes.OnUpdate.firstChange === false && changes.OnUpdate.currentValue) {
        let rowData       = changes.OnUpdate.currentValue.rowData;
        let message       = JSON.stringify(changes.OnUpdate.currentValue.message);
        let currentRow    = JSON.stringify(this.row);

        if (!message) message = langage.MESSAGES.ON_REJECT;

        if ((rowData && rowData[this.indexColumn] 
          && this.row && this.row[this.indexColumn] 
          && this.row[this.indexColumn] == rowData[this.indexColumn])
          || this._notifyRows.indexOf(currentRow) > -1) {

          this._addClass(currentRow, message);
        }
      }

      if (this._tb.showLog === true) {
        console.log("OnUpdateDirective::ngOnChanges::changes", changes);
      }
    }
  }

  onUpdate(event) {
    if (event.previousData) {
      this._removeClass(event.previousData);
    }
  }

  ngOnDestroy(): void {
    if (this._onUpdateSubscription) {
      this._onUpdateSubscription.unsubscribe();
      this._onUpdateSubscription = null;
    }
  }

  private _addClass(currentRow: any, message: string): void {
    let tableCell     = this._el.nativeElement.querySelector('td');
    if (this._notifyRows.indexOf(currentRow) == -1) {
      this._notifyRows.push(currentRow);
      this._el.nativeElement.className += ' update-row tooltip';
      if (tableCell) {
        tableCell.setAttribute('data-tooltip', message);
      }
    }
  }

  private _removeClass(data: any): void {
    let tableCell   = this._el.nativeElement.querySelector('td');
    let currentRow  = JSON.stringify(data);
    let index       = this._notifyRows.indexOf(currentRow);
    if (index > -1) {
      this._notifyRows.splice(index, 1);
      this._el.nativeElement.className = this._el.nativeElement.className.replace('update-row', '');
      this._el.nativeElement.className = this._el.nativeElement.className.replace('tooltip', '');
      this._el.nativeElement.className.trim();
      if (tableCell) {
        tableCell.setAttribute('data-tooltip', '');
      }
    }
  }
}
