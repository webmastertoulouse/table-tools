import { Directive, HostListener, Input } from '@angular/core';
import { TableToolsComponent } from '../components/table/table.component';
import { Column } from '../models/column';

@Directive({
  selector: '[nsiContextMenuHeader]'
})
export class ContextMenuHeader {

  @Input("nsiContextMenuHeader") col: Column;
  constructor(private _tb: TableToolsComponent) { }

  @HostListener('mouseover', ['$event'])
  onMouseHover(event: MouseEvent) {
    this._tb.onContextMenuHeader({ col: this.col, hide: false });
  
    if (this._tb.showLog === true) {
      console.log("nsiContextMenuHeader::onMouseHover::col", this.col);
    }
  }
}