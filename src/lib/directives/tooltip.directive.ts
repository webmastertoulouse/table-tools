import { Directive, Input, HostListener } from '@angular/core';
import { Column } from '../models/column';
import { TableToolsComponent } from '../components/table/table.component';

@Directive({
  selector: '[nsiTooltip]'
})
export class TooltipDirective {

  @Input("nsiTooltip") col: Column;

  @Input("nsiTableCell") tableCell: Column;

  @Input("value") value: any;

  constructor(private _tb: TableToolsComponent) { }

  @HostListener('mouseover', ['$event'])
  onMouseHover(event: MouseEvent) {
    if (this.col.tooltip && 
      (this.col.tooltip.text || (this.col.tooltip.callback && this.col.tooltip.callback instanceof Function))) {
      this._tb.onTooltip({ col: this.col, event: event, value: this.value, tableCell: this.tableCell });
  
      if (this._tb.showLog === true) {
        console.log("nsiTooltip::onMouseHover::col", this.col);
      }
    }
  }
}