import { Directive, Input, ElementRef, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { TableToolsComponent } from '../components/table/table.component';
import { Subscription } from 'rxjs';
import { langage } from '../config/langage';
import { Column } from '../models/column';

@Directive({
  selector: '[onDisableRow]'
})
export class OnDisableRowDirective implements OnChanges, OnDestroy {

  @Input("onDisableRow") rowData: any;
  @Input('indexColumn') indexColumn: string;
  @Input('nsiOnNotifyRow') row: any;
  @Input('nsiOnNotifyColumns') columns: Column;

  private _notifyRows: any[] = [];

  private _onDisableRowSubscription: Subscription;

  constructor(private _tb: TableToolsComponent,
    private _el: ElementRef) {
    this._onDisableRowSubscription = this._tb.tableService.updateCell$.subscribe((e) => {
      this.onUpdate(e);
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    
    if (changes.rowData && changes.rowData.currentValue) {
      let rowData           = changes.rowData.currentValue.rowData;
      let currentRow        = JSON.stringify(this.row);
      let message           = JSON.stringify(changes.rowData.currentValue.message);

      if (!message) message = langage.MESSAGES.ON_DISABLE_ROW;

      if ((rowData && rowData[this.indexColumn] 
        && this.row && this.row[this.indexColumn] 
        && this.row[this.indexColumn] == rowData[this.indexColumn])
        || this._notifyRows.indexOf(currentRow) > -1) {

        this._addClass(currentRow, message);
      }
  
      if (this._tb.showLog === true) {
        console.log("OnDisableRowDirective::ngOnChanges::row", this.row);
        console.log("OnDisableRowDirective::ngOnChanges::changes", changes);
      }
    } else {
      this._removeClass();
    }
  }

  ngOnDestroy(): void {
    if (this._onDisableRowSubscription) {
      this._onDisableRowSubscription.unsubscribe();
      this._onDisableRowSubscription = null;
    }
  }

  onUpdate(event) {
    if (event.previousData) {
      this._removeClass();
    }
  }

  private _addClass(currentRow: any, message: string): void {
    const tableCell = this._el.nativeElement.querySelector("td");
    if (this._notifyRows.indexOf(currentRow) == -1) {
      this._notifyRows.push(currentRow);
      this._el.nativeElement.className += ' disable-row tooltip';
      if (tableCell) {
        tableCell.setAttribute('data-tooltip', message);
      }
    }
  }

  private _removeClass() {
    const tableCell = this._el.nativeElement.querySelector("td");
    this._el.nativeElement.className = this._el.nativeElement.className.replace("disable-row", "");
    this._el.nativeElement.className.trim();
    if (tableCell) {
      tableCell.setAttribute("data-tooltip", "");
    }
  }
}
