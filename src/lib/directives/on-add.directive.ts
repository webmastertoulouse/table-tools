import { Directive, Input, OnChanges, SimpleChanges, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TableToolsComponent } from '../components/table/table.component';
import { Column } from '../models/column';
import  { langage } from '../config/langage';
import { OnNotify } from '../models/on-notify';

@Directive({
  selector: '[onAdd]'
})
export class OnAddDirective implements OnChanges, OnDestroy {

  @Input('onAdd') OnAdd?: OnNotify;
  @Input('nsiOnNotifyRow') row: any;
  @Input('nsiOnNotifyColumns') columns: Column;
  @Input('indexColumn') indexColumn: string;

  private _notifyRows: any[] = [];

  private _onUpdateSubscription: Subscription;

  constructor(private _tb: TableToolsComponent,
    private _el: ElementRef) {
      
    this._onUpdateSubscription = this._tb.tableService.updateCell$.subscribe(event => {
      this.onUpdate(event);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.OnAdd && changes.OnAdd.currentValue !== undefined) {
      if (changes.OnAdd.firstChange === false && changes.OnAdd.currentValue) {
        let rowData       = changes.OnAdd.currentValue.rowData;
        let message       = JSON.stringify(changes.OnAdd.currentValue.message);
        let currentRow    = JSON.stringify(this.row);
  
        if (!message) message = langage.MESSAGES.ON_REJECT;
  
        if ((rowData && rowData[this.indexColumn] 
          && this.row && this.row[this.indexColumn] 
          && this.row[this.indexColumn] == rowData[this.indexColumn])
          || this._notifyRows.indexOf(currentRow) > -1) {
  
          this._addClass(currentRow, message);
        }
      }
  
      if (this._tb.showLog === true) {
        console.log("onAdd::ngOnChanges::col", changes.OnAdd.currentValue);
      }
    }
  }

  onUpdate(event) {
    if (event.previousData) {
      this._removeClass(event.previousData);
    }
  }

  ngOnDestroy(): void {
    if (this._onUpdateSubscription) {
      this._onUpdateSubscription.unsubscribe();
      this._onUpdateSubscription = null;
    }
  }

  private _addClass(currentRow: any, message: string): void {
    let tableCell     = this._el.nativeElement.querySelector('td');
    if (this._notifyRows.indexOf(currentRow) == -1) {
      this._notifyRows.push(currentRow);
      this._el.nativeElement.className += ' add-row tooltip';
      if (tableCell) {
        tableCell.setAttribute('data-tooltip', message);
      }
    }
  }

  private _removeClass(data: any): void {
    let tableCell   = this._el.nativeElement.querySelector('td');
    let currentRow  = JSON.stringify(data);
    let index       = this._notifyRows.indexOf(currentRow);
    if (index > -1) {
      this._notifyRows.splice(index, 1);
      this._el.nativeElement.className = this._el.nativeElement.className.replace('add-row', '');
      this._el.nativeElement.className = this._el.nativeElement.className.replace('tooltip', '');
      this._el.nativeElement.className.trim();
      if (tableCell) {
        tableCell.setAttribute('data-tooltip', '');
      }
    }
  }
}
