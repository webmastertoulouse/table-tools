import { Directive, Input, HostListener } from '@angular/core';
import { TableToolsComponent } from '../components/table/table.component';

@Directive({
  selector: '[onDblclickRow]'
})
export class OnDblclickRowDirective {

  @Input("onDblclickRow") rowData: any;

  constructor(private _tb: TableToolsComponent) { }

  @HostListener('dblclick', ['$event'])
  onMouseDblclick(event: MouseEvent) {
    this._tb.onDblClickRow(this.rowData);
  
    if (this._tb.showLog === true) {
      console.log("onDblclickRow::onMouseDblclick::rowData", this.rowData);
    }
  }
}
