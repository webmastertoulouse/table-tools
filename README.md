# TABLE-TOOLS

## PRÉREQUIS

* primeicons
* primeng
* quill

Ajoutez les styles primeng et primeisons à votre angular.json :

```
"architect": {
  ...
  "build": {
    ...
    "options": {
      ...
      "styles": [
        ...
        "node_modules/primeicons/primeicons.css",
        "node_modules/primeng/resources/primeng.css",
        "node_modules/primeng/resources/themes/nova-light/theme.css"
      ]
```

## Importation de la librarie

```
npm install table-tools --save
```

### Dans votre module :

```
import {TableToolsModule} from 'table-tools';
```

Annexes Columns : [Columns interface](#columns-interface).

### Exemple d'utilisation :

<pre>


import { BodyTable, Column } from 'table-tools';

@Component({
  selector: 'moncomponent',
  template: `
  &lt;table-tools [bodyTable]="bodyTable"
    [showLog]="false"
    (addEvent)="onAdd($event)"
    (updateEvent)="onUpdate($event)"
    (deleteEvent)="onDelete($event)"
    (customEvent)="onCustomEvent($event)"></table-tools>
  `,
  styleUrls: ['./mon.component.scss']
})
export class MyComponent implements OnInit, AfterViewInit {

  public rows: any[] = rows;
  public columns: Array<Column[]> = columns;
  public bodyTable: BodyTable;

  constructor() { }

  ngOnInit() {
    this.bodyTable = {
      columns: this.columns,
      rows: this.rows,
      canEdit: true,
      canAdd: false,
      contextMenu: {
        delete: true,
        actions: [
          { id: "testAction", name: "Test action", icon: "fas fa-truck-loading" }, 
          { id: "testAction2", name: "Test action 2", icon: "fas fa-upload" }
        ]
      },
      config: {
        title: 'Mon joli titre de tableau'   
      },
      callbacks: {
        contextMenu: {
          show: (value: any): boolean => {
            let bool = this.getBoolean();
            console.log(value, bool);
            return bool;
          }
        }
      },
      paginator: {
        rows: 20,
        pageLinkSize: 5
      }
    }
  }

  ngAfterViewInit(): void {
  }

  onClose(event) {
    console.log('onClose', event);
  }

  onChange(event) {
    console.log('onChange', event);
  }

  onDelete(event) {
    console.log('onDelete', event);
  }

  onUpdate(event: any) {
    let bool = this.getBoolean();
    console.log('onUpdate', event);
    console.log("reject", bool);

    // simule erreur :
    if (bool) {
      this.bodyTable.onReject = {
        rowData: event,
        error: "Erreur lors de la mise à jour"
      }
    }
  }

  onCustomEvent(event: any) {
    console.log(event.id, event.value);
  }

  onAdd(event: any) {
    console.log('onAdd', event);
  }

  getBoolean() {
    return !Math.round(Math.random());
  }

</pre>

## ANNEXES

<a name="columns-interface"></a>
## Columns interface : 

| Nom               |                                      Type                                      |                                                                                                                                                                                 Infos |
| ----------------- | :----------------------------------------------------------------------------: | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| field             |                                     string                                     |                                                                                                                                    clé permettant de retrouver la valeur dans l'objet |
| header            |                                     string                                     |                                                                                                                                                                       Titre du champs |
| inputType         |                                     string                                     |                                                                                    autocomplete, select, input, hidden, tooltip, spinner (si tooltip, ce n'est pas une vraie cellule) |
| keyField          |                                     string                                     |                                                                                                  voir "optionLabel" [primeng dropdown](https://www.primefaces.org/primeng/#/dropdown) |
| options           |                                     any[]                                      |                                                                                                                                                  array objets pour remplir une select |
| required          |                                    boolean                                     |                                                                                                                                                                                       |
| emptyMessage      |                                     string                                     |                                                                                                                  Message si la requête pour l'autocomplete ne retourne aucun résultat |
| width             |                                     string                                     |                                                                                                                                                                  largeur des cellules |
| rowspan           |                                     string                                     |                                                                         Sert au groupement des colonnes. voir [primeng colgroup](https://www.primefaces.org/primeng/#/table/colgroup) |
| colspan           |                                     string                                     |                                                                         Sert au groupement des colonnes. voir [primeng colgroup](https://www.primefaces.org/primeng/#/table/colgroup) |
| contextMenuHeader |                            ContextMenuHeaderConfig                             |                                                                      Sert à ajouter les filtres sur les header des colonnes. Valeurs accèptées : ASC, DESC, DATE, STRING, MULTISELECT |
| validators        |                                  Validators[]                                  | Liste de validateurs lors de la mise à jour, ajout de ligne, cellules. Validators.required, Validators.maxLength(10)... [Angular Validators](https://angular.io/api/forms/Validators) |
| tooltip           | Tooltip {text: string, callback: (col: Column, value: any) => Promise<string>} |                                                                                                                                  Sert à afficher une info bulle au hover sur la ligne |
| tooltipPosition   |                                     string                                     |                                                                                                                                                                              position |
| panelGroup        |                                     string                                     |                                                                                                   Sert à positionner le champs dans un panel dans la modal ajout d'une nouvelle ligne |
| placeholder       |                                     string                                     |                                                                                                                                            Information concernant le champs à remplir |

# DECLARATION DE LA LIBRAIRIE (objet BodyTable)

<pre>
export interface BodyTable {
    rows: any[];
    columns: Array<Column[]>;
    canEdit?: boolean;
    canAdd?: boolean;
    canDelete?: boolean;
    onReject?: {rowData: any, error: string};
    contextMenu?: ContextMenuRow;
    config?: ConfigList;
    callbacks?: Callbacks;
    paginator?: Paginator;
}
</pre>

| Nom         |                             Type                             |                                                                                                                 Infos |
| ----------- | :----------------------------------------------------------: | --------------------------------------------------------------------------------------------------------------------: |
| rows        |                            any[]                             |                                                                                           Liste des objets du tableau |
| columns     |                       Array<Column[]>                        |                                         Voir : <a href="#colonnes">pourquoi déclarer un array d'array de colonnes</a> |
| canEdit     |                           boolean                            |                                                                                                       Edition permise |
| canAdd      |                           boolean                            |                                                                                                          Ajout permis |
| canDelete   |                           boolean                            |                                                                                                   Suppression permise |
| onReject    |            Object { rowData: any, error: string }            |                                         Cette évènement peut être appelé lors d'une erreur de mise à jour d'une ligne |
| contextMenu | Objet {actions: {id: string; name: string; icon: string; } } | Permet d'ajouter des valeurs customisée au contextMenu. Evènement récupéré avec (customEvent)="onCustomEvent($event)" |
| config      |                 ConfigList {title: string }                  |                                                                                              Titre du tableau courant |
| callbacks   |                          Callbacks                           |                                          Voir <a href="#callbacks">Ajout de callbacks lors de certains évènements</a> |
| paginator   |                          Paginator                           |                                               Voir <a href="#paginator">Déclaration de valeurs pour la pagination</a> |

# LES METHODES

| Nom                 |                   Fonction                    |                                    Infos |
| ------------------- | :-------------------------------------------: | ---------------------------------------: |
| (addEvent)          |  Méthode appelée à chaque ajout d'un élément  |                                          |
| (deleteEvent)       |      Méthode de suppression d'une ligne       |                                          |
| (updateEvent)       | Appelée après le clic droit sur mettre à jour |                                          |
| (autoCompleteEvent) | Appelée après la saisie dans une autoComplete |                                          |
| (customEvent)       |           ContextMenuRow.actions[]            | Liste d'action spécifique du contextMenu |


| [configList] | ConfigList | Plusieurs infos sont à renseigner avec cette object (title...) |
| [callbacks] | Callbacks | Liste de callbacks pouvant être appelés en fonction des évènements générés par l'utilisateur (doit retourner des promises) |
| [bodyTable] | Object BodyTable | Voir BodyTable interface <code>rows: any[]; columns: Array<Column[]>;</code> |

# ANNEXES

<a name="colonnes"></a>
### Pourquoi déclarer un array d'array de colonne

_C'est pour faciliter la déclaration de groupement de colonnes_
Exemple de définitions des colonnes avec des groupements de colonnes :

<pre>
import { Column, SortType } from 'table-tools';
import { Validators } from '@angular/forms';

export const columns: Array<Column[]> = [
    [
        { rowspan: 3, field: "id", header: "ID (rowspan: 3)" }, 
        { colspan: 3, header: "Col 2 (colspan 4)" }
    ], [
        { rowspan: 2, field: "name", header: "Name (rowspan: 2)", inputType: "input", validators: [Validators.required, Validators.maxLength(10)], contextMenuHeader: {
            STRING: {
                active: true,
                callback: (sortType: SortType) => {
                    return new Promise((resolve, reject) => {
                        // Traitement sur colonne avec requête si vous le souhaitez
                        ...
                        return resolve(newResult);
                    });
                }
            }
        } }, 
        { colspan: 2, header: "Col 4 (colspan: 2)" }
    ], [
        { field: "date", header: "Date", inputType: "date", contextMenuHeader: { DATE: { active: true }}, tooltip: { text: "Test info bulle" },
        validators: [Validators.required, Validators.pattern('[0-9]{2}\/{1}[0-9]{2}\/{1}[0-9]{4}')] },
        { field: "description", header: "Description", inputType: "input", contextMenuHeader: { MULTISELECT: { active: true }  }, 
        tooltip: { callback: (col: Column, value) => {
            return new Promise((resolve, reject) => setTimeout(() => resolve("Nouveau info bulle (" + value[col.field] + ")"), 2000));
        }} }
    ]
]
</pre>

<a name="callbacks"></a>
### Ajout de callbacks lors de certains évènements

<pre>
export interface Callbacks {
    onSort?: {
        title: (newTitle: any) => Promise<string>;
    };
    contextMenu?: {
        show: (value: any) => boolean;
    }
}
</pre>

<a name="paginator"></a>
### déclaration de valeurs pour la pagination

<pre>
export interface Paginator {
    rows: number;
    callback?: (first: number, rows: number) => Promise<any[]>;
    rowsPerPageOptions?: number[];
    pageLinkSize?: number;
}
</pre>

## NOTES

* Un tri multiselect ne fonctionne sur une colonne avec des valeurs séparées par des virgules uniquement avec la valeur inputType: selectmultiple

## P.S. : 

> Documentation peu fournie. Pour toutes questions : [contact](mailto:contact@webmastertoulouse.com).<br>
> Site internet : [www.webmastertoulouse.com](http://www.webmastertoulouse.com/informaticien-freelance-toulouse.html).